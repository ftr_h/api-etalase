<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('get_API_IG','APIInstagram@getIG');

$router->get('/', function () use ($router) {
    return $router->app->version();
});
//create_notif_bayar
$router->post('create_notif_bayar', 'PaymentController@createNotifBayar');
$router->post('create_notif_bayar_test', 'PaymentController@createNotifBayarB');

$router->post('verify_doku', 'PaymentController@verifyDoku');
$router->post('redirect_doku', 'PaymentController@redirectDoku');
$router->post('notify_doku', 'PaymentController@notifyDoku');
$router->post('after_payment_midtrans', 'PaymentController@afterPaymentMidtrans');
$router->post('notify_midtrans', 'PaymentController@notifyMidtrans');

//list get_ig
$router->get('get_ig', 'APIController@getIG');

$router->get('server_get', 'APIController@serverGet');
$router->post('server_post', 'APIController@serverPost');

//get app setting
$router->get('get_app_setting', 'APIController@getAppSetting');
//login
$router->post('post_login', 'UserController@postLogin');
$router->post('post_login_test', 'UserController@postLoginTest');

//create new user/registration
$router->post('create_user', 'UserController@createUser');

$router->group(
    ['middleware' => 'jwt.auth'], 
    function() use ($router) {
//USER
//list user
$router->post('list_user', 'UserController@getListUser');
//get user data / profile
$router->post('get_user', 'UserController@getUser');
//update user
$router->post('update_user', 'UserController@updateUser');
//delete user
$router->post('delete_user', 'UserController@deleteUser');
//update password user
$router->post('update_password_user', 'UserController@updatePasswordUser');
//upload user profile
$router->post('upload_profile_image', 'UserController@uploadProfileImage');

//CHEFF
//list cheff
$router->post('list_cheff', 'CheffController@getListCheff');
//create new cheff/registration
$router->post('register_koki', 'CheffController@createCheff');
//get cheff data
$router->post('get_cheff', 'CheffController@getCheff');
//get cheff and food
$router->post('get_cheff_food', 'CheffController@getCheffFood');
//update cheff
$router->post('update_cheff', 'CheffController@updateCheff');
//upload cheff gallery
$router->post('upload_cheff_gallery', 'CheffController@uploadCheffGallery');
//get list cheff gallery
$router->get('list_cheff_gallery', 'CheffController@getListCheffGallery');
//list cheff search
$router->post('list_cheff_search', 'CheffController@getListCheffSearch');
//list cheff recommend
$router->get('list_recommend_cheff', 'CheffController@getListRecommendCheff');

//FOOD/ITEM
//list food
$router->get('list_food_search', 'FoodController@getListFoodSearch');
//list food
$router->get('list_food_search_new', 'FoodController@getListFoodSearchNew');
//upload food gallery
$router->post('upload_food_gallery', 'FoodController@uploadFoodGallery');
//create new food
$router->post('create_food', 'FoodController@createFood');
//create new food new
$router->post('create_food_new', 'FoodController@createFoodNew');
//update food
$router->post('update_food', 'FoodController@updateFood');
//delete food
$router->post('delete_food', 'FoodController@deleteFood');
//get food data
$router->get('get_food', 'FoodController@getFood');
//get list food
$router->get('list_food_per_cheff', 'FoodController@getListFoodPerCheff');
//get list food gallery
$router->get('list_food_gallery', 'FoodController@getListFoodGallery');
//test distance
$router->post('test_distance', 'FoodController@testDistance');
//signature_food
$router->post('signature_food', 'FoodController@signatureFood');
//publish_food
$router->post('publish_food', 'FoodController@publishFood');
//unpublish_food
$router->post('unpublish_food', 'FoodController@unpublishFood');
//get food data
$router->get('get_food_test', 'FoodController@getFoodTest');

//FOOD CATEGORY
//create_address
$router->post('create_food_category', 'FoodCategoryController@createFoodCategory');
//get data food_category 
$router->get('get_food_category', 'FoodCategoryController@getFoodCategory');
//get list data food_category
$router->get('list_food_category_per_user', 'FoodCategoryController@getListFoodCategory');
//update_food_category
$router->post('update_food_category', 'FoodCategoryController@updateFoodCategory');
//delete_food_category
$router->post('delete_food_category', 'FoodCategoryController@deleteFoodCategory');

//ORDER
//create_order
$router->post('create_order', 'OrderController@createOrder');
//get data order detail
$router->get('get_data_order', 'OrderController@getDataOrder');
//get cart
$router->get('get_cart', 'OrderController@getCart');
//create_order detail
$router->post('create_order_detail', 'OrderController@createOrderDetail');
//list order per user
$router->get('/list_order_per_cheff', 'OrderController@getListOrderPerCheff');
//list order per cheff
$router->get('/list_order_per_user', 'OrderController@getListOrderPerUser');
//list order per cheff
$router->get('/list_order_per_user_by_status', 'OrderController@getListOrderPerUserByStatus');
//list order per cheff by status
$router->get('/list_order_per_cheff_by_status', 'OrderController@getListOrderPerCheffByStatus');
//list order per cheff active
$router->get('/list_order_per_cheff_active', 'OrderController@getListOrderPerCheffActive');
//pay_order
$router->post('pay_order', 'OrderController@payOrder');
//check ongkir
$router->get('check_ongkir', 'OrderController@checkOngkir');

//create_order_new
$router->post('create_order_new', 'OrderController@createOrderNew');
//create_order_neo
$router->post('create_order_neo', 'OrderController@createOrderNeo');
//create_order_niu
$router->post('create_order_niu', 'OrderController@createOrderNiu');
//create_order detail
$router->post('create_order_detail_new', 'OrderController@createOrderDetailNew');
//check ongkir_new
$router->get('check_ongkir_new', 'OrderController@checkOngkirNew');

//confirm_order
$router->post('confirm_order', 'OrderController@confirmOrder');
//receive_order
$router->post('receive_order', 'OrderController@receiveOrder');
//ship_order
$router->post('ship_order', 'OrderController@shipOrder');
//cancel_order
$router->post('cancel_order', 'OrderController@cancelOrder');
//cancel_order_by_admin
$router->post('cancel_order_by_admin', 'OrderController@cancelOrderByCheff');

//NOTIFICATION
//create_notif
$router->post('create_notif', 'NotificationController@createNotif');
//get data notif 
$router->get('get_notif', 'NotificationController@getNotif');
//get data notif detail
$router->get('list_notif_per_user', 'NotificationController@getListNotifPerUser');
//create_notif
$router->post('read_notif', 'NotificationController@readNotif');
//count unread notif
$router->get('count_unread_notif', 'NotificationController@countNotifUnread');
//generate va
$router->get('gen_va', 'PaymentController@genVA');
//get prefix
$router->get('get_prefix', 'PaymentController@getPrefix');
//list prefix
$router->get('list_prefix', 'PaymentController@getLisPrefix');
//submit va
$router->post('submit_va', 'PaymentController@submitVA');
//get_payment_page
$router->get('get_payment_page', 'PaymentController@getPaymentPage');

//PAYMENT
//get payment data
$router->get('get_payment', 'PaymentController@getPayment');
//update payment va
$router->post('update_payment_va', 'PaymentController@updatePaymentVA');

//USER ADDRESS
//create_address
$router->post('create_address', 'AddressController@createAddress');
//get data address 
$router->get('get_address', 'AddressController@getAddress');
//get list data address
$router->get('list_address_per_user', 'AddressController@getListAddressPerUser');
//default_address
$router->post('default_address', 'AddressController@defaultAddress');
//update_address
$router->post('update_address', 'AddressController@updateAddress');
//delete_address
$router->post('delete_address', 'AddressController@deleteAddress');

//REVIEW
//create_review
$router->post('create_review', 'ReviewController@createReview');
//get list data review
$router->get('list_review_per_food', 'ReviewController@getListReviewPerFood');
//get data review 
$router->get('get_review', 'ReviewController@getReview');
//get list data review
$router->get('list_review_per_user', 'ReviewController@getListReviewPerUser');
//update_review
$router->post('update_review', 'ReviewController@updateReview');
//delete_review
$router->post('delete_review', 'ReviewController@deleteReview');

//FOLLOW
//create_follow
$router->post('create_follow', 'FollowController@createFollow');
//get list data follow
$router->get('list_follower', 'FollowController@getListFollower');
//get list data follow
$router->get('list_following', 'FollowController@getListFollowing');
//delete_follow
$router->post('delete_follow', 'FollowController@deleteFollow');
//check follow
$router->get('check_follow', 'FollowController@checkFollow');

//WISHLIST
//create_wishlist
$router->post('create_wishlist', 'WishlistController@createWishlist');
//get list data wishlist
$router->get('list_wishlist', 'WishlistController@getListWishlist');
//delete_wishlist
$router->post('delete_wishlist', 'WishlistController@deleteWishlist');
//check wishlist
$router->get('check_wishlist', 'WishlistController@checkWishlist');

//WISHLIST
//create_wishlist_cheff
$router->post('create_wishlist_cheff', 'WishlistCheffController@createWishlistCheff');
//get list data wishlist_cheff
$router->get('list_wishlist_cheffer', 'WishlistCheffController@getListWishlistCheffer');
//get list data wishlist_cheff
$router->get('list_wishlist_cheff', 'WishlistCheffController@getListWishlistCheff');
//delete_wishlist_cheff
$router->post('delete_wishlist_cheff', 'WishlistCheffController@deleteWishlistCheff');
//check wishlist_cheff
$router->get('check_wishlist_cheff', 'WishlistCheffController@checkWishlistCheff');

//get data news 
$router->get('get_news', 'NewsController@getNews');
//get list data news
$router->get('list_news', 'NewsController@getListNews');

// //CATEGORY CUISINE
// //list cat_cuisine
// $router->post('list_cat_cuisine', 'APIController@getListCatCuisine');
// //create new cat_cuisine
// $router->post('create_cat_cuisine', 'APIController@createCatCuisine');
// //get cat_cuisine data
// $router->post('get_cat_cuisine', 'APIController@getCatCuisine');
// //update cat_cuisine
// $router->post('update_cat_cuisine', 'APIController@updateCatCuisine');
// //delete cat_cuisine
// $router->post('delete_cat_cuisine', 'APIController@deleteCatCuisine');

// //CUISINE
// //list cat_cuisine
// $router->post('list_cuisine', 'APIController@getListCuisine');
// //create new cuisine
// $router->post('create_cuisine', 'APIController@createCuisine');
// //get cuisine data
// $router->post('get_cuisine', 'APIController@getCuisine');
// //update cuisine
// $router->post('update_cuisine', 'APIController@updateCuisine');
// //delete cuisine
// $router->post('delete_cuisine', 'APIController@deleteCuisine');

//app setting
//get_faq1_profile
$router->get('get_faq1_profile', 'OrderController@getFaq1');
//get_faq2_profile
$router->get('get_faq2_profile', 'OrderController@getFaq2');
//get_about_etalase_profile
$router->get('get_about_etalase_profile', 'OrderController@getAboutEtalase');
//get_contact_us_profile
$router->get('get_contact_us_profile', 'OrderController@getContactUs');

//REFF
//list bank
$router->get('list_bank', 'ReffController@getListBank');
//list belajar_memasak
$router->get('list_belajar_memasak', 'ReffController@getListBelajar');
//list category_koki
$router->get('list_category_koki', 'ReffController@getListCategoryKoki');
//list cuisine
$router->get('list_cuisine', 'ReffController@getListCuisine');
//list keahlian
$router->get('list_keahlian', 'ReffController@getListKeahlian');
//list type_diet
$router->get('list_type_diet', 'ReffController@getListTypeDiet');
//list provinsi
$router->get('list_provinsi', 'ReffController@getListProvinsi');
//list kota
$router->get('list_kota', 'ReffController@getListKota');
//list kota
$router->get('list_kecamatan', 'ReffController@getListKecamatan');
//list kota
$router->get('list_kodepos', 'ReffController@getListKodepos');
//list serving_type
$router->get('list_serving_type', 'ReffController@getListServingType');
//list hidangan_type
$router->get('list_hidangan_type', 'ReffController@getListHidanganType');
//list est_cooking
$router->get('list_est_cooking', 'ReffController@getListEstCooking');
//list cooking_method
$router->get('list_cooking_method', 'ReffController@getListCookingMethod');
//list order_type
$router->get('list_order_type', 'ReffController@getListOrderType');

		//get cheff and food test
		$router->post('get_cheff_food_test', 'CheffController@getCheffFoodTest');
    }
);