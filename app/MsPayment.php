<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MsPayment extends Model
{
    protected $table = 'ms_payment';

    public static function createPaymentNiu($reference_id,$va_code,$prefix,$trace_no,$billing_amount) {
        $date_now_ymd = date('Y-m-d');
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = DB::table('ms_payment')->orderby('id_payment','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_payment + 1;
        }
        $sql = DB::insert("INSERT INTO ms_payment (
                id_payment,
                va_code,
                prefix,
                trace_no,
                billing_amount,
                reference_id,
                created_at,
                updated_at
                )
                    values (
                      '".$new_id."',
                      '".$va_code."',
                      '".$prefix."',
                      '".$trace_no."',
                      '".$billing_amount."',
                      '".$reference_id."',
                      '".$date_now_ymdhis."',
                      '".$date_now_ymdhis."'
                    )");
        if ($sql) {
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah pembayaran berhasil";
            // $response["message"] = $message->message;
        } else {          
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah pembayaran gagal";
            // $response["message"] = $message->message;
        }
        
        return $response;
    }
    public static function createPayment($request, $reference_id) {
        $va_code = $request['va_code'];
        $date_now_ymd = date('Y-m-d');
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = DB::table('ms_payment')->orderby('id_payment','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_payment + 1;
        }
        $sql = DB::insert("INSERT INTO ms_payment (
                id_payment,
                va_code,
                reference_id,
                created_at,
                updated_at
                )
                    values (
                      '".$new_id."',
                      '".$va_code."',
                      '".$reference_id."',
                      '".$date_now_ymdhis."',
                      '".$date_now_ymdhis."'
                    )");
        if ($sql) {
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah pembayaran berhasil";
            // $response["message"] = $message->message;
        } else {          
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah pembayaran gagal";
            // $response["message"] = $message->message;
        }
        
        return $response;
    }
    public static function createPaymentBckp($request) {
        $va_code = $request['va_code'];
        $date_now_ymd = date('Y-m-d');
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = DB::table('ms_payment')->orderby('id_payment','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_payment + 1;
        }
        $sql = DB::insert("INSERT INTO ms_payment (
                id_payment,
                va_code,
                created_at,
                updated_at
                )
                    values (
                      '".$new_id."',
                      '".$va_code."',
                      '".$date_now_ymdhis."',
                      '".$date_now_ymdhis."'
                    )");
        if ($sql) {
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah pembayaran berhasil";
            // $response["message"] = $message->message;
        } else {          
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah pembayaran gagal";
            // $response["message"] = $message->message;
        }
        
    	return $response;
    }
    public static function updatePayment($request,$reference_id) {

        $va_code = $request['va_code'];
        $pg_payment_id = $request['payment_id'];
        $top_id = DB::table('ms_payment')->where('va_code',$va_code)->orderby('created_at','desc')
                ->first();
        $id_payment = $top_id->id_payment;
        $date_now_ymdhis = date("Y-m-d H:i:s");
        // dd($birth_date);
        $sql = DB::update("UPDATE ms_payment set 
                        pg_payment_id = '$pg_payment_id',
                        updated_at = '$date_now_ymdhis'
                        where id_payment='$id_payment'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pembayaran berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Pembayaran gagal";
        }
        return $response;
    }
    public static function updatePaymentVA($reference_id,$va_code,$prefix,$trace_no,$billing_amount) {
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $sql = DB::update("UPDATE ms_payment set 
                        va_code = '$va_code',
                        prefix = '$prefix',
                        trace_no = '$trace_no',
                        billing_amount = '$billing_amount',
                        created_at = '$date_now_ymdhis',
                        updated_at = '$date_now_ymdhis'
                        where reference_id='$reference_id'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Ubah data pembayaran berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Ubah data pembayaran gagal";
        }
        return $response;
    }
    public static function getPayment($va_code) {
        
        $sql = DB::table('ms_payment')->where('va_code',$va_code)
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Payment berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Payment tidak ditemukan";
        }
        
    	return $response;
    }
    public static function getPaymentChannel($va_code) {
        
        $sql = DB::select("select * from ms_payment_channel where '".$va_code."' like '%' || prefix || '%'");
        // dd($sql->count());
        if($sql > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Payment Channel berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Payment Channel tidak ditemukan";
        }
        
        return $response;
    }
    
    public static function getDataOrder($va_code) {
        $sql = DB::table('tr_order as tr')
            ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
            ->select('tr.*','pembeli.name as pembeli_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
            ->whereRaw("'$va_code' like '%' || pembeli.phone || '%'")
            ->where('tr.status', '1')
            ->orderby('tr.created_at','desc')
            ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pesanan berhasil ditemukan";
            // dd($response);
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pesanan tidak ditemukan";
            // dd($response);
        }
        
        return $response;
    }
    public static function getDataOrderByVA($va_code) {
        $sql = DB::table('tr_order as tr')
            ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
            ->select('tr.*','pembeli.name as pembeli_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
            ->whereRaw("tr.id_order like '%$va_code%'")
            // ->where('tr.status', '1')
            ->orderby('tr.created_at','desc')
            ->get();
        // dd($sql->count());
        // dd($va_code);
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pesanan berhasil ditemukan";
            // dd($response);
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pesanan tidak ditemukan";
            // dd($response);
        }
        
        return $response;
    }
    public static function getDataOrderPayment($va_code) {
        $sql = DB::table('tr_order as tr')
            ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
            ->select('tr.*','pembeli.name as pembeli_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
            ->where("va_code", $va_code)
            ->where('tr.status', '1')
            ->orderby('tr.created_at','desc')
            ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pesanan berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pesanan tidak ditemukan";
        }
        
        return $response;
    }
    public static function getListPrefix() {
        // dd($email);
        $sql = DB::table('ms_payment_channel')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar channel berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar channel tidak ditemukan";
        }
        
        return $response;
    }
    public static function getPrefix($prefix) {
        // dd($email);
        $sql = DB::table('ms_payment_channel')
            ->where('prefix', $prefix)
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Payment channel berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Payment channel tidak ditemukan";
        }
        
        return $response;
    }
    public static function getPaymentData($reference_id) {
        
        $sql = DB::table('ms_payment')->where('reference_id',$reference_id)
                ->orderby('reference_id', 'desc')
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Data pembayaran berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Data pembayaran tidak ditemukan";
        }
        
    	return $response;
    }
}
