<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class TrOrder extends Model
{
    protected $table = 'tr_order';
    
    public static function createOrder($request) {
        $email_pembeli = $request['email_pembeli'];
        $email_cheff = $request['email_cheff'];
        $total_price = $request['total_price'];
        $tax = $request['tax'];
        $long_pembeli = $request['long_pembeli'];
        $lat_pembeli = $request['lat_pembeli'];
        $long_penjual = $request['long_penjual'];
        $lat_penjual = $request['lat_penjual'];
        $ongkir = $request['ongkir'];
        $self_pickup = $request['self_pickup'];
        $address = $request['address'];
        $status = 1;
        $date_now_ymd = date('Y-m-d');
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = TrOrder::orderby('id_order','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_order + 1;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO tr_order (
                id_order,
                email_pembeli,
                email_cheff,
                total_price,
                tax,
                long_pembeli,
                lat_pembeli,
                long_penjual,
                lat_penjual,
                ongkir,
                self_pickup,
                address,
                status,
                created_at,
                updated_at
                )
                    values (
                      '".$new_id."',
                      '".$email_pembeli."',
                      '".$email_cheff."',
                      '".$total_price."',
                      '".$tax."',
                      '".$long_pembeli."',
                      '".$lat_pembeli."',
                      '".$long_penjual."',
                      '".$lat_penjual."',
                      '".$ongkir."',
                      '".$self_pickup."',
                      '".$address."',
                      '".$status."',
                      '".$date_now_ymdhis."',
                      '".$date_now_ymdhis."'
                    )");
        if ($sql) {
            // $message = MsMessage::where('language_code','ID')
            //         ->where('message_code','create_food_success')
            //         ->first();
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah order berhasil";
            // $response["message"] = $message->message;
        } else {
            $message = MsMessage::where('language_code','ID')
                    ->where('message_code','create_food_fail')
                    ->first();              
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah order gagal";
            // $response["message"] = $message->message;
        }
        
    	return $response;
    }
    public static function createOrderNew($request) {
        // dd($request);
        $email_pembeli = $request['email_pembeli'];
        $email_cheff = $request['email_cheff'];
        $total_price = $request['total_price'];
        $tax = $request['tax'];
        $ongkir = $request['ongkir'];
        $delivery_date = $request['delivery_date'];
        $order_type = $request['order_type'];
        $status = $request['status'];
        // $check_sp = array_key_exists('self_pickup', $request);
        $cheff = MsCheff::getCheff($email_cheff);
        if ($request['self_pickup'] == 'true') {
            $id_user_address = '0';
            $address = '';
            $self_pickup = $request['self_pickup'];
            $lat_pembeli = '';
            $long_pembeli = '';
        } else {
            $id_user_address = $request['id_user_address'];
            $get_address_raw = MsAddress::getAddress($id_user_address);
            $get_address = $get_address_raw['value'];
            // dd($get_address->id_user_address);
            $address = $get_address->address_name;
            $self_pickup = 'false';
            $lat_pembeli = $get_address->latitude;
            $long_pembeli = $get_address->longitude;
        }
        $lat_penjual = $cheff['value']->latitude; 
        $long_penjual = $cheff['value']->longitude;
        // dd($cheff['value']->longitude);
        $date_now_ymd = date('Y-m-d');
        $date_now_ymdhis = date("Y-m-d H:i:s");
        
        $new_id = TrOrder::topIDOrder();
        if ($order_type == '1') {
            $delivery_date = $date_now_ymd;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO tr_order (
                id_order,
                email_pembeli,
                email_cheff,
                total_price,
                tax,
                ongkir,                
                lat_pembeli,
                long_pembeli,
                lat_penjual,
                long_penjual,
                self_pickup,
                id_user_address,
                address,
                status,
                delivery_date,
                created_at,
                updated_at

                )
                    values (
                      '".$new_id."',
                      '".$email_pembeli."',
                      '".$email_cheff."',
                      '".$total_price."',
                      '".$tax."',
                      '".$ongkir."',                      
                      '".$lat_pembeli."',
                      '".$long_pembeli."',
                      '".$lat_penjual."',
                      '".$long_penjual."',
                      '".$self_pickup."',
                      '".$id_user_address."',
                      '".$address."',
                      '".$status."',
                      '".$delivery_date."',
                      '".$date_now_ymdhis."',
                      '".$date_now_ymdhis."'
                    )");
        if ($sql) {
            // $message = MsMessage::where('language_code','ID')
            //         ->where('message_code','create_food_success')
            //         ->first();
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah order berhasil";
            // $response["message"] = $message->message;
        } else {
            // $message = MsMessage::where('language_code','ID')
            //         ->where('message_code','create_food_fail')
            //         ->first();              
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah order gagal";
            // $response["message"] = $message->message;
        }
        
        return $response;
    }
    public static function createOrderNiu($email_pembeli,$email_cheff,$total_price,$tax,$ongkir,$delivery_date,$order_type,$lat_pembeli,$long_pembeli,$address) {
        // dd($request);
        $status = 1;
        // $check_sp = array_key_exists('self_pickup', $request);
        $cheff = MsCheff::getCheff($email_cheff);
        $lat_penjual = $cheff['value']->latitude; 
        $long_penjual = $cheff['value']->longitude;
        $self_pickup = 'false';
        $id_user_address = 0;
        // dd($cheff['value']->longitude);
        $date_now_ymd = date('Y-m-d');
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $new_id = TrOrder::topIDOrder();
        // dd($new_id);
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO tr_order (
                id_order,
                email_pembeli,
                email_cheff,
                total_price,
                tax,
                ongkir,                
                lat_pembeli,
                long_pembeli,
                lat_penjual,
                long_penjual,
                self_pickup,
                id_user_address,
                address,
                status,
                order_type,
                delivery_date,
                created_at,
                updated_at

                )
                    values (
                      '".$new_id."',
                      '".$email_pembeli."',
                      '".$email_cheff."',
                      '".$total_price."',
                      '".$tax."',
                      '".$ongkir."',                      
                      '".$lat_pembeli."',
                      '".$long_pembeli."',
                      '".$lat_penjual."',
                      '".$long_penjual."',
                      '".$self_pickup."',
                      '".$id_user_address."',
                      '".$address."',
                      '".$status."',
                      '".$order_type."',
                      '".$delivery_date."',
                      '".$date_now_ymdhis."',
                      '".$date_now_ymdhis."'
                    )");
        if ($sql) {
            // $message = MsMessage::where('language_code','ID')
            //         ->where('message_code','create_food_success')
            //         ->first();
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah order berhasil";
            // $response["message"] = $message->message;
        } else {
            // $message = MsMessage::where('language_code','ID')
            //         ->where('message_code','create_food_fail')
            //         ->first();              
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah order gagal";
            // $response["message"] = $message->message;
        }
        
        return $response;
    }
    public static function getDataOrder($id_order) {
        
        $data = DB::table('tr_order as tr')
                            ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
                            ->join('ms_user as cheff', 'tr.email_cheff', '=', 'cheff.email')
                            ->select('tr.*', 'cheff.name as cheff_name', 'pembeli.phone as pembeli_phone', 'pembeli.name as pembeli_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
                            ->where('id_order', $id_order)
                            ->first();
        return $data;
    }
    public static function getListOrderPerUser($email,$offset,$limit) {
        // dd($email);
            $sql = DB::table('tr_order as tr')
                ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
                ->join('ms_user as cheff', 'tr.email_cheff', '=', 'cheff.email')
                ->join('reff_order_status as order_status', 'tr.status', '=', 'order_status.id_order_status')
                ->select('tr.*', 'cheff.name as cheff_name', 'pembeli.name as pembeli_name', 'order_status.name as order_status_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
                ->where('tr.email_pembeli', $email)
                ->where('tr.status','<>', '0')
                ->where('tr.status','<>', '2')
                ->where('tr.status','<>', '4')
                ->orderby('tr.created_at','desc')
                ->skip($offset)
                ->take($limit)
                ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar order berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar order tidak ditemukan";
        }
        
    	return $sql;
    }
    public static function getListOrderPerUserByStatus($email,$offset,$limit,$id_order_status) {
        // dd($email);
            $sql = DB::table('tr_order as tr')
                ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
                ->join('ms_user as cheff', 'tr.email_cheff', '=', 'cheff.email')
                ->join('reff_order_status as order_status', 'tr.status', '=', 'order_status.id_order_status')
                ->select('tr.*', 'cheff.name as cheff_name', 'pembeli.name as pembeli_name', 'order_status.name as order_status_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
                ->where('tr.email_pembeli', $email)
                ->where('tr.status', $id_order_status)
                ->orderby('tr.created_at','desc')
                ->skip($offset)
                ->take($limit)
                ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar order berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar order tidak ditemukan";
        }
        
    	return $response;
    }
    public static function getListOrderPercheffByStatus($email,$offset,$limit,$id_order_status) {
        // dd($email);
            $sql = DB::table('tr_order as tr')
                ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
                ->join('ms_user as cheff', 'tr.email_cheff', '=', 'cheff.email')
                ->join('reff_order_status as order_status', 'tr.status', '=', 'order_status.id_order_status')
                ->select('tr.*', 'cheff.name as cheff_name', 'pembeli.name as pembeli_name', 'order_status.name as order_status_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
                ->where('tr.email_cheff', $email)
                ->where('tr.status', $id_order_status)
                ->orderby('tr.created_at','desc')
                ->skip($offset)
                ->take($limit)
                ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar order berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar order tidak ditemukan";
        }
        
    	return $response;
    }
    public static function checkCart($email) {
        // dd($email);
        $sql = DB::table('tr_order as tr')
            ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
            ->join('ms_user as cheff', 'tr.email_cheff', '=', 'cheff.email')
            ->select('tr.*', 'cheff.name as cheff_name', 'pembeli.name as pembeli_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
            ->where('tr.email_pembeli', $email)
            ->where('tr.status', '0')
            ->orderby('tr.created_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql->count();
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pesanan aktif ditemukan";
        } else{
            $response["value"] = 0;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pesanan aktif tidak ditemukan";
        }
        
    	return $response;
    }
    public static function getCart($email) {
        // dd($email);
        $data = DB::table('tr_order as tr')
            ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
            ->join('ms_user as cheff', 'tr.email_cheff', '=', 'cheff.email')
            ->select('tr.*', 'cheff.name as cheff_name', 'pembeli.name as pembeli_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
            ->where('tr.email_pembeli', $email)
            ->where('tr.status', '0')
            ->first();
        return $data;
    }
    public static function getListOrderPerCheff($email) {
        // dd($email);
        $sql = DB::table('tr_order as tr')
            ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
            ->join('ms_user as cheff', 'tr.email_cheff', '=', 'cheff.email')
            ->join('reff_order_status as order_status', 'tr.status', '=', 'order_status.id_order_status')
            ->select('tr.*', 'cheff.name as cheff_name', 'pembeli.name as pembeli_name', 'order_status.name as order_status_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
            ->where('tr.email_cheff', $email)
            ->where('tr.status','<>', '0')
            ->orderby('tr.created_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar order berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar order tidak ditemukan";
        }
        
    	return $response;
    }
    public static function getListOrderPerCheffActive($email) {
        // dd($email);
        $sql = DB::table('tr_order as tr')
            ->join('ms_user as pembeli', 'tr.email_pembeli', '=', 'pembeli.email')
            ->join('ms_user as cheff', 'tr.email_cheff', '=', 'cheff.email')
            ->join('reff_order_status as order_status', 'tr.status', '=', 'order_status.id_order_status')
            ->select('tr.*', 'cheff.name as cheff_name', 'pembeli.name as pembeli_name', 'order_status.name as order_status_name', DB::raw('(tr.total_price + tr.tax + tr.ongkir) as total_payment'))
            ->where('tr.email_cheff', $email)
            ->where('tr.status','<>', '0')
            ->where('tr.status','<>', '4')
            ->orderby('tr.created_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar order berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar order tidak ditemukan";
        }
        
    	return $response;
    }
    public static function payOrder($request) {

        $id_order = $request['id_order'];
        $date_now_ymdhis = date("Y-m-d H:i:s");
        // dd($birth_date);
        $sql = DB::update("UPDATE tr_order set 
                        status = '2',
                        updated_at = '$date_now_ymdhis'
                        where id_order='$id_order'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pembayaran berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Pembayaran gagal";
        }
        return $response;
    }
    public static function updateVA($id_order,$va_code) {

        $id_order = $request['id_order'];
        $date_now_ymdhis = date("Y-m-d H:i:s");
        // dd($birth_date);
        $sql = DB::update("UPDATE tr_order set 
                        va_code = '$va_code',
                        updated_at = '$date_now_ymdhis'
                        where id_order='$id_order'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Penambahan Pembayaran berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Penambahan Pembayaran gagal";
        }
        return $response;
    }
    public static function changeOrderStatus($id_order,$status) {

        // $id_order = $request['id_order'];
        $date_now_ymdhis = date("Y-m-d H:i:s");
        // dd($birth_date);
        $sql = DB::update("UPDATE tr_order set 
                        status = '$status',
                        updated_at = '$date_now_ymdhis'
                        where id_order='$id_order'");
        return $sql;
    }
    public static function cancelOldCart($email_pembeli) {

        // $id_order = $request['id_order'];
        $date_now_ymdhis = date("Y-m-d H:i:s");
        // dd($birth_date);
        $sql = DB::update("UPDATE tr_order set 
                        status = '6',
                        updated_at = '$date_now_ymdhis'
                        where email_pembeli='$email_pembeli'
                        and status = '1'");
        return $sql;
    }
    public static function topIDOrder() {
        // dd($email);
        $now_id = 'ETL'.date('ymd');
        $sql = DB::table('tr_order as tr')
            ->whereRaw("tr.id_order like '%$now_id%'")
            ->orderby('tr.id_order','desc')
            ->first();
            
        if($sql != null){
            $top_id = substr($sql->id_order,9,6) + 1;
            $new_id = '';
            if($top_id > 100000){
                $new_id = $now_id . $top_id;
            } else if($top_id >= 10000){
                $new_id = $now_id .'0'. $top_id;
            } else if($top_id >= 1000){
                $new_id = $now_id .'00'. $top_id;
            } else if($top_id >= 100){
                $new_id = $now_id .'000'. $top_id;
            } else if($top_id >= 10){
                $new_id = $now_id .'0000'. $top_id;
            } else if($top_id < 10){
                $new_id = $now_id .'00000'. $top_id;
            } else {
                $new_id = $now_id . $top_id;
            }
        // dd($new_id);
        } else {
            $new_id = $now_id.'000001';
        }
        
    	return $new_id;
    }
    public static function updateOrderVA($id_order,$va_code,$prefix) {

        $date_now_ymdhis = date("Y-m-d H:i:s");
        // dd($birth_date);
        $sql = DB::update("UPDATE tr_order set 
                        va_code = '$va_code',
                        prefix = '$prefix',
                        updated_at = '$date_now_ymdhis'
                        where id_order='$id_order'");
        return $sql;
    }
    public static function checkOngkir() {
        
        $data = DB::table('ms_app_setting')
                            ->where('code_setting', 'shipping_cost')
                            ->first();
        return $data->content;
    }
    public static function scheduleExpiredPayment($status) {

        // $id_order = $request['id_order'];
        $date_now_ymdhis = date("Y-m-d H:i:s");
        // dd($birth_date);
        $sql = DB::update("UPDATE tr_order set 
                        status = '$status',
                        updated_at = '$date_now_ymdhis'
                        where created_at + interval '1' day < '$date_now_ymdhis'
                        and status = '1'
                        ");
        return $sql;
    }
}
