<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MsFollow extends Model
{
    protected $table = 'ms_follow';

    public static function createFollow($request) {
        $email_follower = $request['email_follower'];
        $email_following = $request['email_following'];
        
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = MsFollow::orderby('id_follow','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_follow + 1;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO ms_follow (
                id_follow,
                following,
                follower,
                created_at
                )
                    values (
                      '".$new_id."',
                      '".$email_following."',
                      '".$email_follower."',
                      '".$date_now_ymdhis."'
                    )");
        if($sql){
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Follow berhasil";
        } else{
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Follow gagal";
        }
    	return $response;
    }
    public static function getListFollowing($email) {
        // dd($id_food);
        $sql = DB::table('ms_follow')
            ->join('ms_user', 'ms_follow.following', '=', 'ms_user.email')
            ->select('ms_follow.*', 'ms_user.name as following_name', 'ms_user.image as following_image')
            ->where('ms_follow.follower', $email)
            ->orderby('ms_follow.created_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar follow berhasil ditemukan";
        } else{
            $response["value"] = $sql;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar follow tidak ditemukan";
        }
        
        return $response;
    }
    public static function getListFollower($email) {
        // dd($email);
        $sql = DB::table('ms_follow')
            ->join('ms_user', 'ms_follow.follower', '=', 'ms_user.email')
            ->select('ms_follow.*', 'ms_user.name as follower_name', 'ms_user.image as follower_image')
            ->where('ms_follow.following', $email)
            ->orderby('ms_follow.created_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar follower berhasil ditemukan";
        } else{
            $response["value"] = $sql;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar follower tidak ditemukan";
        }
        
        return $response;
    }
    public static function getFollow($id_follow) {
        
        $sql = MsFollow::where('id_follow',$id_follow)
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Follow berhasil ditemukan";
        } else{
            $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Follow tidak ditemukan";
        }
        
    	return $response;
    }
    public static function deleteFollow($following,$follower) {
        
        $deleteFollow = DB::table('ms_follow')
                    ->where('following', $following)
                    ->where('follower', $follower)
                    ->delete();
        if ($deleteFollow) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Sukses hapus data";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Gagal hapus data";
        }
        return $response;
    }
    public static function checkFollow($following,$follower) {
        
        $checkFollow = DB::table('ms_follow')
                    ->where('following', $following)
                    ->where('follower', $follower)
                    ->get();
        if ($checkFollow->count() > 0) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Followed";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Not followed";
        }
        return $response;
    }
}
