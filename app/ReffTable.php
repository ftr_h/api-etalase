<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ReffTable extends Model
{
    protected $table = 'ms_user';
    public static function getListBank() {
        
        // dd($search_val);
        $sql = DB::table('reff_bank')
                          ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Bank berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Bank tidak ditemukan";
        }
    	return $response;
    }
    public static function getListBelajar()
    {
        // dd('okok');
        $sql = DB::table('reff_belajar_memasak')
                          ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Belajar berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Belajar tidak ditemukan";
        }
        return $response;
    }
    public static function getListCategoryKoki()
    {
        // dd('okok');
        $sql = DB::table('reff_category_koki')
                          ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Kategori koki berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Kategori koki tidak ditemukan";
        }
        return $response;
    }
    public static function getListCuisine()
    {
        // dd('okok');
        $sql = DB::table('reff_cuisine')
                          ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Cuisine berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Cuisine tidak ditemukan";
        }
        return $response;
    }
    public static function getListKeahlian()
    {
        // dd('okok');
        $sql = DB::table('reff_keahlian')
                          ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Keahlian berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Keahlian tidak ditemukan";
        }
        return $response;
    }
    public static function getListTypeDiet()
    {
        // dd('okok');
        $sql = DB::table('reff_type_diet')
                          ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Type Diet berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Type Diet tidak ditemukan";
        }
        return $response;
    }
    public static function getListProvinsi()
    {
        $sql = DB::table('reff_provinsi')
                          ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Provinsi berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Provinsi tidak ditemukan";
        }
        return $response;
    }
    public static function getListKota($id_provinsi)
    {
        $sql = DB::table('reff_kodepos')
                        ->select('kota_kab')
                        ->where('id_provinsi', $id_provinsi)
                        ->groupBy('kota_kab')
                        ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Kota berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Kota tidak ditemukan";
        }
        return $response;
    }
    public static function getListKecamatan($kota_kab)
    {
        $sql = DB::table('reff_kodepos')
                        ->select('kecamatan')
                        ->where('kota_kab', $kota_kab)
                        ->groupBy('kecamatan')
                        ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Kecamatan berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Kecamatan tidak ditemukan";
        }
        return $response;
    }
    public static function getListKodepos($kecamatan)
    {
        $sql = DB::table('reff_kodepos')
                        ->select('kodepos')
                        ->where('kecamatan', $kecamatan)
                        ->groupBy('kodepos')
                        ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Kodepos berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Kodepos tidak ditemukan";
        }
        return $response;
    }
    public static function getListServingType()
    {
        $sql = DB::table('reff_serving_type')
                        ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Jenis porsi berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Jenis porsi tidak ditemukan";
        }
        return $response;
    }
    public static function getListHidanganType()
    {
        $sql = DB::table('reff_hidangan_type')
                        ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Jenis hidangan berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Jenis hidangan tidak ditemukan";
        }
        return $response;
    }
    public static function getListEstCooking()
    {
        $sql = DB::table('reff_est_cooking')
                        ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Estimasi memasak berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Estimasi memasak tidak ditemukan";
        }
        return $response;
    }
    public static function getListCookingMethod()
    {
        $sql = DB::table('reff_cooking_method')
                        ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Metode memasak berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Metode memasak tidak ditemukan";
        }
        return $response;
    }
    public static function getListOrderType()
    {
        $sql = DB::table('reff_order_type')
                        ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tipe pesanan berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tipe pesanan tidak ditemukan";
        }
        return $response;
    }
}
