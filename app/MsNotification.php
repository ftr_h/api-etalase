<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MsNotification extends Model
{
    protected $table = 'ms_user_notification';

    public static function createNotif($request) {
        $email_user = $request['email'];
        $title = $request['title'];
        $message = $request['message'];
        $date_now_ymd = date('Y-m-d');
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = MsNotification::orderby('id_user_notification','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_user_notification + 1;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO ms_user_notification (
                id_user_notification,
                email_user,
                title,
                message,
                status,
                created_at,
                updated_at
                )
                    values (
                      '".$new_id."',
                      '".$email_user."',
                      '".$title."',
                      '".$message."',
                      '0',
                      '".$date_now_ymdhis."',
                      '".$date_now_ymdhis."'
                    )");
        if ($sql) {
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah notifikasi berhasil";
            // $response["message"] = $message->message;
        } else {          
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah notifikasi gagal";
            // $response["message"] = $message->message;
        }
        
    	return $response;
    }
    public static function createPayment($request, $reference_id) {
        $va_code = $request['va_code'];
        $date_now_ymd = date('Y-m-d');
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = DB::table('ms_payment')->orderby('id_payment','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_payment + 1;
        }
        $sql = DB::insert("INSERT INTO ms_payment (
                id_payment,
                va_code,
                reference_id,
                created_at,
                updated_at
                )
                    values (
                      '".$new_id."',
                      '".$va_code."',
                      '".$reference_id."',
                      '".$date_now_ymdhis."',
                      '".$date_now_ymdhis."'
                    )");
        if ($sql) {
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah pembayaran berhasil";
            // $response["message"] = $message->message;
        } else {          
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah pembayaran gagal";
            // $response["message"] = $message->message;
        }
        
        return $response;
    }
    public static function createPaymentBckp($request) {
        $va_code = $request['va_code'];
        $date_now_ymd = date('Y-m-d');
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = DB::table('ms_payment')->orderby('id_payment','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_payment + 1;
        }
        $sql = DB::insert("INSERT INTO ms_payment (
                id_payment,
                va_code,
                created_at,
                updated_at
                )
                    values (
                      '".$new_id."',
                      '".$va_code."',
                      '".$date_now_ymdhis."',
                      '".$date_now_ymdhis."'
                    )");
        if ($sql) {
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah pembayaran berhasil";
            // $response["message"] = $message->message;
        } else {          
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah pembayaran gagal";
            // $response["message"] = $message->message;
        }
        
    	return $response;
    }
    public static function updatePayment($request,$reference_id) {

        $id_payment = $request['payment_id'];
        $va_code = $request['va_code'];
        $date_now_ymdhis = date("Y-m-d H:i:s");
        // dd($birth_date);
        $sql = DB::update("UPDATE ms_payment set 
                        id_payment = '$id_payment',
                        reference_id = '$reference_id',
                        updated_at = '$date_now_ymdhis'
                        where va_code='$va_code'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pembayaran berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Pembayaran gagal";
        }
        return $response;
    }
    public static function getPayment($va_code) {
        
        $sql = DB::table('ms_payment')->where('va_code',$va_code)
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Payment berhasil ditemukan";
        } else{
            $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Payment tidak ditemukan";
        }
        
    	return $response;
    }
    public static function getListNotifPerUser($email) {
        // dd($email);
        $sql = DB::table('ms_user_notification as nt')
            ->join('ms_user', 'nt.email_user', '=', 'ms_user.email')
            ->select('nt.*', 'ms_user.name as user_name')
            ->where('nt.email_user', $email)
            ->orderby('nt.created_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar notif berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar notif tidak ditemukan";
        }
        
        return $response;
    }
    public static function countNotifUnread($email) {
        // dd($email);
        $sql = DB::table('ms_user_notification as nt')
            ->join('ms_user', 'nt.email_user', '=', 'ms_user.email')
            ->select('ms_user.name as user_name','ms_user.email as email_user')
            ->where('nt.email_user', $email)
            ->where('nt.status', '0')
            ->count();
        if($sql){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Berhasil hitung notif belum terbaca";
        } else{
            $response["value"] = 0;
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Notif kosong";
        }
        
        return $response;
    }
    public static function getNotif($id_user_notification) {
        
        $sql = MsNotification::where('id_user_notification',$id_user_notification)
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Notif berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Notif tidak ditemukan";
        }
        
    	return $response;
    }
    public static function readNotif($request) {

        $id_user_notification = $request['id_user_notification'];
        $date_now_ymdhis = date("Y-m-d H:i:s");
        // dd($birth_date);
        $sql = DB::update("UPDATE ms_user_notification set 
                        status = '1',
                        updated_at = '$date_now_ymdhis'
                        where id_user_notification='$id_user_notification'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Notif dibaca berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Notif tidak terupdate";
        }
    	return $response;
    }
}
