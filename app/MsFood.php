<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class MsFood extends Model
{
    protected $table = 'ms_food';
    
    public static function createFoodNew($request) {
        
        $email_user = $request['email'];
        $menu_name = $request['menu_name'];
        $cover_dish = $request['cover_dish'];
        $price = $request['price'];
        // $id_hidangan_type = $request['id_hidangan_type'];
        $id_est_cooking = $request['id_est_cooking'];
        $id_cuisine = $request['id_cuisine'];
        $id_keahlian = $request['id_keahlian'];
        $about_food = $request['about_food'];
        $id_cooking_method = $request['id_cooking_method'];
        $id_type_diet = $request['id_type_diet'];
        $ingredient = $request['ingredient'];
        $oil = $request['oil'];
        $halal = $request['halal'];
        $share_recipe = $request['share_recipe'];
        $msg_free = $request['msg_free'];
        // $visibility = $request['visibility'];
        $recipe = $request['recipe'];
        $id_serving_type = $request['id_serving_type'];
        $order_type = $request['order_type'];
        $available_at = $request['available_at'];
        $limit_order = $request['limit_order'];
        $date_now_ymd = date('Y-m-d');
        $top_id = MsFood::orderby('id_food','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_food + 1;
        }
        if($order_type == '1'){
            $sql = DB::insert("INSERT INTO ms_food (
                    id_food,
                    email_user,
                    menu_name,
                    cover_dish,
                    price,
                    id_est_cooking,
                    id_cuisine,
                    id_keahlian,
                    about_food,
                    id_cooking_method,
                    id_type_diet,
                    ingredient,
                    oil,
                    halal,
                    share_recipe,
                    msg_free,
                    recipe,
                    id_serving_type,
                    visibility,
                    order_type,
                    available_at,
                    created_at,
                    updated_at)
                        values (
                          '".$new_id."',
                          '".$email_user."',
                          '".$menu_name."',
                          '".$cover_dish."',
                          '".$price."',
                          '".$id_est_cooking."',
                          '".$id_cuisine."',
                          '".$id_keahlian."',
                          '".$about_food."',
                          '".$id_cooking_method."',
                          '".$id_type_diet."',
                          '".$ingredient."',
                          '".$oil."',
                          '".$halal."',
                          '".$share_recipe."',
                          '".$msg_free."',
                          '".$recipe."',
                          '".$id_serving_type."',
                          'false',
                          '".$order_type."',
                          '".$date_now_ymd."',
                          '".$date_now_ymd."',
                          '".$date_now_ymd."'
                        )");
        } else if($order_type == '2'){
        // dd($check_email->count());
            $sql = DB::insert("INSERT INTO ms_food (
                    id_food,
                    email_user,
                    menu_name,
                    cover_dish,
                    price,
                    id_est_cooking,
                    id_cuisine,
                    id_keahlian,
                    about_food,
                    id_cooking_method,
                    id_type_diet,
                    ingredient,
                    oil,
                    halal,
                    share_recipe,
                    msg_free,
                    recipe,
                    id_serving_type,
                    visibility,
                    order_type,
                    limit_order,
                    available_at,
                    created_at,
                    updated_at)
                        values (
                          '".$new_id."',
                          '".$email_user."',
                          '".$menu_name."',
                          '".$cover_dish."',
                          '".$price."',
                          '".$id_est_cooking."',
                          '".$id_cuisine."',
                          '".$id_keahlian."',
                          '".$about_food."',
                          '".$id_cooking_method."',
                          '".$id_type_diet."',
                          '".$ingredient."',
                          '".$oil."',
                          '".$halal."',
                          '".$share_recipe."',
                          '".$msg_free."',
                          '".$recipe."',
                          '".$id_serving_type."',
                          'false',
                          '".$order_type."',
                          '".$limit_order."',
                          '".$available_at."',
                          '".$date_now_ymd."',
                          '".$date_now_ymd."'
                        )");
        } else{;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masukkan tipe pesanan dengan benar";
    	    return $response;
        }
        if ($sql) {
            // $message = MsMessage::where('language_code','ID')
            //         ->where('message_code','create_food_success')
            //         ->first();
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah masakan berhasil";
            // $response["message"] = $message->message;
        } else {
            $message = MsMessage::where('language_code','ID')
                    ->where('message_code','create_food_fail')
                    ->first();              
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah masakan gagal";
            // $response["message"] = $message->message;
        }
    	return $response;
    }
    public static function updateFoodNew($request) {
        
        $order_type = $request['order_type'];
        $available_at = $request['available_at'];
        $limit_order = $request['limit_order'];
        
        $id_food = $request['id_food'];
        $menu_name = $request['menu_name'];
        $cover_dish = $request['cover_dish'];
        $price = $request['price'];
        $id_est_cooking = $request['id_est_cooking'];
        $id_cuisine = $request['id_cuisine'];
        $id_keahlian = $request['id_keahlian'];
        $about_food = $request['about_food'];
        $id_cooking_method = $request['id_cooking_method'];
        $id_type_diet = $request['id_type_diet'];
        $oil = $request['oil'];
        $halal = $request['halal'];
        $share_recipe = $request['share_recipe'];
        $msg_free = $request['msg_free'];
        $recipe = $request['recipe'];
        $id_serving_type = $request['id_serving_type'];
        $date_now_ymd = date('Y-m-d');
        if($order_type == '1'){
            $sql = DB::update("UPDATE ms_food set 
                menu_name = '$menu_name',
                cover_dish = '$cover_dish',
                price = '$price',
                id_est_cooking = '$id_est_cooking',
                id_cuisine = '$id_cuisine',
                id_keahlian = '$id_keahlian',
                about_food = '$about_food',
                id_cooking_method = '$id_cooking_method',
                id_type_diet = '$id_type_diet',
                oil = '$oil',
                halal = '$halal',
                share_recipe = '$share_recipe',
                msg_free = '$msg_free',
                recipe = '$recipe',
                id_serving_type = '$id_serving_type',
                order_type = '$order_type',
                updated_at = '$date_now_ymd'
                where id_food='$id_food'");
        } else if($order_type == '2'){
        // dd($check_email->count());
            $sql = DB::update("UPDATE ms_food set 
                menu_name = '$menu_name',
                cover_dish = '$cover_dish',
                price = '$price',
                id_est_cooking = '$id_est_cooking',
                id_cuisine = '$id_cuisine',
                id_keahlian = '$id_keahlian',
                about_food = '$about_food',
                id_cooking_method = '$id_cooking_method',
                id_type_diet = '$id_type_diet',
                oil = '$oil',
                halal = '$halal',
                share_recipe = '$share_recipe',
                msg_free = '$msg_free',
                recipe = '$recipe',
                id_serving_type = '$id_serving_type',
                order_type = '$order_type',
                limit_order = '$limit_order',
                available_at = '$available_at',
                updated_at = '$date_now_ymd'
                where id_food='$id_food'");
        } else{;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masukkan tipe pesanan dengan benar";
    	    return $response;
        }
        if ($sql) {
            $response["value"] = $id_food;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Ubah data masakan berhasil";
            // $response["message"] = $message->message;
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Ubah data masakan gagal";
            // $response["message"] = $message->message;
        }
    	return $response;
    }
    public static function signatureFood($email_user,$id_food,$cover_dish_reason) {
        $date_now_ymd = date("Y-m-d");
        // dd($birth_date);
        $sql_clear = DB::update("UPDATE ms_food set 
                        cover_dish = 'false',
                        updated_at = '$date_now_ymd'
                        where email_user ='$email_user'");
        if ($sql_clear) {
            $sql_default = DB::update("UPDATE ms_food set 
                            cover_dish = 'true',
                            cover_dish_reason = '$cover_dish_reason',
                            updated_at = '$date_now_ymd'
                            where id_food ='$id_food'");
            if ($sql_default) {
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Kreasi Signature berhasil dipilih";
            } else {
                // $response["value"] = $request->all();
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Kreasi Signature gagal dipilih";
            }
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Kreasi tidak terupdate";
        }
        return $response;
    }

    public static function createFood($request) {
        
        $email_user = $request['email'];
        $menu_name = $request['menu_name'];
        $cover_dish = $request['cover_dish'];
        $price = $request['price'];
        $id_hidangan_type = $request['id_hidangan_type'];
        $id_est_cooking = $request['id_est_cooking'];
        $id_cuisine = $request['id_cuisine'];
        $id_keahlian = $request['id_keahlian'];
        $about_food = $request['about_food'];
        $id_cooking_method = $request['id_cooking_method'];
        $id_type_diet = $request['id_type_diet'];
        $ingredient = $request['ingredient'];
        $oil = $request['oil'];
        $halal = $request['halal'];
        $share_recipe = $request['share_recipe'];
        $msg_free = $request['msg_free'];
        $visibility = $request['visibility'];
        $recipe = $request['recipe'];
        $id_serving_type = $request['id_serving_type'];
        $date_now_ymd = date('Y-m-d');
        $top_id = MsFood::orderby('id_food','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_food + 1;
        }
        if($order_type == '2'){
        
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO ms_food (
                id_food,
                email_user,
                menu_name,
                cover_dish,
                price,
                id_hidangan_type,
                id_est_cooking,
                id_cuisine,
                id_keahlian,
                about_food,
                id_cooking_method,
                id_type_diet,
                ingredient,
                oil,
                halal,
                share_recipe,
                msg_free,
                recipe,
                id_serving_type,
                visibility,
                created_at,
                updated_at)
                    values (
                      '".$new_id."',
                      '".$email_user."',
                      '".$menu_name."',
                      '".$cover_dish."',
                      '".$price."',
                      '".$id_hidangan_type."',
                      '".$id_est_cooking."',
                      '".$id_cuisine."',
                      '".$id_keahlian."',
                      '".$about_food."',
                      '".$id_cooking_method."',
                      '".$id_type_diet."',
                      '".$ingredient."',
                      '".$oil."',
                      '".$halal."',
                      '".$share_recipe."',
                      '".$msg_free."',
                      '".$recipe."',
                      '".$id_serving_type."',
                      '".$visibility."',
                      '".$date_now_ymd."',
                      '".$date_now_ymd."'
                    )");
        if ($sql) {
            // $message = MsMessage::where('language_code','ID')
            //         ->where('message_code','create_food_success')
            //         ->first();
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah masakan berhasil";
            // $response["message"] = $message->message;
        } else {
            $message = MsMessage::where('language_code','ID')
                    ->where('message_code','create_food_fail')
                    ->first();              
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah masakan gagal";
            // $response["message"] = $message->message;
        }
        
    	return $response;
    }
    public static function updateFood($request) {
        
        $id_food = $request['id_food'];
        $menu_name = $request['menu_name'];
        $cover_dish = $request['cover_dish'];
        $price = $request['price'];
        $id_est_cooking = $request['id_est_cooking'];
        $id_cuisine = $request['id_cuisine'];
        $id_keahlian = $request['id_keahlian'];
        $about_food = $request['about_food'];
        $id_cooking_method = $request['id_cooking_method'];
        $id_type_diet = $request['id_type_diet'];
        $oil = $request['oil'];
        $halal = $request['halal'];
        $share_recipe = $request['share_recipe'];
        $msg_free = $request['msg_free'];
        $recipe = $request['recipe'];
        $id_serving_type = $request['id_serving_type'];
        $date_now_ymd = date('Y-m-d');
        // dd($birth_date);
        $sql = DB::update("UPDATE ms_food set 
                        menu_name = '$menu_name',
                        cover_dish = '$cover_dish',
                        price = '$price',
                        id_est_cooking = '$id_est_cooking',
                        id_cuisine = '$id_cuisine',
                        id_keahlian = '$id_keahlian',
                        about_food = '$about_food',
                        id_cooking_method = '$id_cooking_method',
                        id_type_diet = '$id_type_diet',
                        oil = '$oil',
                        halal = '$halal',
                        share_recipe = '$share_recipe',
                        msg_free = '$msg_free',
                        recipe = '$recipe',
                        id_serving_type = '$id_serving_type',
                        updated_at = '$date_now_ymd'
                        where id_food='$id_food'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Ubah data masakan berhasil";
        } else {
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Ubah data masakan gagal";
        }
        
        return $response;
    }
    public static function deleteFood($id_food) {
        $date_now_ymd = date('Y-m-d');
        // dd($birth_date);
          $sql = DB::table('ms_food')->where('id_food', $id_food)->delete();
            $sql_gallery = DB::table('ms_food_gallery')->where('id_food', $id_food)->delete();
            // $sql_review = DB::table('ms_review')->where('id_food', $id_food)->delete();
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Hapus kreasi berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Hapus kreasi gagal";
        }
    	return $response;
    }
    public static function deleteFoodGallery($id_food_gallery) {
        $date_now_ymd = date('Y-m-d');
        // dd($birth_date);
          $sql = DB::table('ms_food_gallery')->where('id_food_gallery', $id_food_gallery)->delete();
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Hapus gallery berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Hapus gallery gagal";
        }
    	return $response;
    }
    public static function getListFoodSearch($search_val,$lat,$long,$offset,$limit) {
        
        $sql = MsFood::join('ms_cheff', 'ms_food.email_user', '=', 'ms_cheff.email_user')
            ->join('ms_user', 'ms_food.email_user', '=', 'ms_user.email')
            ->join('reff_keahlian', 'ms_food.id_keahlian', '=', 'reff_keahlian.id_keahlian')
            ->join('reff_order_type', 'ms_food.order_type', '=', 'reff_order_type.id_order_type')
            ->join('reff_est_cooking', 'ms_food.id_est_cooking', '=', 'reff_est_cooking.id_est_cooking')
            ->join('reff_cuisine', 'ms_food.id_cuisine', '=', 'reff_cuisine.id_cuisine')
            ->join('reff_cooking_method', 'ms_food.id_cooking_method', '=', 'reff_cooking_method.id_cooking_method')
            ->join('reff_type_diet', 'ms_food.id_type_diet', '=', 'reff_type_diet.id_type_diet')
            ->join('reff_serving_type', 'ms_food.id_serving_type', '=', 'reff_serving_type.id_serving_type')
            ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
            ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
            ->select('ms_food.*',DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food and ms_food_gallery.link IS NOT NULL
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"),'reff_keahlian.name as type_hidangan_name','reff_est_cooking.name as est_cooking_name','reff_cuisine.name as cuisine_name','reff_cooking_method.name as cooking_method_name','reff_type_diet.name as type_diet_name','reff_serving_type.name as serving_type_name','reff_order_type.name as order_type_name',DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"),  DB::raw('coalesce(SUM(rate), 0) as total_rate'), DB::raw('COUNT(rate) as total_review'),DB::raw('coalesce(AVG(rate), 0) as food_rating'), DB::raw("
                            (6371  * ACOS(COS(RADIANS($lat))
                                   * COS(RADIANS(latitude::numeric))
                                   * COS(RADIANS($long) - RADIANS(longitude::numeric))
                                   + SIN(RADIANS($lat))
                                   * SIN(RADIANS(latitude::numeric)))) AS distance"))
            ->groupBy('ms_cheff.id_cheff')
            ->groupBy('reff_keahlian.name')
            ->groupBy('reff_est_cooking.name')
            ->groupBy('reff_cuisine.name')
            ->groupBy('reff_cooking_method.name')
            ->groupBy('reff_type_diet.name')
            ->groupBy('reff_serving_type.name')
            ->groupBy('reff_order_type.name')
            ->groupBy('ms_user.image')
            ->groupBy('ms_user.id_user')
            ->groupBy('ms_food.id_food')
            ->Where('ms_cheff.verified', '1')
            ->Where('ms_food.visibility', 'true')
            ->whereRaw('LOWER(menu_name) LIKE ? ',['%'.trim(strtolower($search_val)).'%'])
            ->orWhereRaw('LOWER(ms_user.name) LIKE ? ',['%'.trim(strtolower($search_val)).'%'])
            ->Where('ms_cheff.verified', '1')
            ->Where('ms_food.visibility', 'true')
            ->orderby('distance', 'asc')
            ->orderby('food_rating', 'desc')
            ->skip($offset)
            ->take($limit)
            ->get();
        // dd($sql);
        return $sql;
    }
    public static function getListFoodSearchNew($search_val,$lat,$long,$offset,$limit) {
        
        $sql = MsCheff::join('ms_food', 'ms_food.email_user', '=', 'ms_cheff.email_user')
            ->join('ms_user', 'ms_food.email_user', '=', 'ms_user.email')
            ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
            ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
            ->select('ms_cheff.email_user',DB::raw('coalesce(AVG(rate), 0) as cheff_rating'), 
            DB::raw("(6371  * ACOS(COS(RADIANS($lat))
                                   * COS(RADIANS(latitude::numeric))
                                   * COS(RADIANS($long) - RADIANS(longitude::numeric))
                                   + SIN(RADIANS($lat))
                                   * SIN(RADIANS(latitude::numeric)))) AS distance"))
            ->groupBy('ms_cheff.id_cheff')
            ->groupBy('ms_user.id_user')
            ->Where('ms_cheff.verified', '1')
            ->Where('ms_food.visibility', 'true')
            ->whereRaw('LOWER(menu_name) LIKE ? ',['%'.trim(strtolower($search_val)).'%'])
            ->orWhereRaw('LOWER(ms_user.name) LIKE ? ',['%'.trim(strtolower($search_val)).'%'])
            ->Where('ms_cheff.verified', '1')
            ->Where('ms_food.visibility', 'true')
            ->orderby('distance', 'asc')
            ->orderby('cheff_rating', 'desc')
            ->skip($offset)
            ->take($limit)
            ->get();
        // dd($sql);
        return $sql;
    }
    public static function getListFoodSearchPerCheff($search_val,$email) {
        
        $sql = DB::table('ms_food')
                ->join('ms_user', 'ms_food.email_user', '=', 'ms_user.email')
                ->join('reff_keahlian', 'ms_food.id_keahlian', '=', 'reff_keahlian.id_keahlian')
                ->join('reff_order_type', 'ms_food.order_type', '=', 'reff_order_type.id_order_type')
                ->join('reff_est_cooking', 'ms_food.id_est_cooking', '=', 'reff_est_cooking.id_est_cooking')
                ->join('reff_cuisine', 'ms_food.id_cuisine', '=', 'reff_cuisine.id_cuisine')
                ->join('reff_cooking_method', 'ms_food.id_cooking_method', '=', 'reff_cooking_method.id_cooking_method')
                ->join('reff_type_diet', 'ms_food.id_type_diet', '=', 'reff_type_diet.id_type_diet')
                ->join('reff_serving_type', 'ms_food.id_serving_type', '=', 'reff_serving_type.id_serving_type')
                ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
                ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
                ->select('ms_food.*','ms_user.name as cheff_name','ms_user.email as email_user',  DB::raw('coalesce(SUM(rate), 0) as total_rate'), DB::raw('COUNT(rate) as total_review'),DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food and ms_food_gallery.link IS NOT NULL
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"),DB::raw('coalesce(AVG(rate), 0) as food_rating'),'reff_keahlian.name as keahlian_name','reff_est_cooking.name as est_cooking_name','reff_cuisine.name as cuisine_name','reff_cooking_method.name as cooking_method_name','reff_type_diet.name as type_diet_name','reff_serving_type.name as serving_type_name','reff_order_type.name as order_type_name')
                ->groupBy('ms_food.id_food')
                ->groupBy('ms_user.name')
                ->groupBy('ms_user.email')
                ->groupBy('reff_keahlian.name')
                ->groupBy('reff_est_cooking.name')
                ->groupBy('reff_cuisine.name')
                ->groupBy('reff_cooking_method.name')
                ->groupBy('reff_type_diet.name')
                ->groupBy('reff_serving_type.name')
                ->groupBy('reff_order_type.name')
                ->orderBy('food_rating')
                ->where('ms_food.email_user',$email)
                ->whereRaw('LOWER(menu_name) LIKE ? ',['%'.trim(strtolower($search_val)).'%'])
                ->get();
        // dd($sql);
        return $sql;
    }
    public static function getListFoodSearchGallery($id_food) {
        
        $sql_food_image = DB::table('ms_food_gallery')
            ->where('id_food',$id_food)
            ->where('link','<>','null')
            ->orderBy('is_default', 'desc')
            ->get();
        
    	return $sql_food_image;
    }
    public static function getFood($id_food) {
        
        // $sql = MsFood::where('id_food',$id_food)
        $sql = DB::table('ms_food')
                ->join('ms_user', 'ms_food.email_user', '=', 'ms_user.email')
                ->join('reff_keahlian', 'ms_food.id_keahlian', '=', 'reff_keahlian.id_keahlian')
                ->join('reff_order_type', 'ms_food.order_type', '=', 'reff_order_type.id_order_type')
                ->join('reff_est_cooking', 'ms_food.id_est_cooking', '=', 'reff_est_cooking.id_est_cooking')
                ->join('reff_cuisine', 'ms_food.id_cuisine', '=', 'reff_cuisine.id_cuisine')
                ->join('reff_cooking_method', 'ms_food.id_cooking_method', '=', 'reff_cooking_method.id_cooking_method')
                ->join('reff_type_diet', 'ms_food.id_type_diet', '=', 'reff_type_diet.id_type_diet')
                ->join('reff_serving_type', 'ms_food.id_serving_type', '=', 'reff_serving_type.id_serving_type')
                ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
                ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
                ->select('ms_food.*','ms_user.name as cheff_name','ms_user.email as email_user',DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food and ms_food_gallery.link IS NOT NULL
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"),
                    'reff_keahlian.name as keahlian_name',
                    'reff_est_cooking.name as est_cooking_name',
                    'reff_cuisine.name as cuisine_name',
                    'reff_cooking_method.name as cooking_method_name',
                    'reff_type_diet.name as type_diet_name',
                    'reff_serving_type.name as serving_type_name',
                    'reff_order_type.name as order_type_name', 
                    DB::raw('coalesce(SUM(rate), 0) as total_rate'), 
                    DB::raw('COUNT(rate) as total_review'),
                    DB::raw('coalesce(AVG(rate), 0) as food_rating')
                )
                ->groupBy('ms_food.id_food')
                ->groupBy('ms_user.name')
                ->groupBy('ms_user.email')
                ->groupBy('reff_keahlian.name')
                ->groupBy('reff_est_cooking.name')
                ->groupBy('reff_cuisine.name')
                ->groupBy('reff_cooking_method.name')
                ->groupBy('reff_type_diet.name')
                ->groupBy('reff_serving_type.name')
                ->groupBy('reff_order_type.name')
                ->where('ms_food.id_food',$id_food)
                ->first();
        // dd($sql);
        if($sql){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan tidak ditemukan";
        }
        
        return $response;
    }
    public static function getFoodTest($id_food) {
        
        // $sql = MsFood::where('id_food',$id_food)
        $sql = DB::table('ms_food')
                ->join('ms_user', 'ms_food.email_user', '=', 'ms_user.email')
                ->join('reff_keahlian', 'ms_food.id_keahlian', '=', 'reff_keahlian.id_keahlian')
                ->join('reff_order_type', 'ms_food.order_type', '=', 'reff_order_type.id_order_type')
                ->join('reff_est_cooking', 'ms_food.id_est_cooking', '=', 'reff_est_cooking.id_est_cooking')
                ->join('reff_cuisine', 'ms_food.id_cuisine', '=', 'reff_cuisine.id_cuisine')
                ->join('reff_cooking_method', 'ms_food.id_cooking_method', '=', 'reff_cooking_method.id_cooking_method')
                ->join('reff_type_diet', 'ms_food.id_type_diet', '=', 'reff_type_diet.id_type_diet')
                ->join('reff_serving_type', 'ms_food.id_serving_type', '=', 'reff_serving_type.id_serving_type')
                ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
                ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
                ->select('ms_food.*','ms_user.name as cheff_name','ms_user.email as email_user',  DB::raw('coalesce(SUM(rate), 0) as total_rate'), DB::raw('COUNT(rate) as total_review'),DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food and ms_food_gallery.link IS NOT NULL
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"),DB::raw('coalesce(AVG(rate), 0) as food_rating'),'reff_keahlian.name as keahlian_name','reff_est_cooking.name as est_cooking_name','reff_cuisine.name as cuisine_name','reff_cooking_method.name as cooking_method_name','reff_type_diet.name as type_diet_name','reff_serving_type.name as serving_type_name','reff_order_type.name as order_type_name')
                ->groupBy('ms_food.id_food')
                ->groupBy('ms_user.name')
                ->groupBy('ms_user.email')
                ->groupBy('reff_keahlian.name')
                ->groupBy('reff_est_cooking.name')
                ->groupBy('reff_cuisine.name')
                ->groupBy('reff_cooking_method.name')
                ->groupBy('reff_type_diet.name')
                ->groupBy('reff_serving_type.name')
                ->groupBy('reff_order_type.name')
                ->where('ms_food.id_food',$id_food)
                ->first();
        // dd($sql);
        if($sql){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan berhasil ditemukan";
        } else{
            $response["value"] = $id_food;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan tidak ditemukan";
        }
        
        return $response;
    }
    public static function getListFoodPerCheff($email) {
        
        $sql = DB::table('ms_food')
                ->join('ms_cheff', 'ms_food.email_user', '=', 'ms_cheff.email_user')
                ->join('ms_user', 'ms_food.email_user', '=', 'ms_user.email')
                ->join('reff_keahlian', 'ms_food.id_keahlian', '=', 'reff_keahlian.id_keahlian')
                ->join('reff_order_type', 'ms_food.order_type', '=', 'reff_order_type.id_order_type')
                ->join('reff_est_cooking', 'ms_food.id_est_cooking', '=', 'reff_est_cooking.id_est_cooking')
                ->join('reff_cuisine', 'ms_food.id_cuisine', '=', 'reff_cuisine.id_cuisine')
                ->join('reff_cooking_method', 'ms_food.id_cooking_method', '=', 'reff_cooking_method.id_cooking_method')
                ->join('reff_type_diet', 'ms_food.id_type_diet', '=', 'reff_type_diet.id_type_diet')
                ->join('reff_serving_type', 'ms_food.id_serving_type', '=', 'reff_serving_type.id_serving_type')
                ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
                ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
                ->select('ms_food.*','ms_user.name as cheff_name','ms_user.email as email_user', DB::raw('coalesce(SUM(rate), 0) as total_rate'), DB::raw('COUNT(rate) as total_review'),DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food and ms_food_gallery.link IS NOT NULL
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"),DB::raw('coalesce(AVG(rate), 0) as food_rating'),'reff_keahlian.name as keahlian_name','reff_est_cooking.name as est_cooking_name','reff_cuisine.name as cuisine_name','reff_cooking_method.name as cooking_method_name','reff_type_diet.name as type_diet_name','reff_serving_type.name as serving_type_name','reff_order_type.name as order_type_name')
                ->groupBy('ms_food.id_food')
                ->groupBy('ms_cheff.id_cheff')
                ->groupBy('ms_user.name')
                ->groupBy('ms_user.email')
                ->groupBy('reff_keahlian.name')
                ->groupBy('reff_est_cooking.name')
                ->groupBy('reff_cuisine.name')
                ->groupBy('reff_cooking_method.name')
                ->groupBy('reff_type_diet.name')
                ->groupBy('reff_serving_type.name')
                ->groupBy('reff_order_type.name')
                ->where('ms_cheff.email_user',$email)
                ->orderBy('food_rating', 'desc')
                ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan berhasil ditemukan";
        } else{
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan tidak ditemukan";
        }
        
        return $response;
    }
    public static function newIDGalleryFood() {
        
        $top_id = DB::table('ms_food_gallery')
                ->orderby('id_food_gallery','desc')
                ->first();
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_food_gallery + 1;
        }
        
    	return $new_id;
    }
    public static function uploadFoodGalleryInternal($new_id,$id_food,$link_internal) {
        
        $sql = DB::insert("INSERT INTO ms_food_gallery (
                id_food_gallery,
                id_food,
                source,
                link,
                link_internal)
                    values (
                      '".$new_id."',
                      '".$id_food."',
                      'Internal',
                      '".url($link_internal)."',
                      '".url($link_internal)."'
                    )");
        if ($sql) {
            // $response["value"] = $request->all();
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Upload gallery masakan berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Upload gallery masakan gagal";
        }
        return $response;
    }
    public static function uploadFoodGalleryExternal($new_id,$id_food,$link) {
        
        $sql = DB::insert("INSERT INTO ms_food_gallery (
                id_food_gallery,
                id_food,
                source,
                link_external)
                    values (
                      '".$new_id."',
                      '".$id_food."',
                      'External',
                      '".$link."'
                    )");
        if ($sql) {
            // $response["value"] = $request->all();
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Upload gallery masakan berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Upload gallery masakan gagal";
        }
        return $response;
    }
    public static function getListFoodGallery($id_food) {
        
        // $id_food = $request->get('id_food');
        $sql = DB::table('ms_food_gallery')
                        ->where('id_food',$id_food)
                        // ->where('link','<>','null')
                        ->orderby('is_default','desc')
                        ->get();
        if($sql){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Gallery berhasil ditemukan";
        } else{
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Gallery tidak ditemukan";
        }
    	return $response;
    }
    public static function getListFoodGalleryInternal($id_food) {
        
        // $id_food = $request->get('id_food');
        $sql = DB::table('ms_food_gallery')
                        ->select('id_food_gallery','link')
                        ->where('id_food',$id_food)
                        ->where('source','Internal')
                        ->orderby('is_default','desc')
                        ->get();
        if($sql){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Gallery berhasil ditemukan";
        } else{
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Gallery tidak ditemukan";
        }
    	return $response;
    }
    public static function getListFoodGalleryExternal($id_food) {
        
        // $id_food = $request->get('id_food');
        $sql = DB::table('ms_food_gallery')
                        ->select('id_food_gallery','link_external')
                        ->where('id_food',$id_food)
                        ->where('source','External')
                        ->orderby('is_default','desc')
                        ->get();
        if($sql){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Gallery berhasil ditemukan";
        } else{
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Gallery tidak ditemukan";
        }
    	return $response;
    }
    public static function publishFood($visibility,$id_food) {

        $date_now_ymd = date("Y-m-d");
        // dd($birth_date);
        $sql_default = DB::update("UPDATE ms_food set 
                        visibility = '$visibility',
                        updated_at = '$date_now_ymd'
                        where id_food ='$id_food'");
        if ($sql_default) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Gagal";
        }
        return $response;
    }
    public static function minusStock($id_food, $minus_val) {
        
        $sql = MsFood::where('id_food', $id_food)
            ->update(['limit_order' => DB::raw('CAST(limit_order AS INT)-'.$minus_val)]);
        return $sql;
    }
}
