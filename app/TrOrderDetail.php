<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class TrOrderDetail extends Model
{
    protected $table = 'tr_order_detail';
    
    public static function createOrderDetailNew($id_order,$request) {
        $id_food = $request['id_food'];
        $amount = $request['amount'];
        $note = $request['note'];
        $get_food = MsFood::getFood($id_food);
        // dd($get_food);
        $price = $get_food['value']->price;
        $subtotal = $get_food['value']->price * $request['amount'];
        // dd($subtotal);
        $date_now_ymd = date('Y-m-d');
        $top_id = TrOrderDetail::orderby('id_order_detail','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_order_detail + 1;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO tr_order_detail (
                id_order_detail,
                id_order,
                id_food,
                price,
                amount,
                note,
                subtotal
                )
                    values (
                      '".$new_id."',
                      '".$id_order."',
                      '".$id_food."',
                      '".$price."',
                      '".$amount."',
                      '".$note."',
                      '".$subtotal."'
                    )");
        if ($sql) {
            // $message = MsMessage::where('language_code','ID')
            //         ->where('message_code','create_food_success')
            //         ->first();
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah detail order berhasil";
            // $response["message"] = $message->message;
        } else {
            $message = MsMessage::where('language_code','ID')
                    ->where('message_code','create_food_fail')
                    ->first();              
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah detail order gagal";
            // $response["message"] = $message->message;
        }
        
        return $response;
    }
    public static function createOrderDetail($request) {
        $id_order = $request['id_order'];
        $id_food = $request['id_food'];
        $price = $request['price'];
        $amount = $request['amount'];
        $subtotal = $request['price'] * $request['amount'];

        $date_now_ymd = date('Y-m-d');
        $top_id = TrOrderDetail::orderby('id_order_detail','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_order_detail + 1;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO tr_order_detail (
                id_order_detail,
                id_order,
                id_food,
                price,
                amount,
                subtotal
                )
                    values (
                      '".$new_id."',
                      '".$id_order."',
                      '".$id_food."',
                      '".$price."',
                      '".$amount."',
                      '".$subtotal."'
                    )");
        if ($sql) {
            // $message = MsMessage::where('language_code','ID')
            //         ->where('message_code','create_food_success')
            //         ->first();
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah detail order berhasil";
            // $response["message"] = $message->message;
        } else {
            $message = MsMessage::where('language_code','ID')
                    ->where('message_code','create_food_fail')
                    ->first();              
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah detail order gagal";
            // $response["message"] = $message->message;
        }
        
        return $response;
    }
    public static function getDataOrderDetail($id_order) {
        
        $data = DB::table('tr_order_detail')
                            ->join('ms_food', 'tr_order_detail.id_food', '=', 'ms_food.id_food')
                            ->where('tr_order_detail.id_order', $id_order)
                            ->select('tr_order_detail.*', 'ms_food.menu_name')
                            ->get();
        return $data;
    }
    
}
