<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class KeyCheckGenerator extends Model
{
    public static function UsersKey($key = "", $route) {
        $status = false;
        $message = '';
        $sql_check = DB::table('ms_secret_key')
                        ->where('route',$route)
                        ->first();
    	if($key == $sql_check->value_string){
    	    $status = true;
    	}else{
    	    $message = "You not Have a Credential";
    	}
    	
    	$response = array( 
    	    "Status"    => $status,
    	    "Message"   => $message,
    	    "Key"       => $key
    	);
    	return $response;
    }
}
