<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MsNews extends Model
{
    protected $table = 'ms_news';

    public static function getListNews() {
        // dd($email);
        $date_now_ymd = date('Y-m-d');
        // dd($date_now_ymd);
        $sql = DB::table('ms_news')
            ->where('from_date','<=',$date_now_ymd)
            ->where('to_date','>=',$date_now_ymd)
            ->orderby('ms_news.updated_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar berita berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar berita tidak ditemukan";
        }
        
        return $response;
    }
    public static function getNews($id_news) {
        $sql = MsNews::where('id_news',$id_news)
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Berita berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Berita tidak ditemukan";
        }
        
    	return $response;
    }
}
