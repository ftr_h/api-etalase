<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\TrOrder;
use App\MsWishlistCheff as MsWishlistCheff;
class MsCheff extends Model
{
    protected $table = 'ms_cheff';
    
    public static function getListCheff($search_val) {
        
        $sql = MsUser::join('ms_cheff', 'ms_user.email', '=', 'ms_cheff.email_user')
                          ->select('ms_user.name','ms_user.email','ms_user.image', 'ms_cheff.*')
                          ->orderBy('rating', 'desc')
                          ->get();
        if($sql){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Koki berhasil ditemukan.";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Koki tidak ditemukan.";
        }
    	return $response;
    }
    public static function getListRecommendCheff($lat,$long,$offset,$limit) {
        $sql = MsUser::join('ms_cheff', 'ms_user.email', '=', 'ms_cheff.email_user')
                            ->join('ms_food', 'ms_cheff.email_user', '=', 'ms_food.email_user')
                            ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
                            ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
                            ->select('ms_cheff.email_user','ms_food.id_food', DB::raw("
                            (6371  * ACOS(COS(RADIANS($lat))
                                   * COS(RADIANS(latitude::numeric))
                                   * COS(RADIANS($long) - RADIANS(longitude::numeric))
                                   + SIN(RADIANS($lat))
                                   * SIN(RADIANS(latitude::numeric)))) AS distance"),DB::raw('coalesce(AVG(rate), 0) as food_rating')
                           )
                            // ->having('top_id_food', '>', '0')
                            ->where('ms_food.cover_dish','true')
                            ->groupby('ms_cheff.email_user')
                            ->groupby('ms_cheff.id_cheff')
                            ->groupby('ms_food.id_food')
                            ->groupby('ms_cheff.latitude')
                            ->groupby('ms_cheff.longitude')
                            ->orderBy('distance', 'asc')
                            ->orderBy('food_rating', 'desc')
                            ->skip($offset)
                            ->take($limit)
                            ->get();
        if($sql){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Koki berhasil ditemukan.";
        } else{
            $response["value"] = $sql;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Koki tidak ditemukan.";
        }
    	return $sql;
    }
    public static function createCheff($request) {
        // dd($request['email']);
        
        $email = $request['email'];
        // $lokasi = $request['lokasi'];
        $alamat_bisnis = $request['alamat_bisnis'];
        $provinsi = $request['provinsi'];
        $kecamatan = $request['kecamatan'];
        $kota = $request['kota'];
        $kodepost = $request['kodepost'];
        $longitude = $request['longitude'];
        $latitude = $request['latitude'];
        $bank_account_no = $request['bank_account_no'];
        $kode_bank = $request['kode_bank'];
        $judul_makanan = $request['judul_makanan'];
        $tentang_koki = $request['tentang_koki'];
        $cuisine = $request['cuisine'];
        $type_diet = $request['type_diet'];
        $keahlian = $request['keahlian'];
        $category_koki = $request['category_koki'];
        $belajar_memasak = $request['belajar_memasak'];
        $belajar_memasak_string = implode(', ', $belajar_memasak);
        // dd($belajar_memasak_string);
        // $username = $request->get('username');
        $date_now_ymd = date('Y-m-d');
        $top_id = MsCheff::orderby('id_cheff','desc')
                ->first();
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_cheff + 1;
        }
        $top_id_detail = DB::table('ms_cheff_detail')
                ->orderby('id_cheff_detail','desc')
                ->first();
        $new_id_detail = 0;
        if ($top_id_detail == null) {
            $new_id_detail = 1;
        } else {
            $new_id_detail = $top_id_detail->id_cheff_detail + 1;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO ms_cheff (
                id_cheff,
                email_user,
                alamat_bisnis,
                provinsi,
                kecamatan,
                kota,
                kodepost,
                longitude,
                latitude,
                bank_account_no,
                kode_bank,
                verified,
                total_transaction,
                rating,
                total_vote,
                created_at,
                updated_at)
                    values (
                      '".$new_id."',
                      '".$email."',
                      '".$alamat_bisnis."',
                      '".$provinsi."',
                      '".$kecamatan."',
                      '".$kota."',
                      '".$kodepost."',
                      '".$longitude."',
                      '".$latitude."',
                      '".$bank_account_no."',
                      '".$kode_bank."',
                      '0',
                      '0',
                      '0',
                      '0',
                      '".$date_now_ymd."',
                      '".$date_now_ymd."'
                    )");
                    
        if ($sql) {
            
            $sql_detail = DB::insert("INSERT INTO ms_cheff_detail (
                    id_cheff_detail,
                    id_cheff,
                    judul_makanan,
                    tentang_koki,
                    cuisine,
                    type_diet,
                    keahlian,
                    category_koki,
                    belajar_memasak)
                        values (
                          '".$new_id_detail."',
                          '".$new_id."',
                          '".$judul_makanan."s',
                          '".$tentang_koki."',
                          '".$cuisine."',
                          '".$type_diet."',
                          '".$keahlian."',
                          ".$category_koki.",
                          '".$belajar_memasak_string."'
                        )");
            $sql_update = DB::update("UPDATE ms_user set 
                    id_role_user = '2'
                    where email='$email'");
                    
            if($sql_detail){
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Berhasil! Tunggu konfirmasi admin untuk verifikasi chef";
            } else{
                $response["value"] = $request;
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Pendaftaran gagal, cek lagi data detail chef";
            }
        } else {
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Pendaftaran gagal, cek lagi data chef";
        }
        
    	return $response;
    }
    
    public static function getCheff($email) {
        
        $sql = MsCheff::join('ms_user', 'ms_cheff.email_user', '=', 'ms_user.email')
                ->join('ms_cheff_detail', 'ms_cheff.id_cheff', '=', 'ms_cheff_detail.id_cheff')
                ->join('reff_provinsi', 'ms_cheff.provinsi', '=', 'reff_provinsi.id_provinsi')
                ->join('reff_category_koki', 'ms_cheff_detail.category_koki', '=', 'reff_category_koki.id_category_koki')
                ->join('reff_type_diet', 'ms_cheff_detail.type_diet', '=', 'reff_type_diet.id_type_diet')
                ->join('reff_keahlian', 'ms_cheff_detail.keahlian', '=', 'reff_keahlian.id_keahlian')
                ->join('reff_cuisine', 'ms_cheff_detail.cuisine', '=', 'reff_cuisine.id_cuisine')
                ->leftJoin('ms_food', 'ms_cheff.email_user', '=', 'ms_food.email_user')
                ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
                ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
                ->select('ms_cheff.*','ms_cheff_detail.*','ms_user.*','reff_provinsi.name as provinsi_name','reff_category_koki.name as category_koki_name','reff_type_diet.name as type_diet_name','reff_keahlian.name as keahlian_name','reff_cuisine.name as cuisine_name',DB::raw('coalesce(SUM(ms_review.rate), 0) as total_rate'), DB::raw('COUNT(ms_review.rate) as total_review'),DB::raw('coalesce(AVG(ms_review.rate), 0) as cheff_rating'))
                ->groupby('ms_cheff.email_user')
                ->groupby('ms_cheff.id_cheff')
                ->groupby('ms_cheff_detail.id_cheff_detail')
                ->groupby('ms_cheff_detail.id_cheff_detail')
                ->groupby('ms_user.email')
                ->groupby('reff_provinsi.name')
                ->groupby('reff_category_koki.name')
                ->groupby('reff_type_diet.name')
                ->groupby('reff_keahlian.name')
                ->groupby('reff_cuisine.name')
                ->where('email',$email)
                ->first();
        unset($sql->password);
        $success_trans = TrOrder::where('status', '5')->count();
        $trans = TrOrder::where('status', '<>', '0')
        ->where('status', '<>', '1')
        ->where('status', '<>', '6')
        ->count();
        if($sql){
            $response["value"] = $sql;
            $response["total_success_transaction"] = $success_trans;
            $response["total_transaction"] = $trans;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Koki berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Koki tidak ditemukan";
        }
    	return $response;
    }
    public static function getCheffFoodT($email) {
        
        // $sql = MsCheff::join('ms_user', 'ms_cheff.email_user', '=', 'ms_user.email')
        //         ->join('ms_cheff_detail', 'ms_cheff.id_cheff', '=', 'ms_cheff_detail.id_cheff')
        //         ->join('reff_provinsi', 'ms_cheff.provinsi', '=', 'reff_provinsi.id_provinsi')
        //         ->join('reff_category_koki', 'ms_cheff_detail.category_koki', '=', 'reff_category_koki.id_category_koki')
        //         ->join('reff_type_diet', 'ms_cheff_detail.type_diet', '=', 'reff_type_diet.id_type_diet')
        //         ->join('reff_keahlian', 'ms_cheff_detail.keahlian', '=', 'reff_keahlian.id_keahlian')
        //         ->join('reff_cuisine', 'ms_cheff_detail.cuisine', '=', 'reff_cuisine.id_cuisine')
        //         ->leftJoin('ms_food', 'ms_cheff.email_user', '=', 'ms_food.email_user')
        //         ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
        //         ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
        //         ->select('ms_cheff.*','ms_cheff_detail.*','ms_user.*','reff_provinsi.name as provinsi_name','reff_category_koki.name as category_koki_name','reff_type_diet.name as type_diet_name','reff_keahlian.name as keahlian_name','reff_cuisine.name as cuisine_name',DB::raw('coalesce(SUM(ms_review.rate), 0) as total_rate'), DB::raw('COUNT(ms_review.rate) as total_review'),DB::raw('coalesce(AVG(rate), 0) as cheff_rating'))
        //         ->groupby('ms_cheff.email_user')
        //         ->groupby('ms_cheff.id_cheff')
        //         ->groupby('ms_cheff_detail.id_cheff_detail')
        //         ->groupby('ms_cheff_detail.id_cheff_detail')
        //         ->groupby('ms_user.email')
        //         ->groupby('reff_provinsi.name')
        //         ->groupby('reff_category_koki.name')
        //         ->groupby('reff_type_diet.name')
        //         ->groupby('reff_keahlian.name')
        //         ->groupby('reff_cuisine.name')
        //         ->where('email',$email)
        //         ->first();
        $test = TrOrder::paginate(5);
        // dd($test);
        return $test;
        $search_val = '';
        $sql = Mscheff::join('ms_user', 'ms_cheff.email_user', '=', 'ms_user.email')
            ->leftJoin('ms_food', 'ms_cheff.email_user', '=', 'ms_food.email_user')
            ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
            ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
            ->select('ms_cheff.*','ms_user.*',DB::raw('coalesce(SUM(ms_review.rate), 0) as total_rate'), DB::raw('COUNT(ms_review.rate) as total_review'),DB::raw('coalesce(AVG(rate), 0) as cheff_rating'))
            ->WhereRaw('LOWER(ms_user.name) LIKE ? ',['%'.trim(strtolower($search_val)).'%'])
            ->Where('ms_cheff.verified', '1')
            ->groupby('ms_cheff.email_user')
            ->groupby('ms_cheff.id_cheff')
            ->groupby('ms_user.email')
            ->get();
        $sql_rate = DB::table('ms_review')
            ->join('tr_order_detail', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
            ->join('ms_food', 'tr_order_detail.id_food', '=', 'ms_food.id_food')
            ->join('ms_cheff', 'ms_food.email_user', '=', 'ms_cheff.email_user')
            ->select(DB::raw('coalesce(SUM(ms_review.rate), 0) as total_rate'), DB::raw('COUNT(ms_review.rate) as total_review'),DB::raw('coalesce(AVG(rate), 0) as cheff_rating'))
            ->where('ms_cheff.email_user', $email)
            ->groupby('ms_cheff.email_user')
            ->get();
        unset($sql->password);
        dump($sql);
        dd($sql_rate);
        $list_wishlist = MsWishlist::getListWishlist($email);
        $list_follower = MsFollow::getListFollower($email);
        if($sql){
            $response["value_cheff"] = $sql;
            $response["count_wishlist"] = $list_wishlist['value']->count();
            $response["count_follower"] = $list_follower['value']->count();
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Koki berhasil ditemukan";
        } else{
            $response["value_cheff"] = null;
            $response["count_wishlist"] = 0;
            $response["count_follower"] = 0;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Koki tidak ditemukan";
        }
        return $response;
    }
    public static function getCheffFood($request) {
        
        $email = $request['email'];
        if(array_key_exists('email_user_now', $request)){
            $follower = $request['email_user_now'];
        } else {
            $follower = '';
        }
        $sql = MsCheff::join('ms_user', 'ms_cheff.email_user', '=', 'ms_user.email')
                ->join('ms_cheff_detail', 'ms_cheff.id_cheff', '=', 'ms_cheff_detail.id_cheff')
                ->join('reff_provinsi', 'ms_cheff.provinsi', '=', 'reff_provinsi.id_provinsi')
                ->join('reff_category_koki', 'ms_cheff_detail.category_koki', '=', 'reff_category_koki.id_category_koki')
                ->join('reff_type_diet', 'ms_cheff_detail.type_diet', '=', 'reff_type_diet.id_type_diet')
                ->join('reff_keahlian', 'ms_cheff_detail.keahlian', '=', 'reff_keahlian.id_keahlian')
                ->join('reff_cuisine', 'ms_cheff_detail.cuisine', '=', 'reff_cuisine.id_cuisine')
                ->leftJoin('ms_food', 'ms_cheff.email_user', '=', 'ms_food.email_user')
                ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
                ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
                ->select('ms_cheff.*','ms_cheff_detail.*','ms_user.*','reff_provinsi.name as provinsi_name','reff_category_koki.name as category_koki_name','reff_type_diet.name as type_diet_name','reff_keahlian.name as keahlian_name','reff_cuisine.name as cuisine_name',DB::raw('coalesce(SUM(ms_review.rate), 0) as total_rate'), DB::raw('COUNT(ms_review.rate) as total_review'),DB::raw('coalesce(AVG(ms_review.rate), 0) as cheff_rating'))
                ->groupby('ms_cheff.email_user')
                ->groupby('ms_cheff.id_cheff')
                ->groupby('ms_cheff_detail.id_cheff_detail')
                ->groupby('ms_cheff_detail.id_cheff_detail')
                ->groupby('ms_user.email')
                ->groupby('reff_provinsi.name')
                ->groupby('reff_category_koki.name')
                ->groupby('reff_type_diet.name')
                ->groupby('reff_keahlian.name')
                ->groupby('reff_cuisine.name')
                ->where('email',$email)
                ->first();
        
        unset($sql->password);
         $sql_food = DB::table('ms_food')
                ->join('ms_cheff', 'ms_food.email_user', '=', 'ms_cheff.email_user')
                ->join('ms_user', 'ms_food.email_user', '=', 'ms_user.email')
                ->join('reff_keahlian', 'ms_food.id_keahlian', '=', 'reff_keahlian.id_keahlian')
                ->join('reff_order_type', 'ms_food.order_type', '=', 'reff_order_type.id_order_type')
                ->join('reff_est_cooking', 'ms_food.id_est_cooking', '=', 'reff_est_cooking.id_est_cooking')
                ->join('reff_cuisine', 'ms_food.id_cuisine', '=', 'reff_cuisine.id_cuisine')
                ->join('reff_cooking_method', 'ms_food.id_cooking_method', '=', 'reff_cooking_method.id_cooking_method')
                ->join('reff_type_diet', 'ms_food.id_type_diet', '=', 'reff_type_diet.id_type_diet')
                ->join('reff_serving_type', 'ms_food.id_serving_type', '=', 'reff_serving_type.id_serving_type')
                ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
                ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
                ->select('ms_food.*','ms_user.name as cheff_name','ms_user.email as email_user', DB::raw('coalesce(SUM(rate), 0) as total_rate'), DB::raw('COUNT(rate) as total_review'),DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"),DB::raw('coalesce(AVG(rate), 0) as food_rating'),'reff_keahlian.name as keahlian_name','reff_est_cooking.name as est_cooking_name','reff_cuisine.name as cuisine_name','reff_cooking_method.name as cooking_method_name','reff_type_diet.name as type_diet_name','reff_serving_type.name as serving_type_name','reff_order_type.name as order_type_name')
                ->groupBy('ms_food.id_food')
                ->groupBy('ms_cheff.id_cheff')
                ->groupBy('ms_user.name')
                ->groupBy('ms_user.email')
                ->groupBy('reff_keahlian.name')
                ->groupBy('reff_est_cooking.name')
                ->groupBy('reff_cuisine.name')
                ->groupBy('reff_cooking_method.name')
                ->groupBy('reff_type_diet.name')
                ->groupBy('reff_serving_type.name')
                ->groupBy('reff_order_type.name')
                ->where('ms_cheff.email_user',$email)
                ->orderBy('food_rating', 'asc')
                ->get();
      
        $list_wishlist = MsWishlist::getListWishlist($email);
        $list_follower = MsFollow::getListFollower($email);
        $checkFollow = MsFollow::checkFollow($email,$follower);
        if($list_wishlist['value'] != null){
            $count_wishlist = $list_wishlist['value']->count();
        } else {
            $count_wishlist = 0;
        }
        if($list_follower['value'] != null){
            $count_follower = $list_follower['value']->count();
        } else {
            $count_follower = 0;
        }
        if($sql){
            $response["value_cheff"] = $sql;
            $response["value_food"] = $sql_food;
            $response["count_food"] = $sql_food->count();
            $response["count_wishlist"] = $count_wishlist;
            $response["count_follower"] = $count_follower;
            $response["is_followed"] = $checkFollow['status'];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Koki berhasil ditemukan";
        } else{
            $response["value_cheff"] = $sql;
            $response["value_food"] = null;
            $response["count_food"] = $sql_food->count();
            $response["count_wishlist"] = 0;
            $response["count_follower"] = 0;
            $response["is_followed"] = $checkFollow['status'];
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Koki tidak ditemukan";
        }
        return $response;
    }
    public static function updateCheff($request) {
        
        $email = $request['email'];
        $judul_makanan = $request['judul_makanan'];
        $tentang_koki = $request['tentang_koki'];
        // $username = $request->get('keahlian');
        $type_diet = $request['type_diet'];
        $keahlian = $request['keahlian'];
        $category_koki = $request['category_koki'];
        $date_now_ymd = date('Y-m-d');
        // dd($check_email->count());
        $sql_check = DB::table('ms_cheff')
                    ->where('email_user',$email)
                    ->first();
        $sql_cheff = DB::update("UPDATE ms_cheff_detail set 
                        judul_makanan = '$judul_makanan',
                        tentang_koki = '$tentang_koki',
                        type_diet = '$type_diet',
                        keahlian = '$keahlian',
                        category_koki = '$category_koki'
                        where id_cheff=$sql_check->id_cheff");
        if ($sql_cheff) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Update data koki berhasil";
        } else {
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Update data koki gagal";
        }
    	return $response;
    }
    public static function newIDCheffGallery() {
        
        $top_id = DB::table('ms_cheff_gallery')
                ->orderby('id_cheff_gallery','desc')
                ->first();
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_cheff_gallery + 1;
        }
    	return $new_id;
    }
    public static function uploadCheffGalleryInternal($new_id,$email_user,$type,$source,$link,$is_default) {
        
        // dd(url($link_to_db.'/'.$name));
        $sql = DB::insert("INSERT INTO ms_cheff_gallery (
                id_cheff_gallery,
                email_user,
                type,
                source,
                link,
                is_default)
                    values (
                      '".$new_id."',
                      '".$email_user."',
                      '".$type."',
                      '".$source."',
                      '".url($link)."',
                      '".$is_default."'
                    )");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Upload gallery koki berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Upload gallery koki gagal";
        }
    	return $response;
    }
    public static function uploadCheffGalleryYT($new_id,$email_user,$type,$source,$link,$is_default) {
        
        $sql = DB::insert("INSERT INTO ms_cheff_gallery (
                id_cheff_gallery,
                email_user,
                type,
                source,
                link,
                is_default)
                    values (
                      '".$new_id."',
                      '".$email_user."',
                      '".$type."',
                      '".$source."',
                      '".$link."',
                      '".$is_default."'
                    )");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Upload gallery koki berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Upload gallery koki gagal";
        }
    	return $response;
    }
    public static function getListCheffGallery($email) {
        
        $sql = DB::table('ms_cheff_gallery')
                        ->where('email_user',$email)
                        ->orderby('is_default','desc')
                        ->get();
        if($sql){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Gallery berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Gallery tidak ditemukan";
        }
    	return $response;
    }
    public static function getListCheffSearch($search_val) {
        
        $sql = Mscheff::join('ms_user', 'ms_cheff.email_user', '=', 'ms_user.email')
            ->leftJoin('ms_food', 'ms_cheff.email_user', '=', 'ms_food.email_user')
            ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
            ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
            ->select('ms_cheff.*','ms_user.*',DB::raw('coalesce(SUM(ms_review.rate), 0) as total_rate'), DB::raw('COUNT(ms_review.rate) as total_review'),DB::raw('coalesce(AVG(rate), 0) as cheff_rating'))
            ->WhereRaw('LOWER(ms_user.name) LIKE ? ',['%'.trim(strtolower($search_val)).'%'])
            ->Where('ms_cheff.verified', '1')
            ->groupby('ms_cheff.email_user')
            ->groupby('ms_cheff.id_cheff')
            ->groupby('ms_user.email')
            ->get();
        unset($sql->password);
    	return $sql;
    }
    public static function getListCheffSearchFood($email_user) {
        
        $sql_food = DB::table('ms_food')
                ->join('ms_user', 'ms_food.email_user', '=', 'ms_user.email')
                ->join('reff_keahlian', 'ms_food.id_keahlian', '=', 'reff_keahlian.id_keahlian')
                ->join('reff_order_type', 'ms_food.order_type', '=', 'reff_order_type.id_order_type')
                ->join('reff_est_cooking', 'ms_food.id_est_cooking', '=', 'reff_est_cooking.id_est_cooking')
                ->join('reff_cuisine', 'ms_food.id_cuisine', '=', 'reff_cuisine.id_cuisine')
                ->join('reff_cooking_method', 'ms_food.id_cooking_method', '=', 'reff_cooking_method.id_cooking_method')
                ->join('reff_type_diet', 'ms_food.id_type_diet', '=', 'reff_type_diet.id_type_diet')
                ->join('reff_serving_type', 'ms_food.id_serving_type', '=', 'reff_serving_type.id_serving_type')
                ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
                ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
                ->select('ms_food.*','ms_user.name as cheff_name','ms_user.email as email_user', DB::raw('coalesce(SUM(rate), 0) as total_rate'), DB::raw('COUNT(rate) as total_review'),DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"),DB::raw('coalesce(AVG(rate), 0) as food_rating'),'reff_keahlian.name as keahlian_name','reff_est_cooking.name as est_cooking_name','reff_cuisine.name as cuisine_name','reff_cooking_method.name as cooking_method_name','reff_type_diet.name as type_diet_name','reff_serving_type.name as serving_type_name','reff_order_type.name as order_type_name')
                ->groupBy('ms_food.id_food')
                ->groupBy('ms_user.name')
                ->groupBy('ms_user.email')
                ->groupBy('reff_keahlian.name')
                ->groupBy('reff_est_cooking.name')
                ->groupBy('reff_cuisine.name')
                ->groupBy('reff_cooking_method.name')
                ->groupBy('reff_type_diet.name')
                ->groupBy('reff_serving_type.name')
                ->groupBy('reff_order_type.name')
                ->where('ms_food.email_user',$email_user)
                ->orderBy('cover_dish', 'desc')
                ->get();
    	return $sql_food;
    }
    public function createNotifBayar(Request $request)
    {
        // dd($requset->getContent());
        // $data = [];
        // $data['email'] = "cheff@gmail.com";
        // $data['title'] = "Pembayaran Berhasil";
        // $data['message'] = "Test Pembayaran Berhasil";
        // $notif_bayar = MsNotification::createNotif($data);
        // echo json_encode($notif_bayar);
        
        // Handling callbacks:
        // ... code to handle callbacks
        // ... extract request headers and body
        $timestamp = 'timestamp'; // X-Odeo-Timestamp headers from callback request
        $signature = 'signature'; // X-Odeo-Signature headers from callback request
        $requestBody = '{rawRequestBody}'; // Raw request body from callback request
        $method = 'POST'; // callback HTTP method
        $path = '/your-callback-path';  // the part after host name and port number from callback URL
                                        // must begins with '/'
        $isValid = $disbursement->isValidSignature($signature, $method, $path, $timestamp, $requestBody);
        if ($isValid) {
          return 'Success';
        } else {
          return 'Gagal';
        }
          return 'OK';
    }
    public static function checkDistance($email,$lat,$long)
    {
        $sql = MsUser::join('ms_cheff', 'ms_user.email', '=', 'ms_cheff.email_user')
                            ->select('ms_cheff.email_user',DB::raw("
                            (6371  * ACOS(COS(RADIANS($lat))
                                   * COS(RADIANS(latitude::numeric))
                                   * COS(RADIANS($long) - RADIANS(longitude::numeric))
                                   + SIN(RADIANS($lat))
                                   * SIN(RADIANS(latitude::numeric)))) AS distance")
                           )
                            ->where('ms_cheff.email_user',$email)
                            ->first();
        return number_format((float)$sql->distance, 2, '.', '');
    }
    
}
