<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MsMessage extends Model
{
    protected $table = 'ms_message';
}
