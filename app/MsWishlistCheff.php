<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MsWishlistCheff extends Model
{
    protected $table = 'ms_wishlist_cheff';

    public static function createWishlistCheff($request) {
        $email_user = $request['email_user'];
        $email_cheff = $request['email_cheff'];
        
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = MsWishlistCheff::orderby('id_wishlist_cheff','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_wishlist_cheff + 1;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO ms_wishlist_cheff (
                id_wishlist_cheff,
                email_cheff,
                email_user,
                created_at
                )
                    values (
                      '".$new_id."',
                      '".$email_cheff."',
                      '".$email_user."',
                      '".$date_now_ymdhis."'
                    )");
        if($sql){
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Wishlist Cheff berhasil";
        } else{
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Wishlist Cheff gagal";
        }
        return $response;
    }
    public static function getListWishlistCheff($email) {
        // dd($id_food);
        $sql = DB::table('ms_wishlist_cheff')
            ->join('ms_user', 'ms_wishlist_cheff.email_cheff', '=', 'ms_user.email')
            ->select('ms_wishlist_cheff.*', 'ms_user.name as email_cheff_name', 'ms_user.image as email_cheff_image')
            ->where('ms_wishlist_cheff.email_user', $email)
            ->orderby('ms_wishlist_cheff.created_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar wishlist cheff berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar wishlist cheff tidak ditemukan";
        }
        
        return $response;
    }
    public static function getWishlistCheff($id_wishlist_cheff) {
        
        $sql = MsWishlistCheff::where('id_wishlist_cheff',$id_wishlist_cheff)
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Wishlist Cheff berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Wishlist Cheff tidak ditemukan";
        }
        
        return $response;
    }
    public static function deleteWishlistCheff($email_cheff,$email_user) {
        
        $deleteWishlistCheff = DB::table('ms_wishlist_cheff')
                    ->where('email_cheff', $email_cheff)
                    ->where('email_user', $email_user)
                    ->delete();
        if ($deleteWishlistCheff) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Sukses hapus data wishlist";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Gagal hapus data wishlist";
        }
        return $response;
    }
    public static function checkWishlistCheff($email_cheff,$email_user) {
        
        $checkWishlistCheff = DB::table('ms_wishlist_cheff')
                    ->where('email_cheff', $email_cheff)
                    ->where('email_user', $email_user)
                    ->get();
        if ($checkWishlistCheff->count() > 0) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Wishlisted";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Not wishlisted";
        }
        return $response;
    }
}
