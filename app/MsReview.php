<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MsReview extends Model
{
    protected $table = 'ms_review';

    public static function createReview($request) {
        $email_pembeli = $request['email_pembeli'];
        $id_order_detail = $request['id_order_detail'];
        $title = $request['title'];
        $content = $request['content'];
        $rate = $request['rate'];
        $sql_order_detail = DB::table('tr_order_detail')
                            ->where('id_order_detail', $id_order_detail)
                            ->first();
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = MsReview::orderby('id_review','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_review + 1;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO ms_review (
                id_review,
                email_pembeli,
                id_order_detail,
                title,
                content,
                rate,
                id_food,
                created_at,
                updated_at
                )
                    values (
                      '".$new_id."',
                      '".$email_pembeli."',
                      '".$id_order_detail."',
                      '".$title."',
                      '".$content."',
                      '".$rate."',
                      '".$sql_order_detail->id_food."',
                      '".$date_now_ymdhis."',
                      '".$date_now_ymdhis."'
                    )");
        if ($sql) {
            $sql_update = DB::update("UPDATE tr_order_detail set 
                        is_reviewed = '1'
                        where id_order_detail='$id_order_detail'");
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah ulasan berhasil";
            // $response["message"] = $message->message;
        } else {          
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah ulasan gagal";
            // $response["message"] = $message->message;
        }
        
    	return $response;
    }
    public static function getListReviewPerFood($id_food) {
        // dd($id_food);
        $sql = DB::table('tr_order_detail')
            ->join('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
            ->join('ms_food', 'tr_order_detail.id_food', '=', 'ms_food.id_food')
            ->join('ms_user', 'ms_review.email_pembeli', '=', 'ms_user.email')
            ->select('ms_review.*', 'tr_order_detail.id_order', 'ms_food.menu_name', 'ms_food.id_food', 'ms_user.name as user_name', 'ms_user.image as user_image')
            ->where('tr_order_detail.id_food', $id_food)
            ->orderby('ms_review.created_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar ulasan berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar ulasan tidak ditemukan";
        }
        
        return $response;
    }
    public static function getListReviewPerUser($email) {
        // dd($email);
        $sql = DB::table('tr_order_detail')
            ->join('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
            ->join('ms_food', 'tr_order_detail.id_food', '=', 'ms_food.id_food')
            ->join('ms_user', 'ms_review.email_pembeli', '=', 'ms_user.email')
            ->select('ms_review.*', 'tr_order_detail.id_order', 'ms_food.menu_name', 'ms_food.id_food', 'ms_user.name as user_name',DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"))
            ->where('ms_review.email_pembeli', $email)
            ->orderby('ms_review.created_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar ulasan berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar ulasan tidak ditemukan";
        }
        
        return $response;
    }
    public static function getReview($id_review) {
        
        $sql = MsReview::where('id_review',$id_review)
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Ulasan berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Ulasan tidak ditemukan";
        }
        
    	return $response;
    }
    public static function updateReview($request) {
        $title = $request['title'];
        $content = $request['content'];
        $rate = $request['rate'];
        $id_review = $request['id_review'];

        $date_now_ymdhis = date("Y-m-d H:i:s");
        // dd($birth_date);
        $sql = DB::update("UPDATE ms_review set 
                        title = '$title',
                        content = '$content',
                        rate = '$rate',
                        updated_at = '$date_now_ymdhis'
                        where id_review='$id_review'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Review berhasil diupdate";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Review tidak terupdate";
        }
        return $response;
    }
    public static function deleteReview($id_review) {
        
        $deleteReview = DB::table('ms_review')->where('id_review', $id_review)->delete();
        return $deleteReview;
    }
    public static function deleteReviewByFood($id_food) {
        
        $deleteReview = DB::table('ms_review')
            ->join('tr_order_detail', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
            ->where('id_food', $id_food)->delete();
        return $deleteReview;
    }
}
