<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MsAddress extends Model
{
    protected $table = 'ms_user_address';

    public static function createAddress($request) {
        $email_user = $request['email'];
        $address_name = $request['address_name'];
        $longitude = $request['longitude'];
        $latitude = $request['latitude'];
        $date_now_ymd = date("Y-m-d");
        $top_id = MsAddress::orderby('id_user_address','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_user_address + 1;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO ms_user_address (
                id_user_address,
                email_user,
                address_name,
                longitude,
                latitude,
                is_default,
                created_at,
                updated_at
                )
                    values (
                      '".$new_id."',
                      '".$email_user."',
                      '".$address_name."',
                      '".$longitude."',
                      '".$latitude."',
                      '0',
                      '".$date_now_ymd."',
                      '".$date_now_ymd."'
                    )");
        if ($sql) {
            $response["value"] = $new_id;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah alamat berhasil";
            // $response["message"] = $message->message;
        } else {          
            $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah alamat gagal";
            // $response["message"] = $message->message;
        }
        
    	return $response;
    }
    public static function getListAddressPerUser($email) {
        // dd($email);
        $sql = DB::table('ms_user_address as adr')
            ->join('ms_user', 'adr.email_user', '=', 'ms_user.email')
            ->select('adr.*', 'ms_user.name as user_name')
            ->where('adr.email_user', $email)
            ->orderby('adr.is_default','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar alamat berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar alamat tidak ditemukan";
        }
        
        return $response;
    }
    public static function getAddress($id_user_address) {
        
        $sql = MsAddress::where('id_user_address',$id_user_address)
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Alamat berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Alamat tidak ditemukan";
        }
        
    	return $response;
    }
    public static function defaultAddress($request) {

        $email_user = $request['email'];
        $id_user_address = $request['id_user_address'];
        $date_now_ymd = date("Y-m-d");
        // dd($birth_date);
        $sql_clear = DB::update("UPDATE ms_user_address set 
                        is_default = '0',
                        updated_at = '$date_now_ymd'
                        where email_user ='$email_user'");
        if ($sql_clear) {
            $sql_default = DB::update("UPDATE ms_user_address set 
                            is_default = '1',
                            updated_at = '$date_now_ymd'
                            where id_user_address ='$id_user_address'");
            if ($sql_default) {
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Default Address berhasil diupdate";
            } else {
                // $response["value"] = $request;
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Default Address tidak terupdate";
            }
        } else {
            // $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Default Address tidak terupdate";
        }
        return $response;
    }
    public static function updateAddress($request) {
        $email_user = $request['email'];
        $address_name = $request['address_name'];
        $longitude = $request['longitude'];
        $latitude = $request['latitude'];
        $id_user_address = $request['id_user_address'];

        $date_now_ymd = date("Y-m-d");
        // dd($birth_date);
        $sql = DB::update("UPDATE ms_user_address set 
                        email_user = '$email_user',
                        address_name = '$address_name',
                        longitude = '$longitude',
                        latitude = '$latitude',
                        updated_at = '$date_now_ymd'
                        where id_user_address='$id_user_address'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Address berhasil diupdate";
        } else {
            // $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Address tidak terupdate";
        }
        return $response;
    }
    public static function deleteAddress($id_user_address) {
        
        $deleteAddress = DB::table('ms_user_address')->where('id_user_address', $id_user_address)->delete();
        return $deleteAddress;
    }
}
