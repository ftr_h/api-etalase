<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MsFoodCategory extends Model
{
    protected $table = 'ms_food_category';

    public static function createFoodCategory($request) {
        $email_user = $request['email_user'];
        $name = $request['name'];
        
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO ms_food_category (
                name,
                email_user
                )
                    values (
                      '".$name."',
                      '".$email_user."'
                    )");
        if($sql){
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah kategori berhasil";
        } else{
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah kategori gagal";
        }
        return $response;
    }
    public static function getListFoodCategory($email) {
        // dd($id_food);
        $sql = DB::table('ms_food_category')
            ->where('ms_food_category.email_user', $email)
            ->orderby('ms_food_category.name','asc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar kategori berhasil ditemukan";
        } else{
            $response["value"] = $sql;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar kategori tidak ditemukan";
        }
        
        return $response;
    }
    public static function getFoodCategory($id_food_category) {
        
        $sql = MsFoodCategory::where('id_food_category',$id_food_category)
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Detail Kategori berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Detail Kategori tidak ditemukan";
        }
        
        return $response;
    }
    public static function deleteFoodCategory($id_food_category) {
        
        $deleteFoodCategory = DB::table('ms_food_category')
                    ->where('id_food_category', $id_food_category)
                    ->delete();
        if ($deleteFoodCategory) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Sukses hapus data";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Gagal hapus data";
        }
        return $response;
    }
    public static function updateFoodCategory($request) {
        $name = $request['name'];
        $id_food_category = $request['id_food_category'];

        $date_now_ymd = date("Y-m-d");
        // dd($birth_date);
        $sql = DB::update("UPDATE ms_food_category set 
                        name = '$name'
                        where id_food_category='$id_food_category'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Kategori berhasil diupdate";
        } else {
            // $response["value"] = $request;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Kategori tidak terupdate";
        }
        return $response;
    }
}
