<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MsWishlist extends Model
{
    protected $table = 'ms_wishlist';

    public static function createWishlist($request) {
        $email_user = $request['email_user'];
        $id_food = $request['id_food'];
        
        $date_now_ymdhis = date("Y-m-d H:i:s");
        $top_id = MsWishlist::orderby('id_wishlist','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_wishlist + 1;
        }
        // dd($check_email->count());
        $sql = DB::insert("INSERT INTO ms_wishlist (
                id_wishlist,
                id_food,
                email_user,
                created_at
                )
                    values (
                      '".$new_id."',
                      '".$id_food."',
                      '".$email_user."',
                      '".$date_now_ymdhis."'
                    )");
        if($sql){
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Tambah wishlist berhasil";
        } else{
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Tambah wishlist gagal";
        }
        return $response;
    }
    public static function getListWishlist($email) {
        // dd($id_food);
        $sql = DB::table('ms_wishlist')
            ->join('ms_food', 'ms_wishlist.id_food', '=', 'ms_food.id_food')
            ->select('ms_wishlist.*', 'ms_food.menu_name')
            ->where('ms_wishlist.email_user', $email)
            ->orderby('ms_wishlist.created_at','desc')
            ->get();
        if($sql->count() > 0){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar wishlist berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar wishlist tidak ditemukan";
        }
        
        return $response;
    }
    public static function getListWishlistFavorite($email,$offset,$limit,$lat,$long) {
        // dd($id_food);
        if($lat == null){
            $lat = 0;
        }
        if($long == null){
            $long = 0;
        }
        $sql = DB::table('ms_wishlist')
            ->join('ms_food', 'ms_wishlist.id_food', '=', 'ms_food.id_food')
            ->join('ms_cheff', 'ms_food.email_user', '=', 'ms_cheff.email_user')
            ->join('ms_user', 'ms_food.email_user', '=', 'ms_user.email')
            ->join('reff_keahlian', 'ms_food.id_keahlian', '=', 'reff_keahlian.id_keahlian')
            ->join('reff_order_type', 'ms_food.order_type', '=', 'reff_order_type.id_order_type')
            ->join('reff_est_cooking', 'ms_food.id_est_cooking', '=', 'reff_est_cooking.id_est_cooking')
            ->join('reff_cuisine', 'ms_food.id_cuisine', '=', 'reff_cuisine.id_cuisine')
            ->join('reff_cooking_method', 'ms_food.id_cooking_method', '=', 'reff_cooking_method.id_cooking_method')
            ->join('reff_type_diet', 'ms_food.id_type_diet', '=', 'reff_type_diet.id_type_diet')
            ->join('reff_serving_type', 'ms_food.id_serving_type', '=', 'reff_serving_type.id_serving_type')
            ->leftJoin('tr_order_detail', 'ms_food.id_food', '=', 'tr_order_detail.id_food')
            ->leftJoin('ms_review', 'tr_order_detail.id_order_detail', '=', 'ms_review.id_order_detail')
            ->select('ms_food.*',DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food and ms_food_gallery.link IS NOT NULL
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"),'reff_keahlian.name as type_hidangan_name','reff_est_cooking.name as est_cooking_name','reff_cuisine.name as cuisine_name','reff_cooking_method.name as cooking_method_name','reff_type_diet.name as type_diet_name','reff_serving_type.name as serving_type_name','reff_order_type.name as order_type_name',DB::raw("(SELECT ms_food_gallery.link FROM ms_food_gallery
                    WHERE CAST(ms_food_gallery.id_food AS INTEGER) = ms_food.id_food
                    order by ms_food_gallery.is_default desc LIMIT 1) as menu_image"),  
                    DB::raw('coalesce(SUM(rate), 0) as total_rate'), 
                    DB::raw('COUNT(rate) as total_review'),
                    DB::raw('coalesce(AVG(rate), 0) as food_rating'), 
                    DB::raw("
                            (6371  * ACOS(COS(RADIANS($lat))
                                   * COS(RADIANS(latitude::numeric))
                                   * COS(RADIANS($long) - RADIANS(longitude::numeric))
                                   + SIN(RADIANS($lat))
                                   * SIN(RADIANS(latitude::numeric)))) AS distance"))
            ->groupBy('ms_cheff.id_cheff')
            ->groupBy('reff_keahlian.name')
            ->groupBy('reff_est_cooking.name')
            ->groupBy('reff_cuisine.name')
            ->groupBy('reff_cooking_method.name')
            ->groupBy('reff_type_diet.name')
            ->groupBy('reff_serving_type.name')
            ->groupBy('reff_order_type.name')
            ->groupBy('ms_user.image')
            ->groupBy('ms_user.id_user')
            ->groupBy('ms_food.id_food')
            ->groupBy('ms_wishlist.created_at')
            ->Where('ms_cheff.verified', '1')
            ->Where('ms_food.visibility', 'true')
            ->Where('ms_wishlist.email_user', $email)
            ->orderby('ms_wishlist.created_at', 'desc')
            ->skip($offset)
            ->take($limit)
            ->get();
        
        return $sql;
    }
    public static function getWishlist($id_wishlist) {
        
        $sql = MsWishlist::where('id_wishlist',$id_wishlist)
                ->get();
        // dd($sql->count());
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Wishlist berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Wishlist tidak ditemukan";
        }
        
        return $response;
    }
    public static function deleteWishlist($id_food,$email_user) {
        
        $deleteWishlist = DB::table('ms_wishlist')
                    ->where('id_food', $id_food)
                    ->where('email_user', $email_user)
                    ->delete();
        if ($deleteWishlist) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Sukses hapus data";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Gagal hapus data";
        }
        return $response;
    }
    public static function checkWishlist($id_food,$email_user) {
        
        $checkWishlist = DB::table('ms_wishlist')
                    ->where('id_food', $id_food)
                    ->where('email_user', $email_user)
                    ->get();
        if ($checkWishlist->count() > 0) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Wishlisted";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Not wishlisted";
        }
        return $response;
    }
}
