<?php
namespace App\Http\Middleware;
use Closure;
use Exception;
use App\User;
use App\MsUser;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('token', '');
        
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'code' => 401,
                'message' => 'Token not provided.',
                'value' => null,
                'error' => 'Anda tidak dapat mengakses, silahkan login ulang.'
            ], 401);
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'code' => 401,
                'message' => 'Provided token is expired.',
                'value' => null,
                'error' => 'Anda tidak dapat mengakses, silahkan login ulang',
            ], 400);
        } catch(Exception $e) {
            return response()->json([
                'code' => 401,
                'message' => 'An error while decoding token.',
                'value' => null,
                'error' => 'Anda tidak dapat mengakses, silahkan login ulang'
            ], 400);
        }
        $user = MsUser::getUser($credentials->sub);
        // Now let's put the user in the request class so that you can grab it from there
        $request->auth = $user;
        return $next($request);
    }
}