<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\TrOrder as TrOrder;
use App\MsNotification as MsNotification;

class NotificationController extends Controller
{

    public function createNotif(Request $request)
    {           
        try {
            $create_user = MsNotification::createNotif($request->all());
            return $create_user;
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function quickRandom($length = 5)
    {
        $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
    public function createNotifBayar(Request $request)
    {
        // return $data_response;
        if($request->get('notify_type') == 'va_inquiry'){
            $id_order = '95';
            $get_order = TrOrder::getDataOrder($id_order);
            $create_pay = MsNotification::createPaymentBckp($request->all());
            $response_va_inquiry = '{
                "customer_name": "'.$get_order->pembeli_name.'",
                "item_name": "Pembayaran pesanan dengan Order ID '.$get_order->id_order.'",
                "amount": "'.$get_order->total_payment.'",
                "display_text": '. $request.'
            }';
            
            return $response_va_inquiry;
        } else if($request->get('notify_type') == 'va_payment'){
            $reference_id = "ETL".$this->quickRandom();
            $update_payment = MsNotification::updatePayment($request->all(), $reference_id);
            if($update_payment["status"]){
                $response_va_payment = '{
                    "reference_id": "'.$reference_id.'"
                }';
            } else {
                $response_va_payment = '{
                    "reference_id": "'."0".'"
                }';
            }
            return $response_va_payment;
            
        } else if($request->get('notify_type') == 'payment_status'){
            
            $response["success"] = true;
            $response["reference_id"] = $request->get('reference_id');
            $response_payment_status = $response;
            return $response_payment_status;
        }
    }
    public function createNotifBayarB(Request $request)
    {
        // dd($this->quickRandom());
        if($request->get('notify_type') == 'va_inquiry'){
            $id_order = '95';
            $reference_id = "ETL".$this->quickRandom();
            $get_order = TrOrder::getDataOrder($id_order);
            $create_pay = MsNotification::createPayment($request->all(),$reference_id);
            $reference_id = "ETL124";
            $response_va_inquiry = '{
                "customer_name": "'.$get_order->pembeli_name.'",
                "item_name": "Pembayaran pesanan dengan Order ID '.$get_order->id_order.'",
                "amount": "'.$get_order->total_payment.'",
                "reference_id": "ETL1234",
                "display_text": ' . $request.'
            }';
            
            return $response_va_inquiry;
        } else if($request->get('notify_type') == 'va_payment'){
            $va_code = $request->get('va_code');
            $reference_id = "ETL".$this->quickRandom();
            $get_payment = MsNotification::getPayment($va_code);
            
            $reference_id = $get_payment['value']->reference_id;
            $reference_id = "ETL124";
            if($get_payment["status"]){
                $response_va_payment = '{
                    "reference_id": "ETL1234",
                }';
            } else {
                $response_va_payment = '{
                    "reference_id": "'."0".'",
                }';
            }
            return $response_va_payment;
            
        } else if($request->get('notify_type') == 'payment_status'){
            
            $response["success"] = true;
            $response["reference_id"] = "ETL1234";
            $response_payment_status = $response;
            return $response_payment_status;
        }
    }
    public function createNotifBayarBckp(Request $request)
    {
        // dd($this->quickRandom());
        if($request->get('notify_type') == 'va_inquiry'){
            $id_order = '95';
            $get_order = TrOrder::getDataOrder($id_order);
            $create_pay = MsNotification::createPayment($request->all());
            $response_va_inquiry = '{
                "customer_name": "'.$get_order->pembeli_name.'",
                "item_name": "Pembayaran pesanan dengan Order ID '.$get_order->id_order.'",
                "amount": "'.$get_order->total_payment.'",
                "display_text": ' . $request.'
            }';
            
            return $response_va_inquiry;
        } else if($request->get('notify_type') == 'va_payment'){
            $reference_id = "ETL".$this->quickRandom();
            $update_payment = MsNotification::updatePayment($request->all(), $reference_id);
            if($update_payment["status"]){
                $response_va_payment = '{
                    "reference_id": "'.$reference_id.'",
                }';
            } else {
                $response_va_payment = '{
                    "reference_id": "'."0".'",
                }';
            }
            return $response_va_payment;
            
        } else if($request->get('notify_type') == 'payment_status'){
            
            $response["success"] = true;
            $response["reference_id"] = $request->get('reference_id');
            $response_payment_status = $response;
            return $response_payment_status;
        }
    }
    public function readNotif(Request $request)
    {
        try {
            $date_now_ymd = date('Y-m-d');
            // dd($birth_date);
            $update_notif = MsNotification::readNotif($request->all());
            return $update_notif;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListNotifPerUser(Request $request)
    {
        try {
            // dd('okok');
            @$email = $request->get('email');
            // dd($search_val);
            $list_user = MsNotification::getListNotifPerUser($email);
            return $list_user;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function countNotifUnread(Request $request)
    {
        try {
            // dd('okok');
            @$email = $request->get('email');
            // dd($search_val);
            $list_user = MsNotification::countNotifUnread($email);
            return $list_user;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getNotif(Request $request)
    {
        try {
            $id_user_notification = $request->input('id_user_notification');
            $get_user_notif = MsNotification::getNotif($id_user_notification);
            return $get_user_notif;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    
}
