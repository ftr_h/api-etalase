<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsUser as MsUser;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Carbon\Carbon;

class UserController extends Controller
{

    protected function jwt($user) {
        // dd($user);
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->email, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => Carbon::now()->addYear()->timestamp // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 
    public function postLogin(Request $request)
    {
        try {
            $email = $request->get('email');
            $password = $request->get('password');
            $check_login = MsUser::checkLogin($email, $password);
            if ($check_login['status']) {
                $jwt_token = $this->jwt($check_login['value']);
                $check_login["token"] = $jwt_token;
            }
            return $check_login;
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function createUser(Request $request)
    {           
        try {
            $name = $request->get('name');
            $email = $request->get('email');
            // $username = $request->get('username');
            $password = Hash::make($request->get('password'));
            $phone = $request->get('phone');
            $address = $request->get('address');
            $gender = $request->get('gender');
            
            $create_user = MsUser::createUser($name,$email,$password,$phone,$address,$gender);
            if($create_user['status']){
                $url = 'https://backendetalase.ran-it.com/send_mail?email='.$email;

                //open connection
                $ch = curl_init();

                //set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                //execute post
                $result = curl_exec($ch);
                // $response["email_message"] = $result['message'];
                //close connection
                curl_close($ch);
                $jwt_token = $this->jwt($create_user['value']);
                $get_user = MsUser::getUser($email);
                $response["token"] = $jwt_token;
                $response["value"] = $get_user['value'];
                $response["value_cart"] = $create_user['value_cart'];
            } else {
                
            }
            // return $result;
            $response["status"] = $create_user['status'];
            $response["code"] = $create_user['code'];
            $response["error"] = null;
            $response["message"] = $create_user['message'];
            return $response;
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function updateUser(Request $request)
    {
        try {
            $name = $request->get('name');
            $phone = $request->get('phone');
            $email = $request->get('email');
            $birth_date = $request->get('birth_date');
            // $username = $request->get('username');
            $jenis_kelamin = $request->get('gender');
            $date_now_ymd = date('Y-m-d');
            // dd($birth_date);
            $update_user = MsUser::updateUser($name,$phone,$birth_date,$jenis_kelamin,$email);
            return $update_user;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function deleteUser(Request $request)
    {
        try {
            $email = $request->get('email');
            $date_now_ymd = date('Y-m-d');
            // dd($birth_date);
            $delete_user = MsUser::deleteUser($email);
            return $delete_user;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    
    public function updatePasswordUser(Request $request)
    {
        try {
            $email = $request->get('email');
            $old_password = $request->get('old_password');
            $new_password = Hash::make($request->get('new_password'));
            $conf_password = $request->get('conf_password');
            
            $update_password = MsUser::updatePasswordUser($email,$old_password,$new_password,$conf_password);
            return $update_password;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getListUser(Request $request)
    {
        try {
            // dd('okok');
            @$search_val = $request->get('search_val');
            // dd($search_val);
            $list_user = MsUser::getListUser();
            return $response;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getUser(Request $request)
    {
        try {
            $email = str_replace(' ','', $request->input('email'));
            $get_user = MsUser::getUser($email);
            return $get_user;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    
    public function uploadProfileImage(Request $request)
    {
        try {
            // dd('');
            $email_user = $request->get('email');
            // dd(app()->basePath('/files/cheff/'.$email_user));
            $base_path = app()->basePath('public');
            // $this->validate($request, [
            //     'file' => 'required|file',
            // ]); 
            // dd($request->all());
            if ($request->hasFile('file')) {
                
                $link_to_db = '/files/'.$email_user.'/profile/';
                $file = $request->file('file');
                $name = 'profile'.'.'.$file->getClientOriginalExtension();
                $destinationPath = app()->basePath('public'.$link_to_db);
                $file->move($destinationPath, $name);
                // dd(url($link_to_db.'/'.$name));
                $link = $link_to_db.$name;
                $upload_profile = MsUser::updateProfileImage($email_user, $link);
                return $upload_profile;
            } else{
                $response["value"] = $request->all();
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "File yang anda masukkan kosong";
            }
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
}
