<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsAddress as MsAddress;

class AddressController extends Controller
{

    public function createAddress(Request $request)
    {           
        try {
            $create_user = MsAddress::createAddress($request->all());
            return $create_user;
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function defaultAddress(Request $request)
    {
        try {
            // dd($birth_date);
            $update_notif = MsAddress::defaultAddress($request->all());
            return $update_notif;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function updateAddress(Request $request)
    {
        try {
            // dd($birth_date);
            $update_notif = MsAddress::updateAddress($request->all());
            return $update_notif;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListAddressPerUser(Request $request)
    {
        try {
            // dd('okok');
            @$email = $request->get('email');
            // dd($search_val);
            $list_user = MsAddress::getListAddressPerUser($email);
            return $list_user;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getAddress(Request $request)
    {
        try {
            $id_user_address = $request->input('id_user_address');
            $get_user_notif = MsAddress::getAddress($id_user_address);
            return $get_user_notif;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function deleteAddress(Request $request) {
      
        $id_user_address = $request->input('id_user_address');
        $deleteAddress = MsAddress::deleteAddress($id_user_address);
          
        if($deleteAddress){
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Sukses hapus data";
        }else{
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Gagal hapus data";
        }
        return $response;
    }
    
}
