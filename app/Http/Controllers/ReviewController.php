<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsReview as MsReview;

class ReviewController extends Controller
{

    public function createReview(Request $request)
    {           
        try {
            $create_user = MsReview::createReview($request->all());
            return $create_user;
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function updateReview(Request $request)
    {
        try {
            // dd($birth_date);
            $update_notif = MsReview::updateReview($request->all());
            return $update_notif;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListReviewPerUser(Request $request)
    {
        try {
            // dd('okok');
            @$email = $request->get('email');
            // dd($search_val);
            $list_user = MsReview::getListReviewPerUser($email);
            return $list_user;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListReviewPerFood(Request $request)
    {
        try {
            // dd('okok');
            @$id_food = $request->get('id_food');
            // dd($search_val);
            $list_review = MsReview::getListReviewPerFood($id_food);
            return $list_review;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getReview(Request $request)
    {
        try {
            $id_review = $request->input('id_review');
            $get_user_notif = MsReview::getReview($id_review);
            return $get_user_notif;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function deleteReview(Request $request) {
      
        $id_review = $request->input('id_review');
        $deleteReview = MsReview::deleteReview($id_review);
          
        if($deleteReview){
            $response["value"] = 1;
            $response["code"] = 200;
            $response["message"] = "Sukses hapus data";
        }else{
            $response["value"] = 0;
            $response["code"] = 500;
            $response["message"] = "Gagal hapus data";
        }
        return $response;
    }
    
}
