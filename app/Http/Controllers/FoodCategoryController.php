<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsFoodCategory as MsFoodCategory;
use App\MsCheff as MsCheff;

class FoodCategoryController extends Controller
{

    public function createFoodCategory(Request $request)
    {           
        $create_cat = MsFoodCategory::createFoodCategory($request->all());
        return $create_cat;
            
    }

    public function getListFoodCategory(Request $request)
    {
        $email = $request->get('email_user');
        $sql = MsFoodCategory::getListFoodCategory($email);
        return $sql;
    }
    public function getFoodCategory(Request $request)
    {
        $id_food_category = $request->get('id_food_category');
        $sql = MsFoodCategory::getFoodCategory($id_food_category);
        return $sql;
    }

    public function updateFoodCategory(Request $request)
    {
        // dd($birth_date);
        $update_cat = MsFoodCategory::updateFoodCategory($request->all());
        return $update_cat;
    }

    public function deleteFoodCategory(Request $request) {
        $id_food_category = $request->input('id_food_category');
        $deleteFoodCategory = MsFoodCategory::deleteFoodCategory($id_food_category);
         
        return $deleteFoodCategory;
    }
    
}
