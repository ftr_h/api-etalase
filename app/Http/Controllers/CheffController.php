<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsUser as MsUser;
use App\MsCheff as MsCheff;
use App\MsFood as MsFood;
use App\MsFollow as MsFollow;
use App\MsWishlist as MsWishlist;

class CheffController extends Controller
{

    public function getListCheff(Request $request)
    {
        try {
            $search_val = $request->get('search_val');
            $list_cheff = Mscheff::getListCheff($search_val);
            return $list_cheff;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }

    }

    public function createCheff(Request $request)
    {
        try {
            $check_chef = MsCheff::where('email_user',$request->get('email'))->get();
            if($check_chef->count() > 0){
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Akun anda telah terdaftar sebagai chef";
                return $response;
            }
            $create_cheff = MsCheff::createCheff($request->all());
            return $create_cheff;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getCheff(Request $request)
    {
        // dd('pp[');
        try {
            $email = $request->get('email');
            $get_cheff = MsCheff::getCheff($email);
            return $get_cheff;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getCheffFoodTest(Request $request)
    {
        // dd('pp[');
        try {
            $email = $request->get('email');
            
            $get_cheff_food = MsCheff::getCheffFoodT($email);
            return $get_cheff_food;
        } catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    // catch (Exception $e) {
    //         $response["status"] = false;
    //         $response["code"] = 200;
    //         $response["error"] = null;
    //         $response["message"] = "Error : ".$e->getMessage();
    //         return $response;
    //     }
    }
    public function getCheffFood(Request $request)
    {
        // dd('pp[');
        try {
            $get_cheff_food = MsCheff::getCheffFood($request->all());
            return $get_cheff_food;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function updateCheff(Request $request)
    {
        try {
            $update = MsCheff::updateCheff($request->all());
            return $update;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    
    public function uploadCheffGallery(Request $request)
    {
        try {
            // dd('');
            $email_user = $request->get('email');
            // dd(app()->basePath('/files/cheff/'.$email_user));
            $base_path = app()->basePath('public');
            $type = $request->get('type');
            $source = $request->get('source');
            $link = $request->get('link');
            $is_default = $request->get('is_default');
            // $this->validate($request, [
            //     'file' => 'required|file',
            // ]); 
            $new_id = MsCheff::newIDCheffGallery();
            // dd($new_id);
            if ($source == 'Internal') {
                if ($request->hasFile('file')) {
                    
                    $link_to_db = '/files/'.$email_user.'/cheff/';
                    $file = $request->file('file');
                    $name = 'file'.$new_id.'.'.$file->getClientOriginalExtension();
                    $destinationPath = app()->basePath('public'.$link_to_db);
                    $file->move($destinationPath, $name);
                    $link_internal = $link_to_db.$name; 
                    $upload_img_internal = MsCheff::uploadCheffGalleryInternal($new_id,$email_user,$type,$source,$link_internal,$is_default);
                    return $upload_img_internal;
                } else{
                    $response["value"] = $request->all();
                    $response["status"] = false;
                    $response["code"] = 500;
                    $response["error"] = null;
                    $response["message"] = "File yang anda masukkan kosong";
                }
            }else if ($source == 'YouTube') {
                $upload_img_yt = MsCheff::uploadCheffGalleryYT($new_id,$email_user,$type,$source,$link,$is_default);
                return $upload_img_yt;
            } else {
                $response["value"] = $request->all();
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Upload file bukan Internal";
                return $response;
            }
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    
    public function getListCheffGallery(Request $request)
    {
        try {
            $email = $request->get('email');
            
            $list_gallery = MsCheff::getListCheffGallery($email);
            return $list_gallery;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }

    }
    
    public function getListCheffSearch(Request $request)
    {
        try {
        
            $lat1 = (float)$request->get('latitude'); 
            $lon1 = (float)$request->get('longitude'); 
            $order_by = $request->get('order_by'); 
            $search_val = $request->get('search_val'); 
            
            $cart = array();
            // dd('a');
            $sql = MsCheff::getListCheffSearch($search_val);
            
            for ($i=0; $i < $sql->count(); $i++) { 
                $rating = 0;
                if ($sql[$i]->total_vote != 0 || $sql[$i]->total_vote != null){
                    $rating = $sql[$i]->rating / $sql[$i]->total_vote;
                }
                $lat2 = (float)$sql[$i]->latitude; 
                $lon2 = (float)$sql[$i]->longitude; 
                
                
                $sql_food = MsCheff::getListCheffSearchFood($sql[$i]->email_user);
                
                
                if (($lat1 == $lat2) && ($lon1 == $lon2)) {
                    $distance = 0;
                }
                else {
                    $theta = $lon1 - $lon2;
                    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                    $dist = acos($dist);
                    $dist = rad2deg($dist);
                    $miles = $dist * 60 * 1.1515;
                    $unit = strtoupper('K');
                    
                    $distance = ($miles * 1.609344);
                }
                if($sql_food->count() > 0){
                    $cart[] = array(
                        'cheff_name' => $sql[$i]->name,
                        'email' => $sql[$i]->email,
                        'rating' => $sql[$i]->cheff_rating,
                        'distance' => number_format((float)$distance, 2, '.', ''),
                        'longitude' => $sql[$i]->longitude,
                        'latitude' => $sql[$i]->latitude,
                        'city' => $sql[$i]->kota,
                        'list_food' => $sql_food,
                        'cheff_image' => $sql[$i]->image);
                }
            // 	dump($sql[$i]);
            }
            // etc
            if($order_by == 'Rating'){
                $columns = array_column($cart, 'rating');
                array_multisort($columns, SORT_DESC, $cart);
            } else{
                $columns = array_column($cart, 'distance');
                array_multisort($columns, SORT_ASC, $cart);
            }
            // dd($cart);
            // dd(count($cart));
            if(count($cart) > 0){
                $response["value"] = $cart;
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Cheff berhasil ditemukan.";
            } else{
                $response["value"] = null;
                $response["status"] = false;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Cheff tidak ditemukan.";
            }
            return $response;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListRecommendCheff(Request $request)
    {
        try {
        
            $lat1 = (float)$request->get('latitude'); 
            $lon1 = (float)$request->get('longitude'); 
            $order_by = $request->get('order_by'); 
            $offset = $request->get('offset');
            $limit = $request->get('limit');
            // return $limit;
            $request_arr = $request->all();
            if(array_key_exists('email_user_now', $request_arr)){
                $email_user_now = $request_arr['email_user_now'];
            } else {
                $email_user_now = '';
            }
            
            $cart = array();
            // dd('a');
            $sql = MsCheff::getListRecommendCheff($lat1,$lon1,$offset,$limit);
            // return $sql;
            for ($i=0; $i < $sql->count(); $i++) { 
                
                $sql_food = MsFood::getFood($sql[$i]->id_food);
                $get_cheff = MsCheff::getCheff($sql[$i]->email_user);
                $sql_follower_cheff = MsFollow::getListFollower($sql[$i]->email_user);
                if($sql_follower_cheff["value"] != null){
                    $count_follower_cheff = $sql_follower_cheff["value"]->count();
                } else {
                    $count_follower_cheff = 0;
                }
                $checkWishlist = MsWishlist::checkWishlist($sql_food['value']->id_food,$email_user_now);
                $checkFollow = MsFollow::checkFollow($sql[$i]->email_user,$email_user_now);
                $distance = number_format((float)$sql[$i]->distance, 2, '.', '');
                unset($sql[$i]->distance);
                unset($sql[$i]->top_id_food);
                unset($sql[$i]->password);
                $cart[] = array(
                    'food_data' => $sql_food['value'],
                    'cheff_data' => $get_cheff['value'],
                    'distance' => $distance,
                    'cheff_follower' => $count_follower_cheff,
                    'food_rating' => $sql_food['value']->food_rating,
                    'is_wishlisted' => $checkWishlist["status"],
                    'is_followed' => $checkFollow["status"]);
            // 	dump($sql[$i]);
            }
            // $columns = array_column($cart, 'food_rating');
            // array_multisort($columns, SORT_DESC, $cart);
            // dd($cart);
            // dd(count($cart));
            if(count($cart) > 0){
                $response["value"] = $cart;
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Cheff berhasil ditemukan.";
            } else{
                $response["value"] = null;
                $response["status"] = false;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Cheff tidak ditemukan.";
            }
            return $response;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    // public function testDistance(Request $request)
    // {
    //     try {
            
    //         return $response;
    //     } catch (Exception $e) {
    //         $response["status"] = false;
    //         $response["code"] = 200;
    //         $response["error"] = null;
    //         $response["message"] = "Error : ".$e->getMessage();
    //         return $response;
    //     }

    // }
}
