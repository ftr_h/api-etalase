<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsFollow as MsFollow;

class FollowController extends Controller
{

    public function createFollow(Request $request)
    {           
        try {
            $follower = $request->input('email_follower');
            $following = $request->input('email_following');
            $checkFollow = MsFollow::checkFollow($following,$follower);
            if($checkFollow["status"] == true){
                return $checkFollow;
            }else {
                $create_user = MsFollow::createFollow($request->all());
                return $create_user;
            }
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getListFollowing(Request $request)
    {
        try {
            // dd('okok');
            @$email = $request->get('email');
            // dd($search_val);
            $list_user = MsFollow::getListFollowing($email);
            return $list_user;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListFollower(Request $request)
    {
        try {
            // dd('okok');
            @$email = $request->get('email');
            // dd($search_val);
            $list_follow = MsFollow::getListFollower($email);
            return $list_follow;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function deleteFollow(Request $request) {
        $follower = $request->input('email_follower');
        $following = $request->input('email_following');
        $deleteFollow = MsFollow::deleteFollow($following,$follower);
         
        return $deleteFollow;
    }
    public function checkFollow(Request $request) {
        $follower = $request->input('email_follower');
        $following = $request->input('email_following');
        $checkFollow = MsFollow::checkFollow($following,$follower);
         
        return $checkFollow;
    }
    
}
