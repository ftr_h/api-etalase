<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\TrOrder as TrOrder;
use App\TrOrderDetail as TrOrderDetail;
use App\MsCheff as MsCheff;
use App\MsFood as MsFood;
use App\MsAddress as MsAddress;
use App\MsPayment as MsPayment;

class OrderController extends Controller
{
    public function createOrderNeo(Request $request)
    {
        $order_detail =$request->get('order_detail') ;
        dd(count($order_detail));
        // dd($order_detail[0]);
        $create_order = TrOrder::createOrderNew($request->all());
        // dd($create_order['status']);
        if ($create_order['status']) {
            for ($i=0; $i < count($order_detail); $i++) { 
                $create_order_detail = TrOrderDetail::createOrderDetailNew($create_order['value'],$order_detail[$i]);                
                $get_food = MsFood::getFood($order_detail[$i]['id_food']);
                if ( filter_var($get_food['value']->limit_order, FILTER_VALIDATE_INT)) {
                    $min_stock = MsFood::minusStock($order_detail[$i]['id_food'],$order_detail[$i]['amount']);
                }

            }
        }
        
    }
    public function createOrderNiu(Request $request)
    {
        $email_pembeli =$request->get('email_pembeli') ;
        $email_cheff =$request->get('email_cheff') ;
        $order_detail =$request->get('order_detail') ;
        $ongkir =$request->get('ongkir') ;
        $address =$request->get('address') ;
        $lat_pembeli =$request->get('latitude') ;
        $long_pembeli =$request->get('longitude') ;
        if(!is_array($order_detail)){
            $order_detail =json_decode($request->get('order_detail'), true) ;
        }
        $order_type = '1';
        $delivery_date = date('Y-m-d');
        $total_price = 0;
        $tax = 0;
        for ($i=0; $i < count($order_detail); $i++) { 
            $checkFood = MsFood::where('id_food',$order_detail[$i]['id_food'])->first();
            if($checkFood->order_type == 2){
                $order_type = '2';
            }
            if($checkFood->available_at > $delivery_date){
                $delivery_date = $checkFood->available_at;
            }
            $total_price += $checkFood->price * $order_detail[$i]['amount'];
        }
        $checkTax = DB::table('ms_app_setting')->where('code_setting','tax_cost')->first();
        $tax = $total_price * (int)$checkTax->content/100;
        $create_order = TrOrder::createOrderNiu($email_pembeli,$email_cheff,$total_price,$tax,$ongkir,$delivery_date,$order_type,$lat_pembeli,$long_pembeli,$address);
        if ($create_order['status']) {
            for ($i=0; $i < count($order_detail); $i++) { 
                $create_order_detail = TrOrderDetail::createOrderDetailNew($create_order['value'],$order_detail[$i]);                
                $get_food = MsFood::getFood($order_detail[$i]['id_food']);
                if ( filter_var($get_food['value']->limit_order, FILTER_VALIDATE_INT)) {
                    $min_stock = MsFood::minusStock($order_detail[$i]['id_food'],$order_detail[$i]['amount']);
                }

            }
            $cancel_old_cart = TrOrder::cancelOldCart($request->get('email_pembeli'));
            $except_me = TrOrder::changeOrderStatus($create_order['value'],'1');
        }

        return $create_order;

    }
    public function checkOngkirNew(Request $request)
    {
        $id_user_address = $request->get('id_user_address');
        $email_cheff = $request->get('email_cheff');
        $ongkir = TrOrder::checkOngkir();
        $cheff = MsCheff::getCheff($email_cheff);
        // dd($cheff);
        $lat1 = (float)$cheff['value']->latitude; 
        $lon1 = (float)$cheff['value']->longitude; 
        // dd($lat1);
        $address = MsAddress::getAddress($id_user_address);
        // dd(json_encode($address->latitude));
        $lat2 = (float)$request->get('latitude');
        $lon2 = (float)$request->get('longitude'); 
        
        
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            $distance = 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper('K');
            
            $distance = ($miles * 1.609344);
        }
        
        $total_ongkir = $ongkir * $distance;
        // dd(number_format((float)$distance, 2, '.', ''));
        $total_ongkir=ceil($total_ongkir);
        if (substr($total_ongkir,-3)>499){
            $total_harga=round($total_ongkir,-3);
        } else {
            $total_harga=round($total_ongkir,-3)+1000;
        }
        $response['value'] = (int)$total_harga;
        $response["status"] = true;
        $response["code"] = 200;
        $response["error"] = null;
        return $response ;
    }
    public function createOrder(Request $request)
    {           
        try {
            $create_food = TrOrder::createOrder($request->all());
            return $create_food;
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function createOrderNew(Request $request)
    {           
        try {
            $get_cheff = MsCheff::getCheff($request->get('email_cheff'));
            // dd($get_cheff['value']->name);
            $create_food = TrOrder::createOrderNew($request->all());
            return $create_food;
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function createOrderDetailNew(Request $request)
    {           
        try {
            $getHeaderKey = $request->header('Etalase-key', '');
            if($getHeaderKey == null){
                $check = [];
                $check["Status"] = true;
                // dd($request->all());
                if ($check["Status"] == true) {
                    $create_food = TrOrderDetail::createOrderDetailNew($request->get('id_order'),$request->all());
                    return $create_food;
                } else {
                    $response['value'] = "";
                    $response["status"] = false;
                    $response["code"] = 200;
                    $response["error"] = null;
                    $response["message"] = $check["Message"];
                    return $response;
                }
            } else {
                $response['value'] = "";
                $response["status"] = false;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Anda tidak memiliki hak akses";
                return $response;
            }
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    
    public function createOrderDetail(Request $request)
    {           
        try {
            $getHeaderKey = $request->header('Etalase-key', '');
            if($getHeaderKey == null){
                $check = [];
                $check["Status"] = true;
                // dd($check["Status"]);
                if ($check["Status"] == true) {
                    $create_food = TrOrderDetail::createOrderDetail($request->all());
                    return $create_food;
                } else {
                    $response['value'] = "";
                    $response["status"] = false;
                    $response["code"] = 200;
                    $response["error"] = null;
                    $response["message"] = $check["Message"];
                    return $response;
                }
            } else {
                $response['value'] = "";
                $response["status"] = false;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Anda tidak memiliki hak akses";
                return $response;
            }
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getDataOrder(Request $request)
    {
        $id_order = $request->get('id_order');
        $order = TrOrder::getDataOrder($id_order);

        $detail = TrOrderDetail::getDataOrderDetail($id_order);
        // dd($ajax_cheff);
        if ($order != null) {
            $response['value_order'] = $order;
            $response['value_detail'] = $detail;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Data Order berhasil ditemukan";
        } else {
            $response['value_order'] = null;
            $response['value_detail'] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Data Order gagal ditemukan";
        }
        
        return $response;
    }
    public function checkOngkir(Request $request)
    {
        $lat1 = (float)$request->get('latitude'); 
        $lon1 = (float)$request->get('longitude'); 
        $id_user_address = $request->get('id_user_address');
        $ongkir = 5000;

        $address = MsAddress::getAddress($id_user_address);
        // dd(json_encode($address['value']['latitude']));
        $lat2 = (float)$address['value']['latitude']; 
        $lon2 = (float)$address['value']['latitude']; 
        
        
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            $distance = 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper('K');
            
            $distance = ($miles * 1.609344);
        }
        
        $total_ongkir = $ongkir * $distance;
        // dd($ajax_cheff);
        
        return (int)$total_ongkir;
    }
    public function getCart(Request $request)
    {
        $email = $request->get('email');
        $order = TrOrder::getCart($email);

        // dd($ajax_cheff);
        if ($order != null) {
            $detail = TrOrderDetail::getDataOrderDetail($order->id_order);
            $response['value_order'] = $order;
            $response['value_detail'] = $detail;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Keranjang berhasil ditemukan";
        } else {
            $response['value'] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Keranjang gagal ditemukan";
        }
        
        return $response;
    }
    public function getListOrderPerUserByStatus(Request $request)
    {
        try {
            $email = $request->get('email');
            $id_order_status = $request->get('id_order_status');
            $offset = $request->get('offset');
            $limit = $request->get('limit');
            $list_order = TrOrder::getListOrderPerUserByStatus($email,$offset,$limit,$id_order_status);
            return $list_order;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }

    }
    public function getListOrderPerUser(Request $request)
    {
        try {
            $email = $request->get('email');
            $offset = $request->get('offset');
            $limit = $request->get('limit');
            $list_cheff = TrOrder::getListOrderPerUser($email,$offset,$limit);
            if($list_cheff->count() > 0){
                $response["value"] = $list_cheff;
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Daftar order berhasil ditemukan";
            } else{
                $response["value"] = null;
                $response["status"] = false;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Daftar order tidak ditemukan";
            }
            return $response;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }

    }
    public function getListOrderPerCheffByStatus(Request $request)
    {
        try {
            $email = $request->get('email');
            $id_order_status = $request->get('id_order_status');
            $offset = $request->get('offset');
            $limit = $request->get('limit');
            $list_order = TrOrder::getListOrderPerCheffByStatus($email,$offset,$limit,$id_order_status);
            return $list_order;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }

    }
    public function getListOrderPerCheff(Request $request)
    {
        try {
            // dd('aa');
            $email = $request->get('email');
            $list_cheff = TrOrder::getListOrderPerCheff($email);
            return $list_cheff;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }

    }
    public function getListOrderPerCheffActive(Request $request)
    {
        try {
            $email = $request->get('email');
            $list_cheff = TrOrder::getListOrderPerCheffActive($email);
            return $list_cheff;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }

    }
    public function payOrder(Request $request)
    {
        try {
            // dd($birth_date);
            $bayar_order = TrOrder::payOrder($request->all());
            return $bayar_order;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function confirmOrder(Request $request)
    {
        try {
            // dd($birth_date);
            $confirm_order = TrOrder::changeOrderStatus($request->get('id_order'), '3');
            
            if ($confirm_order) {
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Konfirmasi berhasil";
            } else {
                // $response["value"] = $request->all();
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Konfirmasi gagal";
            }
            return $response;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function receiveOrder(Request $request)
    {
        try {
            // dd($birth_date);
            $receive_order = TrOrder::changeOrderStatus($request->get('id_order'), '5');
            
            if ($receive_order) {
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Penerimaan pesanan berhasil";
            } else {
                // $response["value"] = $request->all();
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Penerimaan pesanan gagal";
            }
            return $response;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function shipOrder(Request $request)
    {
        try {
            // dd($birth_date);
            $ship_order = TrOrder::changeOrderStatus($request->get('id_order'), '4');
            
            if ($ship_order) {
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Pesanan berhasil dikirim";
            } else {
                // $response["value"] = $request->all();
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Pesanan gagal dikirim";
            }
            return $response;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function cancelOrder(Request $request)
    {
        try {
            // dd($birth_date);
            $cancel_order = TrOrder::changeOrderStatus($request->get('id_order'), '6');
            
            if ($cancel_order) {
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Pembatalan pesanan berhasil";
            } else {
                // $response["value"] = $request->all();
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Pembatalan pesanan gagal";
            }
            return $response;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function cancelOrderByCheff(Request $request)
    {
        try {
            // dd($birth_date);
            $cancel_order_by_admin = TrOrder::changeOrderStatus($request->get('id_order'), '6');
            
            if ($cancel_order_by_admin) {
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Pembatalan pesanan berhasil";
            } else {
                // $response["value"] = $request->all();
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Pembatalan pesanan gagal";
            }
            return $response;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getFaq1(Request $request)
    {
        $response["value"] = 'https://backendetalase.ran-it.com/app_setting/mobile?code_setting=faq1';
        $response["code"] = 200;
        $response["error"] = null;
        $response["message"] = "Success";
        return $response;
    }
    public function getFaq2(Request $request)
    {
        $response["value"] = 'https://backendetalase.ran-it.com/app_setting/mobile?code_setting=faq2';
        $response["code"] = 200;
        $response["error"] = null;
        $response["message"] = "Success";
        return $response;
    }
    public function getAboutEtalase(Request $request)
    {
        $response["value"] = 'https://backendetalase.ran-it.com/app_setting/mobile?code_setting=about_etalase';
        $response["code"] = 200;
        $response["error"] = null;
        $response["message"] = "Success";
        return $response;
    }
    public function getContactUs(Request $request)
    {
        $response["value"] = 'https://backendetalase.ran-it.com/app_setting/mobile?code_setting=contact_us';
        $response["code"] = 200;
        $response["error"] = null;
        $response["message"] = "Success";
        return $response;
    }
    public function getOrderView(Request $request)
    {           
        try {
            $getHeaderKey = $request->header('Etalase-key', '');
            if($getHeaderKey == null){
                $email = $request->get('email');
                $check = [];
                $check["Status"] = true;
                // dd($check["Status"]);
                if ($check["Status"] == true) {
                    $get_cheff = MsCheff::getCheff($email);
                    $get_food = MsFood::getListFoodPerCheff($email);
                    // $get_cheff = MsCheff::getCheff($email);
                    if($get_cheff['status'] == 'true' && $get_food['status'] == 'true'){
                        $response['cheff_value'] = $get_cheff['value'];
                        $response['food_value'] = $get_food['value'];
                        $response["status"] = true;
                        $response["code"] = 200;
                        $response["error"] = null;
                        $response["message"] = "Data berhasil didapatkan";
                    } else if($get_cheff['status'] == 'false'){
                        $response['value'] = "";
                        $response["status"] = false;
                        $response["code"] = 200;
                        $response["error"] = null;
                        $response["message"] = "Data cheff gagal didapatkan";
                    } else if($get_food['status'] == 'false'){
                        $response['value'] = "";
                        $response["status"] = false;
                        $response["code"] = 200;
                        $response["error"] = null;
                        $response["message"] = "Data masakan gagal didapatkan";
                    } else {
                        $response['value'] = "";
                        $response["status"] = false;
                        $response["code"] = 200;
                        $response["error"] = null;
                        $response["message"] = "Data gagal didapatkan";
                    }
                    return $create_food;
                } else {
                    $response['value'] = "";
                    $response["status"] = false;
                    $response["code"] = 200;
                    $response["error"] = null;
                    $response["message"] = $check["Message"];
                    return $response;
                }
            } else {
                $response['value'] = "";
                $response["status"] = false;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = "Anda tidak memiliki hak akses";
                return $response;
            }
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

}
