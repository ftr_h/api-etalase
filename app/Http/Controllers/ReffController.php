<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsUser as MsUser;
use App\ReffTable as ReffTable;

class ReffController extends Controller
{

    public function getListBank(Request $request)
    {
        try {
            // dd('okok');
            @$search_val = $request->get('search_val');
            $list = ReffTable::getListBank();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListBelajar(Request $request)
    {
        try {
            $list = ReffTable::getListBelajar();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListCategoryKoki(Request $request)
    {
        try {
            $list = ReffTable::getListCategoryKoki();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListCuisine(Request $request)
    {
        try {
            $list = ReffTable::getListCuisine();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListKeahlian(Request $request)
    {
        try {
            $list = ReffTable::getListKeahlian();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListTypeDiet(Request $request)
    {
        try {
            $list = ReffTable::getListTypeDiet();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListProvinsi(Request $request)
    {
        try {
            $list = ReffTable::getListProvinsi();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListKota(Request $request)
    {
        try {
            @$id_provinsi = $request->get('id_provinsi');
            $list = ReffTable::getListKota($id_provinsi);
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListKecamatan(Request $request)
    {
        try {
            @$kota_kab = $request->get('kota_kab');
            $list = ReffTable::getListKecamatan($kota_kab);
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListKodepos(Request $request)
    {
        try {
            @$kecamatan = $request->get('kecamatan');
            $list = ReffTable::getListKodepos($kecamatan);
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListServingType(Request $request)
    {
        try {
            $list = ReffTable::getListServingType();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListHidanganType(Request $request)
    {
        try {
            $list = ReffTable::getListHidanganType();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListEstCooking(Request $request)
    {
        try {
            $list = ReffTable::getListEstCooking();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListCookingMethod(Request $request)
    {
        try {
            $list = ReffTable::getListCookingMethod();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListOrderType(Request $request)
    {
        try {
            $list = ReffTable::getListOrderType();
            return $list;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
}
