<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\TrOrder as TrOrder;
use App\MsNotification as MsNotification;
use App\MsPayment as MsPayment;
use App\MsUser as MsUser;

// MIDTRANS
// Configurations
use App\Http\Controllers\Midtrans\Config;

// Midtrans API Resources
use App\Http\Controllers\Midtrans\Transaction;

// Plumbing
use App\Http\Controllers\Midtrans\ApiRequestor;
use App\Http\Controllers\Midtrans\SnapApiRequestor;
use App\Http\Controllers\Midtrans\Notification;
use App\Http\Controllers\Midtrans\CoreApi;
use App\Http\Controllers\Midtrans\Snap;

// Sanitization
use App\Http\Controllers\Midtrans\Sanitizer;

class PaymentController extends Controller
{

    public function quickRandom($length = 5)
    {
        $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
    public function afterPaymentMidtrans(Request $request)
    {
        $date_now_ymd = date("Y-m-d H:i:s");
        $content = json_encode($request->all());
        $insert_log = DB::insert("INSERT INTO ms_test_log (
                content,
                type,
                created_at
                )
                    values (
                      '".$content."',
                      'after_payment_midtrans',
                      '".$date_now_ymd."'
                    )");
        return $insert_log;
    }
    public function notifyMidtrans(Request $request)
    {
        $date_now_ymd = date("Y-m-d H:i:s");
        $content = json_encode($request->all());
        $insert_log = DB::insert("INSERT INTO ms_test_log (
                content,
                type,
                created_at
                )
                    values (
                      '".$content."',
                      'after_payment_midtrans',
                      '".$date_now_ymd."'
                    )");
        $payload = $request->getContent();
		$notification = json_decode($payload);

		$validSignatureKey = hash("sha512", $notification->order_id . $notification->status_code . $notification->gross_amount . "SB-Mid-server-EntaESltgX51J7wO-ddsi4ZI");

		if ($notification->signature_key != $validSignatureKey) {
			return response(['message' => 'Invalid signature'], 403);
		}

		// Set your Merchant Server Key
		Config::$serverKey = "SB-Mid-server-EntaESltgX51J7wO-ddsi4ZI";
		// Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
		Config::$isProduction = false;
		// Set sanitization on (default)
		Config::$isSanitized = true;
		// Set 3DS transaction for credit card to true
		Config::$is3ds = true;
		
		$statusCode = null;

		$paymentNotification = new Notification();

		$transaction = $paymentNotification->transaction_status;
		$type = $paymentNotification->payment_type;
		$orderId = $paymentNotification->order_id;
		$fraud = $paymentNotification->fraud_status;

		$vaNumber = null;
		$vendorName = null;
		if (!empty($paymentNotification->va_numbers[0])) {
			$vaNumber = $paymentNotification->va_numbers[0]->va_number;
			$vendorName = $paymentNotification->va_numbers[0]->bank;
		}

		$paymentStatus = null;
		if ($transaction == 'capture') {
			// For credit card transaction, we need to check whether transaction is challenge by FDS or not
			if ($type == 'credit_card') {
				if ($fraud == 'challenge') {
					// TODO set payment status in merchant's database to 'Challenge by FDS'
					// TODO merchant should decide whether this transaction is authorized or not in MAP
					$paymentStatus = "CHALLENGE";
				} else {
					// TODO set payment status in merchant's database to 'Success'
					$paymentStatus = "SUCCESS";
				}
			}
		} else if ($transaction == 'settlement') {
			// TODO set payment status in merchant's database to 'Settlement'
			$paymentStatus = "SETTLEMENT";
		} else if ($transaction == 'pending') {
			// TODO set payment status in merchant's database to 'Pending'
			$paymentStatus = "PENDING";
		} else if ($transaction == 'deny') {
			// TODO set payment status in merchant's database to 'Denied'
			$paymentStatus = "DENY";
		} else if ($transaction == 'expire') {
			// TODO set payment status in merchant's database to 'expire'
			$paymentStatus = "EXPIRE";
		} else if ($transaction == 'cancel') {
			// TODO set payment status in merchant's database to 'Denied'
			$paymentStatus = "CANCEL";
		}

		$paymentParams = [
			'order_id' => $paymentNotification->order_id,
			'number' => $paymentNotification->order_id,
			'amount' => $paymentNotification->gross_amount,
			'method' => 'midtrans',
			'status' => $paymentStatus,
			'token' => $paymentNotification->transaction_id,
			'payloads' => $payload,
			'payment_type' => $paymentNotification->payment_type,
			'va_number' => $vaNumber,
			'vendor_name' => $vendorName,
			'biller_code' => $paymentNotification->biller_code,
			'bill_key' => $paymentNotification->bill_key,
		];


		$message = 'Payment status is : '. $paymentStatus;

		$response = [
			'code' => 200,
			'message' => $message,
		];

		return response($response, 200);
    }
    public function redirectDoku(Request $request)
    {
        $date_now_ymd = date("Y-m-d H:i:s");
        $content = json_encode($request->all());
        $insert_log = DB::insert("INSERT INTO ms_test_log (
                content,
                type,
                created_at
                )
                    values (
                      '".$content."',
                      'redirect_doku',
                      '".$date_now_ymd."'
                    )");
        return $insert_log;
    }
    public function verifyDoku(Request $request)
    {
        $date_now_ymd = date("Y-m-d H:i:s");
        $content = json_encode($request->all());
        $insert_log = DB::insert("INSERT INTO ms_test_log (
                content,
                type,
                created_at
                )
                    values (
                      '".$content."',
                      'verify_doku',
                      '".$date_now_ymd."'
                    )");
        return $insert_log;
    }
    public function notifyDoku(Request $request)
    {
        $date_now_ymd = date("Y-m-d H:i:s");
        $content = json_encode($request->all());
        $insert_log = DB::insert("INSERT INTO ms_test_log (
                content,
                type,
                created_at
                )
                    values (
                      '".$content."',
                      'notify_doku',
                      '".$date_now_ymd."'
                    )");
        return $insert_log;
    }
    public function createNotifBayar(Request $request)
    {
        if($request->get('notify_type') == 'va_inquiry'){
            // $id_order = '95';
            $va_code = $request->get('va_code');
            $get_payment_channel = MsPayment::getPaymentChannel($va_code);
            $get_order = MsPayment::getDataOrderByVA(substr($va_code, -12));
            // dd($get_order['value']);
            $amount_string = strval($get_order['value']->total_payment + $get_payment_channel['value']->fee);
            $response['customer_name'] = $get_order['value']->pembeli_name;
            $response['item_name'] = 'Pesanan '.$get_order['value']->id_order;
            $response['amount'] = $amount_string;
            $response['display_text'] = "Pembayaran pesanan dengan Order ID ".$get_order['value']->id_order;
            
            $create_payment = MsPayment::createPaymentNiu($get_order['value']->id_order,$va_code,$get_payment_channel['value']->prefix,0,$amount_string);
            return $response;
        } else if($request->get('notify_type') == 'va_payment'){
            // dd($request->all());
            $va_code = $request->get('va_code');
            $get_order = MsPayment::getDataOrderPayment($va_code);
            // dump($va_code);
            // dd($get_order);
            // $reference_id = "ETL".$this->quickRandom();
            $update_payment = MsPayment::updatePayment($request->all(), $get_order['value']->id_order);
            if($update_payment["status"]){
                $response_va_payment = '{
                    "reference_id": "'.$get_order['value']->id_order.'"
                }';
            } else {
                $response_va_payment = '{
                    "reference_id": "'."0".'"
                }';
            }
            $response['reference_id'] = $get_order['value']->id_order;
            return $response;
            
        } else if($request->get('notify_type') == 'payment_status'){
            $id_order = $request->get('reference_id');
            $get_order = TrOrder::getDataOrder($request->get('reference_id'));
            if($request->get('status') == '50000'){
                $notif_pembeli['email'] = $get_order->email_pembeli;
                $notif_pembeli['title'] = "Pembayaran Berhasil";
                $notif_pembeli['message'] = "Pembayaran pesananan dengan No. Transaksi " . $request->get('reference_id') . " berhasil";
                $notif_cheff['email'] = $get_order->email_cheff;
                $notif_cheff['title'] = "Pesanan Baru";
                $notif_cheff['message'] = "Pembayaran pesananan dengan No. Transaksi " . $request->get('reference_id') . " telah berhasil. Dimohon pesanan diproses secepatnya.";
                $create_notif_pembeli = MsNotification::createNotif($notif_pembeli);
                $create_notif_cheff = MsNotification::createNotif($notif_cheff);
                $update_status_order = TrOrder::changeOrderStatus($id_order,'2');
            } else if($request->get('status') == '80000'){
                $notif_pembeli['email'] = $get_order->email_pembeli;
                $notif_pembeli['title'] = "Pembayaran Gagal";
                $notif_pembeli['message'] = "Pembayaran pesananan dengan No. Transaksi " . $request->get('reference_id') . " berstatus Suspect";
                $create_notif_pembeli = MsNotification::createNotif($notif_pembeli);
                
            } else if($request->get('status') == '90000'){
                $notif_pembeli['email'] = $get_order->email_pembeli;
                $notif_pembeli['title'] = "Pembayaran Gagal";
                $notif_pembeli['message'] = "Pembayaran pesananan dengan No. Transaksi " . $request->get('reference_id') . " Gagal. Anda dapat mengubah metode pembayaran dan melakukan pembayaran ulang";
                $create_notif_pembeli = MsNotification::createNotif($notif_pembeli);
            }
            $response["success"] = true;
            $response["reference_id"] = $request->get('reference_id');
            $response_payment_status = json_encode($response);
            return $response_payment_status;
        }
    }
    public function createNotifBayarB(Request $request)
    {
        if($request->get('notify_type') == 'va_inquiry'){
            // $id_order = '95';
            $va_code = $request->get('va_code');
            $get_payment_channel = MsPayment::getPaymentChannel($va_code);
            $get_order = MsPayment::getDataOrderByVA(substr($va_code, -12));
            // dd($get_order['value']);
            $amount_string = strval($get_order['value']->total_payment + $get_payment_channel['value']->fee);
            $response['customer_name'] = $get_order['value']->pembeli_name;
            $response['item_name'] = 'Pesanan '.$get_order['value']->id_order;
            $response['amount'] = $amount_string;
            $response['display_text'] = "Pembayaran pesanan dengan Order ID ".$get_order['value']->id_order;
            
            $create_payment = MsPayment::createPaymentNiu($get_order['value']->id_order,$va_code,$get_payment_channel['value']->prefix,0,$amount_string);
            return $response;
        } else if($request->get('notify_type') == 'va_payment'){
            // dd($request->all());
            $va_code = $request->get('va_code');
            $get_order = MsPayment::getDataOrderPayment($va_code);
            // dump($va_code);
            // dd($get_order);
            // $reference_id = "ETL".$this->quickRandom();
            $update_payment = MsPayment::updatePayment($request->all(), $get_order['value']->id_order);
            if($update_payment["status"]){
                $response_va_payment = '{
                    "reference_id": "'.$get_order['value']->id_order.'"
                }';
            } else {
                $response_va_payment = '{
                    "reference_id": "'."0".'"
                }';
            }
            $response['reference_id'] = $get_order['value']->id_order;
            return $response;
            
        } else if($request->get('notify_type') == 'payment_status'){
            $id_order = $request->get('reference_id');
            $get_order = TrOrder::getDataOrder($request->get('reference_id'));
            if($request->get('status') == '50000'){
                $notif_pembeli['email'] = $get_order->email_pembeli;
                $notif_pembeli['title'] = "Pembayaran Berhasil";
                $notif_pembeli['message'] = "Pembayaran pesananan dengan No. Transaksi " . $request->get('reference_id') . " berhasil";
                $notif_cheff['email'] = $get_order->email_cheff;
                $notif_cheff['title'] = "Pesanan Baru";
                $notif_cheff['message'] = "Pembayaran pesananan dengan No. Transaksi " . $request->get('reference_id') . " telah berhasil. Dimohon pesanan diproses secepatnya.";
                $create_notif_pembeli = MsNotification::createNotif($notif_pembeli);
                $create_notif_cheff = MsNotification::createNotif($notif_cheff);
                $update_status_order = TrOrder::changeOrderStatus($id_order,'2');
            } else if($request->get('status') == '80000'){
                $notif_pembeli['email'] = $get_order->email_pembeli;
                $notif_pembeli['title'] = "Pembayaran Gagal";
                $notif_pembeli['message'] = "Pembayaran pesananan dengan No. Transaksi " . $request->get('reference_id') . " berstatus Suspect";
                $create_notif_pembeli = MsNotification::createNotif($notif_pembeli);
                
            } else if($request->get('status') == '90000'){
                $notif_pembeli['email'] = $get_order->email_pembeli;
                $notif_pembeli['title'] = "Pembayaran Gagal";
                $notif_pembeli['message'] = "Pembayaran pesananan dengan No. Transaksi " . $request->get('reference_id') . " Gagal. Anda dapat mengubah metode pembayaran dan melakukan pembayaran ulang";
                $create_notif_pembeli = MsNotification::createNotif($notif_pembeli);
            }
            $response["success"] = true;
            $response["reference_id"] = $request->get('reference_id');
            $response_payment_status = json_encode($response);
            return $response_payment_status;
        }
    }
    public function createNotifBayarBckp(Request $request)
    {
        // dd($this->quickRandom());
        if($request->get('notify_type') == 'va_inquiry'){
            $id_order = '95';
            $get_order = TrOrder::getDataOrder($id_order);
            $create_pay = MsNotification::createPayment($request->all());
            $response_va_inquiry = '{
                "customer_name": "'.$get_order->pembeli_name.'",
                "item_name": "Pembayaran pesanan dengan Order ID '.$get_order->id_order.'",
                "amount": "'.$get_order->total_payment.'",
                "display_text": ' .  json_encode($request->all()) . '
            }';
            
            return $response_va_inquiry;
        } else if($request->get('notify_type') == 'va_payment'){
            $reference_id = "ETL".$this->quickRandom();
            $update_payment = MsNotification::updatePayment($request->all(), $reference_id);
            if($update_payment["status"]){
                $response_va_payment = '{
                    "reference_id": "'.$reference_id.'",
                }';
            } else {
                $response_va_payment = '{
                    "reference_id": "'."0".'",
                }';
            }
            return $response_va_payment;
            
        } else if($request->get('notify_type') == 'payment_status'){
            
            $response["success"] = true;
            $response["reference_id"] = $request->get('reference_id');
            $response_payment_status = json_encode($response);
            return $response_payment_status;
        }
    }
    
    public function genVA(Request $request)
    {
        $email_pembeli = $request->get('email_pembeli');
        $prefix = $request->get('prefix');
        $get_user = MsUser::getUser($email_pembeli);
        // dd($get_order->pembeli_phone);
        $get_prefix = MsPayment::getPrefix($prefix);
        $va_code = $prefix . $get_user['value']->phone;
        $response['va_code'] = $va_code;
        $response['prefix'] = $get_prefix;
        return $response;
    }
    
    public function submitVA(Request $request)
    {
        try {
            // dd('okok');
            @$va_code = $request->get('virtual_account_number');
            //open connection
            
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL,"https://odeo-core-api.dev.odeo.co.id/v1/test/pg/notify-inquiry");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                        "virtual_account_number=".$va_code);
            
            // In real life you should use something like:
            // curl_setopt($ch, CURLOPT_POSTFIELDS, 
            //          http_build_query(array('postvar1' => 'value1')));
            
            // Receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $server_output = curl_exec($ch);
            
            curl_close ($ch);
            // $server_output1 = json_decode($server_output);
            return $server_output;
            // Further processing ...
            if ($server_output == "OK") { 
                
                
            } else {
                
                
            }
            // dd($search_val);
            $list_user = MsPayment::getListPrefix();
            return $server_output;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function updatePaymentVA(Request $request)
    {
        try {
            // dd('okok');
            $va_code = $request->get('va_code');
            $prefix = $request->get('prefix');
            $id_order = $request->get('id_order');
            //open connection
            
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL,"https://odeo-core-api.dev.odeo.co.id/v1/test/pg/notify-inquiry");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                        "virtual_account_number=".$va_code);
            
            // In real life you should use something like:
            // curl_setopt($ch, CURLOPT_POSTFIELDS, 
            //          http_build_query(array('postvar1' => 'value1')));
            
            // Receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $server_output = curl_exec($ch);
            
            curl_close ($ch);
            $output_decode = json_decode($server_output);
            if($output_decode->responseMessage != "Success"){
                $response_pay['value'] = 0;
                $response_pay['status'] = false;
                $response_pay['code'] = 500;
                $response_pay['message'] = "Gagal generate data pembayaran";
                return $response_pay;
            }
            $update_payment = MsPayment::updatePaymentVA($id_order,$va_code,$prefix,$output_decode->traceNo,$output_decode->billingAmount);
            $update_order = TrOrder::updateOrderVA($id_order,$va_code,$prefix);
            return $update_payment;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getLisPrefix(Request $request)
    {
        try {
            // dd('okok');
            @$email = $request->get('email');
            // dd($search_val);
            $list_user = MsPayment::getListPrefix();
            return $list_user;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getPrefix(Request $request)
    {
        try {
            // dd('okok');
            @$prefix = $request->get('prefix');
            // dd($search_val);
            $list_user = MsPayment::getPrefix($prefix);
            return $list_user;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getPayment(Request $request)
    {
        try {
            // dd('okok');
            @$reference_id = $request->get('id_order');
            // dd($search_val);
            $get_payment = MsPayment::getPaymentData($reference_id);
            return $get_payment;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getPaymentPage(Request $request)
    {
        @$id_order = $request->get('id_order');
        $response["value"] = 'http://backendetalase.ran-it.com/get_payment_page?id_order='.$id_order;
        $response["code"] = 200;
        $response["error"] = null;
        $response["message"] = "Success";
        return $response;
    }
    
}
