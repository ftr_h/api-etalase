<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsNews as MsNews;

class NewsController extends Controller
{

    public function getListNews(Request $request)
    {
        try {
            // dd('okok');
            // dd($search_val);
            $list_news = MsNews::getListNews();
            return $list_news;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getNews(Request $request)
    {
        try {
            // dd($request->all());
            $id_news = $request->get('id_news');
            $get_user_notif = MsNews::getNews($id_news);
            return $get_user_notif;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    
}
