<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsWishlistCheff as MsWishlistCheff;

class WishlistCheffController extends Controller
{

    public function createWishlistCheff(Request $request)
    {           
        try {
            $user = $request->input('email_user');
            $cheff = $request->input('email_cheff');
            $checkWishlistCheff = MsWishlistCheff::checkWishlistCheff($cheff,$user);
            if($checkWishlistCheff["status"] == true){
                return $checkWishlistCheff;
            }else {
                $create_user = MsWishlistCheff::createWishlistCheff($request->all());
                return $create_user;
            }
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getListWishlistCheff(Request $request)
    {
        try {
            // dd('okok');
            @$email = $request->get('email_user');
            // dd($search_val);
            $list_user = MsWishlistCheff::getListWishlistCheff($email);
            return json_encode($list_user, JSON_UNESCAPED_SLASHES);
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function deleteWishlistCheff(Request $request) {
        $user = $request->input('email_user');
        $cheff = $request->input('email_cheff');
        $deleteWishlistCheff = MsWishlistCheff::deleteWishlistCheff($cheff,$user);
         
        return $deleteWishlistCheff;
    }
    public function checkWishlistCheff(Request $request) {
        $user = $request->input('email_user');
        $cheff = $request->input('email_cheff');
        $checkWishlistCheff = MsWishlistCheff::checkWishlistCheff($cheff,$user);
         
        return $checkWishlistCheff;
    }
    
}
