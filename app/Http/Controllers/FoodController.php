<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsFood as MsFood;
use App\MsReview as MsReview;
use App\MsWishlist as MsWishlist;
use App\MsWishlistCheff as MsWishlistCheff;
use App\MsFollow as MsFollow;
use App\MsCheff as MsCheff;

class FoodController extends Controller
{

    public function createFood(Request $request)
    {           
        try {
            $create_food = MsFood::createFood($request->all());
            return $create_food;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function createFoodNew(Request $request)
    {           
        try {
            $data = $request->all();
            $email_user = $request['email'];
            $cover_dish_reason = $request['cover_dish_reason'];
            $file_internal = $request->file('file_internal');
            $url_external = $request->get('url_external');
            $hasFile = $request->hasFile('file_internal');
            $create_food = MsFood::createFoodNew($request->all());
            if($request->get('cover_dish') == '1' || $request->get('cover_dish') == 'true'){
                $update_sgn = MsFood::signatureFood($email_user,$create_food['value'],$cover_dish_reason);
            }
            // dd($url_external);
            $id_food = $create_food['value'];
            for ($i=0; $i < count($file_internal); $i++) { 
                $new_id = MsFood::newIDGalleryFood();
                $link_to_db = '/files/'.$email_user.'/food/';
                $file = $file_internal[$i] ;
                $name = 'file'.$new_id.'.'.$file->getClientOriginalExtension();
                $destinationPath = app()->basePath('public'.$link_to_db);
                $file->move($destinationPath, $name);
                $link_internal = $link_to_db.$name;
                // dd(url($link_to_db.'/'.$name));

                $upload_internal = MsFood::uploadFoodGalleryInternal($new_id,$id_food,$link_internal);
                // return $upload_internal;
            }
            for ($j=0; $j < count($url_external); $j++) { 
                $new_id = MsFood::newIDGalleryFood();
                $upload_external = MsFood::uploadFoodGalleryExternal($new_id,$id_food,$url_external[$j]);
                // return $upload_external;
            }
            return $create_food;
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function updateFood(Request $request)
    {
        try {
            // $email_user = $request['email'];
            $id_food = $request['id_food'];
            $get_food = MsFood::getFood($id_food);
            $email_user = $get_food['value']->email_user;
            $cover_dish_reason = $request['cover_dish_reason'];
            $file_internal = $request->file('file_internal');
            $url_external = $request->get('url_external');
            $delete_gallery_id = $request->get('delete_gallery_id');
            $update_food = MsFood::updateFoodNew($request->all());
            if($request->get('cover_dish') == '1' || $request->get('cover_dish') == 'true'){
                $update_sgn = MsFood::signatureFood($get_food['value']->email_user,$id_food,$cover_dish_reason);
            }
            if($update_food['status']){
                
                for ($i=0; $i < count($file_internal); $i++) { 
                    $new_id = MsFood::newIDGalleryFood();
                    $link_to_db = '/files/'.$email_user.'/food/';
                    $file = $file_internal[$i] ;
                    $name = 'file'.$new_id.'.'.$file->getClientOriginalExtension();
                    $destinationPath = app()->basePath('public'.$link_to_db);
                    $file->move($destinationPath, $name);
                    $link_internal = $link_to_db.$name;
                    // dd(url($link_to_db.'/'.$name));

                    $upload_internal = MsFood::uploadFoodGalleryInternal($new_id,$id_food,$link_internal);
                    // return $upload_internal;
                }
                for ($j=0; $j < count($url_external); $j++) { 
                    $new_id = MsFood::newIDGalleryFood();
                    $upload_external = MsFood::uploadFoodGalleryExternal($new_id,$id_food,$url_external[$j]);
                    // return $upload_external;
                }
                for ($k=0; $k < count($delete_gallery_id); $k++) { 
                    $delete_gallery = MsFood::deleteFoodGallery($delete_gallery_id[$k]);
                    // return $delete_gallery;
                }
            }
            return $update_food;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function deleteFood(Request $request)
    {
        try {
            // $email_user = $request['email'];
            $id_food = $request['id_food'];
            $delete_food = MsFood::deleteFood($id_food);
            $delete_review = MsReview::deleteReviewByFood($id_food);
            return $delete_food;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getListFoodSearchNew(Request $request)
    {
        
        $lat1 = (float)$request->get('latitude'); 
        $lon1 = (float)$request->get('longitude'); 
        $order_by = $request->get('order_by'); 
        $search_val = $request->get('search_val'); 
        $offset = $request->get('offset');
        $limit = $request->get('limit');
        $request_arr = $request->all();
        if(array_key_exists('email_user_now', $request_arr)){
            $email_user_now = $request_arr['email_user_now'];
        } else {
            $email_user_now = '';
        }
        
        $cart = array();

        $sql = MsFood::getListFoodSearchNew($search_val,$lat1,$lon1,$offset,$limit);
        // return $sql;
        // dd($sql[11]->cheff_image);
        for ($i=0; $i < $sql->count(); $i++) { 
            $sql_food = MsFood::getListFoodSearchPerCheff($search_val,$sql[$i]->email_user);
            if($sql_food->count() <= 0){
                $sql_food = null;
            }
            
            $get_cheff = MsCheff::getCheff($sql[$i]->email_user);
            $sql_follower_cheff = MsFollow::getListFollower($sql[$i]->email_user);
            if($sql_follower_cheff["value"] != null){
                $count_follower_cheff = $sql_follower_cheff["value"]->count();
            } else {
                $count_follower_cheff = 0;
            }
            $checkWishlist = MsWishlist::checkWishlist($sql[$i]->id_food,$email_user_now);
            $checkFollow = MsFollow::checkFollow($sql[$i]->email_user,$email_user_now);
            $distance = number_format((float)$sql[$i]->distance, 2, '.', '');
            unset($sql[$i]->distance);
            $cart[] = array(
                'cheff_data' => $get_cheff['value'],
                'food_data' => $sql_food,
                'distance' => $distance,
                'cheff_follower' => $count_follower_cheff,
                'food_rating' => $sql[$i]->food_rating,
                'cheff_rating' => $sql[$i]->cheff_rating,
                'is_wishlisted' => $checkWishlist["status"],
                'is_followed' => $checkFollow["status"]);
        }
        if(count($cart) > 0){
            $response["value"] = $cart;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan berhasil ditemukan.";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan tidak ditemukan.";
        }
        return $response;
    }
    public function getListFoodSearch(Request $request)
    {
        
        $lat1 = (float)$request->get('latitude'); 
        $lon1 = (float)$request->get('longitude'); 
        $order_by = $request->get('order_by'); 
        $search_val = $request->get('search_val'); 
        $offset = $request->get('offset');
        $limit = $request->get('limit');
        $request_arr = $request->all();
        if(array_key_exists('email_user_now', $request_arr)){
            $email_user_now = $request_arr['email_user_now'];
        } else {
            $email_user_now = '';
        }
        
        $cart = array();

        $sql = MsFood::getListFoodSearch($search_val,$lat1,$lon1,$offset,$limit);
        // return $sql;
        // dd($sql[11]->cheff_image);
        for ($i=0; $i < $sql->count(); $i++) { 
            $sql_food_image = MsFood::getListFoodSearchGallery($sql[$i]->id_food);
            $food_image = '';
            if($sql_food_image->count() > 0){
                $food_image = $sql_food_image[0]->link;
            }
            
            $get_cheff = MsCheff::getCheff($sql[$i]->email_user);
            $sql_follower_cheff = MsFollow::getListFollower($sql[$i]->email_user);
            if($sql_follower_cheff["value"] != null){
                $count_follower_cheff = $sql_follower_cheff["value"]->count();
            } else {
                $count_follower_cheff = 0;
            }
            $checkWishlist = MsWishlist::checkWishlist($sql[$i]->id_food,$email_user_now);
            $checkFollow = MsFollow::checkFollow($sql[$i]->email_user,$email_user_now);
            $distance = number_format((float)$sql[$i]->distance, 2, '.', '');
            unset($sql[$i]->distance);
            $cart[] = array(
                'food_data' => $sql[$i],
                'cheff_data' => $get_cheff['value'],
                'distance' => $distance,
                'cheff_follower' => $count_follower_cheff,
                'food_rating' => $sql[$i]->food_rating,
                'is_wishlisted' => $checkWishlist["status"],
                'is_followed' => $checkFollow["status"]);
        }
        if(count($cart) > 0){
            $response["value"] = $cart;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan berhasil ditemukan.";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan tidak ditemukan.";
        }
        return $response;
    }

    public function getListFavorite(Request $request)
    {
        
        $offset = $request->get('offset');
        $limit = $request->get('limit');
        $request_arr = $request->all();
        if(array_key_exists('email_user_now', $request_arr)){
            $email_user_now = $request_arr['email_user_now'];
        } else {
            $email_user_now = '';
        }
        
        $cart = array();

        $sql = MsFood::getListFoodSearch($offset,$limit);
        // return $sql;
        // dd($sql[11]->cheff_image);
        for ($i=0; $i < $sql->count(); $i++) { 
            $sql_food_image = MsFood::getListFoodSearchGallery($sql[$i]->id_food);
            $food_image = '';
            if($sql_food_image->count() > 0){
                $food_image = $sql_food_image[0]->link;
            }
            
            $get_cheff = MsCheff::getCheff($sql[$i]->email_user);
            $checkWishlist = MsWishlist::checkWishlist($sql[$i]->id_food,$email_user_now);
            $distance = number_format((float)$sql[$i]->distance, 2, '.', '');
            unset($sql[$i]->distance);
            $cart[] = array(
                'food_data' => $sql[$i],
                'cheff_data' => $get_cheff['value'],
                // 'rating' => $sql[$i]->rating,
                'distance' => $distance,
                'is_wishlisted' => $checkWishlist["status"]);
        }
        if(count($cart) > 0){
            $response["value"] = $cart;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan berhasil ditemukan.";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Masakan tidak ditemukan.";
        }
        return $response;
    }
    public function getFoodTest(Request $request)
    {
        try {
            $id_food = $request->input('id_food');
            $get_food = MsFood::getListFoodSearch('');
            // $get_food = MsFood::getFoodTest($id_food);
            // $get_review = MsReview::getListReviewPerFood($id_food);
            // $get_gallery_internal = MsFood::getListFoodGalleryInternal($id_food);
            // $get_gallery_external = MsFood::getListFoodGalleryExternal($id_food);
            // $response["value"] = $get_food['value'];
            // // dd($get_review);
            // $response["list_review"] = $get_review['value'];
            // $response["gallery_external"] = $get_gallery_external['value'];
            // $response["gallery_internal"] = $get_gallery_internal['value'];
            // $response["status"] = $get_food['status'];
            // $response["code"] = 200;
            // $response["error"] = null;
            // $response["message"] = $get_food['message'];
            return $get_food;
            // return $response;
        } catch (Exception $e) {
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getFood(Request $request)
    {
        try {
            $data_request = $request->all();
            if(array_key_exists('email_user', $data_request)){
                $email_user = $data_request['email_user'];
            } else {
                $email_user = '';
            }
            if(array_key_exists('latitude', $data_request)){
                $lat = $data_request['latitude'];
            } else {
                $lat = 0;
            }
            if(array_key_exists('longitude', $data_request)){
                $long = $data_request['longitude'];
            } else {
                $long = 0;
            }
            $id_food = $request->input('id_food');
            $get_food = MsFood::getFood($id_food);
            if($get_food['status']){
                $get_cheff = MsCheff::getCheff($get_food['value']->email_user);
                $get_review = MsReview::getListReviewPerFood($id_food);
                $get_distance = MsCheff::checkDistance($get_food['value']->email_user,$lat,$long);
                // dd($get_distance);
                $get_gallery_internal = MsFood::getListFoodGalleryInternal($id_food);
                $get_gallery_external = MsFood::getListFoodGalleryExternal($id_food);
                $checkFollow = MsFollow::checkFollow($get_food['value']->email_user,$email_user);
                $checkWishlist = MsWishlist::checkWishlist($id_food,$email_user);
                $checkWishlistCheff = MsWishlistCheff::checkWishlistCheff($get_food['value']->email_user,$email_user);
                
                $sql_follower_cheff = MsFollow::getListFollower($get_food['value']->email_user);
                if($sql_follower_cheff["value"] != null){
                    $count_follower_cheff = $sql_follower_cheff["value"]->count();
                } else {
                    $count_follower_cheff = 0;
                }
                $response["food_data"] = $get_food['value'];
                $response["cheff_data"] = $get_cheff['value'];
                $response["distance"] = $get_distance;
                $response["cheff_follower"] = $count_follower_cheff;
                $response["list_review"] = $get_review['value'];
                $response["is_followed"] = $checkFollow["status"];
                $response["is_wishlisted"] = $checkWishlist['status'];
                $response["is_wishlisted_cheff"] = $checkWishlistCheff['status'];
                $response["gallery_external"] = $get_gallery_external['value'];
                $response["gallery_internal"] = $get_gallery_internal['value'];
                $response["status"] = $get_food['status'];
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = $get_food['message'];
            } else {
                $response["food_data"] = null;
                $response["cheff_data"] = null;
                $response["distance"] = 0;
                $response["cheff_follower"] = 0;
                $response["list_review"] = null;
                $response["is_followed"] = false;
                $response["is_wishlisted"] = false;
                $response["is_wishlisted_cheff"] = false;
                $response["gallery_external"] = null;
                $response["gallery_internal"] = null;
                $response["status"] = false;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = $get_food['message'];
                return $response;
            }
            return $response;
        } catch (Exception $e) {
            $response["value"] = null;
            $response["list_review"] = null;
            $response["is_wishlisted"] = false;
            $response["gallery_external"] = null;
            $response["gallery_internal"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function getListFoodPerCheff(Request $request)
    {
        try {
            $email = $request->input('email');
            $get_food = MsFood::getListFoodPerCheff($email);
            return $get_food;
        } catch (Exception $e) {
            $response["value"] = '';
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function uploadFoodGallery(Request $request)
    {
        try {
            // dd('');
            $email_user = $request->get('email');
            $id_food = $request->get('id_food');
            // dd(app()->basePath('/files/cheff/'.$email_user));
            $base_path = app()->basePath('public');
            $type = $request->get('type');
            $source = $request->get('source');
            $link = $request->get('link');
            $is_default = $request->get('is_default');
            // $this->validate($request, [
            //     'file' => 'required|file',
            // ]); 
            $new_id = MsFood::newIDGalleryFood();
            if ($source == 'Internal') {
                if ($request->hasFile('file')) {
                    $link_to_db = '/files/'.$email_user.'/food/';
                    $file = $request->file('file');
                    $name = 'file'.$new_id.'.'.$file->getClientOriginalExtension();
                    $destinationPath = app()->basePath('public'.$link_to_db);
                    $file->move($destinationPath, $name);
                    $link_internal = $link_to_db.$name;
                    // dd(url($link_to_db.'/'.$name));
                    $upload_internal = MsFood::uploadFoodGalleryInternal($new_id,$id_food,$type,$source,$link_internal,$is_default);
                    return $upload_internal;
                } else{
                    $response["value"] = $request->all();
                    $response["status"] = false;
                    $response["code"] = 500;
                    $response["error"] = null;
                    $response["message"] = "File yang anda masukkan kosong";
                }
            }else if ($source == 'YouTube') {
                    
                $upload_yt = MsFood::uploadFoodGalleryYT($new_id,$id_food,$type,$source,$link,$is_default);
                return $upload_yt;
            } else {
                $response["value"] = $request->all();
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Upload file bukan Internal";
                return $response;
            }
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    
    public function getListFoodGallery(Request $request)
    {
        try {
            $email = $request->get('email');
            $id_food = $request->get('id_food');
            $list_gallery = MsFood::getListFoodGallery($id_food);
            // return $response;
            // return $response;
            
            return $list_gallery;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }

    }

    function testDistance(Request $request) {
        $lat1 = $request->get('lat1'); 
        $lon1 = $request->get('lon1'); 
        $lat2 = $request->get('lat2'); 
        $lon2 = $request->get('lon2'); 
        $unit = $request->get('unit'); 
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);
        
            if ($unit == "K") {
              return ($miles * 1.609344);
            } else if ($unit == "N") {
              return ($miles * 0.8684);
            } else {
              return $miles;
            }
        }
    }
    public function signatureFood(Request $request)
    {
        try {
            $email_user = $request['email'];
            $id_food = $request['id_food'];
            $cover_dish_reason = $request['cover_dish_reason'];
            // dd($birth_date);
            $update_sgn = MsFood::signatureFood($email_user,$id_food,$cover_dish_reason);
            return $update_sgn;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function publishFood(Request $request)
    {
        try {
            $id_food = $request['id_food'];
            // dd($birth_date);
            $update_sgn = MsFood::publishFood('true',$id_food);
            return $update_sgn;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
    public function unpublishFood(Request $request)
    {
        try {
            $id_food = $request['id_food'];
            // dd($birth_date);
            $update_sgn = MsFood::publishFood('false',$id_food);
            return $update_sgn;
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }
}
