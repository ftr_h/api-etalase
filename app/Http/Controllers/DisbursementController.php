<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\KeyCheckGenerator as KEY_CHECK;
use OdeoApi\Services\Disbursement;

class DisbursementController extends Controller
{
    $disbursement = new Disbursement();
    $disbursement->setBaseUrl($apiBaseUrl);
    $disbursement->setCredentials($clientId, $clientSecret, $signingKey);

    // request /dg/v1/bank-account-inquiry API
    $disbursement->bankAccountInquiry($accountNo, $bankId, $customerName, $withValidation);

    // request /dg/v1/banks API
    $disbursement->bankList();

    // request ​/dg​/v1​/disbursements API
    $disbursement->executeDisbursement($accountNo, $amount, $bankId, $customerName, $referenceId, $description);

    // request /dg/v1/disbursements/reference-id/{reference_id} API
    $disbursement->checkDisbursementByReferenceId($referenceId);

    // request /dg/v1/disbursements/{disbursement_id} API
    $disbursement->checkDisbursementByDisbursementId($disbursementId);

    // request /cash/me/balance API
    $disbursement->checkBalance();
    
}
