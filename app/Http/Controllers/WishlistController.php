<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\KeyCheckGenerator as KEY_CHECK;
use App\MsMessage as MsMessage;
use App\MsWishlist as MsWishlist;
use App\MsCheff as MsCheff;
use App\MsFollow as MsFollow;

class WishlistController extends Controller
{

    public function createWishlist(Request $request)
    {           
        try {
            $email_user = $request->input('email_user');
            $id_food = $request->input('id_food');
            $checkWishlist = MsWishlist::checkWishlist($id_food,$email_user);
            if($checkWishlist["status"] == true){
                return $checkWishlist;
            }else {
                $create_user = MsWishlist::createWishlist($request->all());
                return $create_user;
            }
            
        } catch (Exception $e) {
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Error : ".$e->getMessage();
            return $response;
        }
    }

    public function getListWishlist(Request $request)
    {
        $offset = $request->get('offset');
        $limit = $request->get('limit');
        $email = $request->get('email_user');
        $lat = $request->get('latitude');
        $long = $request->get('longitude');
        $request_arr = $request->all();
        if(array_key_exists('email_user_now', $request_arr)){
            $email_user_now = $request_arr['email_user_now'];
        } else {
            $email_user_now = '';
        }
        
        $cart = array();

        // return $sql;
        // dd($sql[11]->cheff_image);
        $sql = MsWishlist::getListWishlistFavorite($email,$offset,$limit,$lat,$long);
        for ($i=0; $i < $sql->count(); $i++) { 
            $distance = number_format((float)$sql[$i]->distance, 2, '.', '');
            unset($sql[$i]->distance);
            $get_cheff = MsCheff::getCheff($sql[$i]->email_user);
            $sql_follower_cheff = MsFollow::getListFollower($sql[$i]->email_user);
            if($sql_follower_cheff["value"] != null){
                $count_follower_cheff = $sql_follower_cheff["value"]->count();
            } else {
                $count_follower_cheff = 0;
            }
            $checkWishlist = MsWishlist::checkWishlist($sql[$i]->id_food,$email);
            $checkFollow = MsFollow::checkFollow($sql[$i]->email_user,$email);
            $cart[] = array(
                'food_data' => $sql[$i],
                'cheff_data' => $get_cheff['value'],
                'distance' => $distance,
                'cheff_follower' => $count_follower_cheff,
                'food_rating' => $sql[$i]->food_rating,
                'is_wishlisted' => $checkWishlist["status"],
                'is_followed' => $checkFollow["status"]);
        }
        if(count($cart) > 0){
            $response["value"] = $cart;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar wishlist berhasil ditemukan.";
        } else{
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Daftar wishlist kosong.";
        }
        return $response;
    }

    public function deleteWishlist(Request $request) {
        $email_user = $request->input('email_user');
        $id_food = $request->input('id_food');
        $deleteWishlist = MsWishlist::deleteWishlist($id_food,$email_user);
         
        return $deleteWishlist;
    }
    public function checkWishlist(Request $request) {
        $email_user = $request->input('email_user');
        $id_food = $request->input('id_food');
        $checkWishlist = MsWishlist::checkWishlist($id_food,$email_user);
         
        return $checkWishlist;
    }
    
}
