<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Hash;
use App\TrOrder;
use App\MsWishlist;
use App\MsFollow;
class MsUser extends Model
{
    protected $table = 'ms_user';
    
    public static function checkLogin($email,$password) {
        $sql = DB::table('ms_user')->where('email',$email)
                    ->join('ms_role_user', 'ms_user.id_role_user', '=', 'ms_role_user.id_role_user')
                    ->select('ms_user.*','ms_role_user.name as role_name')
                    ->get();
        
        // dd($sql[0]);
        if ($sql->count() > 0) {
            if (Hash::check($password, $sql[0]->password)) {
                if($sql[0]->status == 1){
                    $cart = TrOrder::checkCart($sql[0]->email);
                    $message = MsMessage::where('language_code',$sql[0]->language_code)
                            ->where('message_code','login_success')
                            ->first();
                    $response['value'] = $sql[0];
                    $response['value_cart'] = $cart;
                    $response["status"] = true;
                    $response["code"] = 200;
                    $response["error"] = null;
                    $response["message"] = $message->message;
                } else{
                    $response["status"] = false;
                    $response["code"] = 500;
                    $response["error"] = null;
                    $response["message"] = "Your email has not been verified, please check your email for verification";
                }
            } else {
                $message = MsMessage::where('language_code',$sql[0]->language_code)
                        ->where('message_code','wrong_password')
                        ->first();
                // dd($message);
                // $response["value"] = $request->all();
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = $message->message;                    
            }
            
        } else {
            $message = MsMessage::where('language_code','ID')
                    ->where('message_code','wrong_email')
                    ->first();
            $response["value"] = $email;
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = $message->message;
        }
        
        
    	return $response;
    }
    public static function createUser($name,$email,$password,$phone,$address,$gender) {
        
        $date_now_ymd = date('Y-m-d');
        $check_email = MsUser::where('email',$email)
                ->get();
        $top_id = MsUser::orderby('id_user','desc')
                ->first();
        // dd($top_id);
        $new_id = 0;
        if ($top_id == null) {
            $new_id = 1;
        } else {
            $new_id = $top_id->id_user + 1;
        }
        // dd($check_email->count());
        if ($check_email->count() < 1) {
            $sql = DB::insert("INSERT INTO ms_user (
                    id_user,
                    name,
                    email,
                    password,
                    phone,
                    address,
                    gender,
                    id_role_user,
                    status,
                    language_code,
                    created_at,
                    updated_at)
                        values (
                          '".$new_id."',
                          '".$name."',
                          '".$email."',
                          '".$password."',
                          '".$phone."',
                          '".$address."',
                          '".$gender."',
                          '1',
                          '1',
                          'ID',
                          '".$date_now_ymd."',
                          '".$date_now_ymd."'
                        )");
            if ($sql) {
                $sql1 = DB::table('ms_user')->where('email',$email)
                    ->join('ms_role_user', 'ms_user.id_role_user', '=', 'ms_role_user.id_role_user')
                    ->select('ms_user.*','ms_role_user.name as role_name')
                    ->get();
                $message = MsMessage::where('language_code','ID')
                        ->where('message_code','create_user_success')
                        ->first();
                // redirect()->to('http://backendetalase.ran-it.com/send_mail/'.$email);
                $cart = TrOrder::checkCart($sql1[0]->email);

                $response["value"] = $sql1[0];
                $response['value_cart'] = $cart;
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = $message->message;
            } else {
                $message = MsMessage::where('language_code','ID')
                        ->where('message_code','create_user_fail')
                        ->first();              
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = $message->message;
            }
        } else{
            $message = MsMessage::where('language_code','ID')
                    ->where('message_code','email_registered')
                    ->first();
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = $message->message;
        }
        
    	return $response;
    }
    public static function updateUser($name,$phone,$birth_date,$jenis_kelamin,$email) {
        $date_now_ymd = date('Y-m-d');
        // dd($birth_date);
        $sql = DB::update("UPDATE ms_user set 
                        name = '$name',
                        phone = '$phone',
                        birth_date = '$birth_date',
                        gender = '$jenis_kelamin',
                        updated_at = '$date_now_ymd'
                        where email='$email'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Update data pengguna berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Update data pengguna gagal";
        }
    	return $response;
    }
    public static function deleteUser($email) {
        $date_now_ymd = date('Y-m-d');
        // dd($birth_date);
          $deleteUser = DB::table('ms_user')->where('email', $email)->delete();
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "delete data pengguna berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "delete data pengguna gagal";
        }
    	return $response;
    }
    public static function updatePasswordUser($email,$old_password,$new_password,$conf_password) {
        $date_now_ymd = date('Y-m-d');
        $sql = MsUser::where('email',$email)
                ->first();
        if (Hash::check($old_password, $sql->password)) {
            
            $sql_update = DB::update("UPDATE ms_user set 
                            password = '$new_password'
                            where email='$email'");
            
            if($sql_update){
                $response['value'] = '';
                $response["status"] = true;
                $response["code"] = 200;
                $response["error"] = null;
                $response["message"] = 'Ubah password berhasil';
            } else{
                // $response["value"] = $request->all();
                $response["value"] = '';
                $response["status"] = false;
                $response["code"] = 500;
                $response["error"] = null;
                $response["message"] = "Ubah password gagal";
            }
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = 'Password lama salah';                    
        }
    	return $response;
    }
    public static function getListUser($search_val) {
        
        $sql = MsUser::join('ms_role_user', 'ms_user.id_role_user', '=', 'ms_role_user.id_role_user')
                          ->where('ms_user.name', 'like',"%".$search_val."%")
                          ->select('ms_user.name','ms_user.id_user','ms_user.id_role_user','ms_user.status','ms_user.created_at','ms_user.phone','ms_user.email','ms_user.birth_date','ms_user.gender','ms_user.language_code','ms_user.image', 'ms_role_user.name as role_name')
                          ->orderby('created_at','desc')
                          ->get();
        if($sql){
            $response["value"] = $sql;
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pengguna berhasil ditemukan";
        } else{
            // $response["value"] = $request->all();
            $response["value"] = null;
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pengguna tidak ditemukan";
        }
    	return $response;
    }
    public static function getUser($email) {
        
        $sql = MsUser::where('email',$email)
                ->join('ms_role_user', 'ms_user.id_role_user', '=', 'ms_role_user.id_role_user')
                ->select('ms_user.name','ms_user.id_user','ms_user.id_role_user','ms_user.status','ms_user.created_at','ms_user.phone','ms_user.email','ms_user.birth_date','ms_user.gender','ms_user.language_code','ms_user.image', 'ms_role_user.name as role_name')
                ->get();
        // dd($sql->count());
        $list_wishlist = MsWishlist::getListWishlist($email);
        $list_follower = MsFollow::getListFollower($email);
        if($sql->count() > 0){
            $response["value"] = $sql[0];
            $response["count_wishlist"] = $list_wishlist['value']->count();
            $response["count_follower"] = $list_follower['value']->count();
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pengguna berhasil ditemukan";
        } else{
            $response["value"] = null;
            $response["count_wishlist"] = $list_wishlist['value']->count();
            $response["count_follower"] = $list_follower['value']->count();
            $response["status"] = false;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Pengguna tidak ditemukan";
        }
    	return $response;
    }
    public static function updateProfileImage($email_user, $link) {
        
        $sql = DB::update("update ms_user set 
                image = '".url($link)."' 
                where email = '$email_user'");
        if ($sql) {
            $response["status"] = true;
            $response["code"] = 200;
            $response["error"] = null;
            $response["message"] = "Upload foto profil berhasil";
        } else {
            // $response["value"] = $request->all();
            $response["status"] = false;
            $response["code"] = 500;
            $response["error"] = null;
            $response["message"] = "Upload foto profil gagal";
        }
    	return $response;
    }
}
